<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/menu', 'page-head'); ?>
  <?php get_template_part('templates/content', 'annual-reports'); ?>
  <?php get_template_part('templates/menu', 'explore-this-section'); ?>
<?php endwhile; ?>
