<?php
  $images = get_posts(
    array(
        'post_type'      => 'attachment',
        'post_mime_type' => 'image',
        'post_parent'    => $post->ID,
        'posts_per_page' => 1
    )
  );
  $large_image = wp_get_attachment_image_src( $images[0]->ID, 'large' );
?>
<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="row">
	<?php if (has_post_thumbnail()): ?>
    <div class="col-sm-12">
    	<div class="thumbnail"><?php the_post_thumbnail('large'); ?></div>
    </div>
    <div class="entry-summary col-sm-12">
      <?php the_excerpt(); ?>
    </div>
  <?php elseif($large_image): ?>
    <div class="col-sm-12">
    	<div class="thumbnail"><img src="<?php echo $large_image[0]; ?>" alt="<?php the_title(); ?>"></div>
    </div>
    <div class="entry-summary col-sm-12">
      <?php the_excerpt(); ?>
    </div>
  <?php else: ?>
    <div class="entry-summary col-sm-12">
      <?php the_excerpt(); ?>
    </div>
  <?php endif; ?>
  </div>
</article>
