<?php
	$parents = get_post_ancestors( $post->ID );
	$id = ($parents) ? $parents[count($parents)-1]: $post->ID;
	$children = get_pages('child_of='.$post->ID);
	if(count($children) > 0) { $parent_id = $post->ID; } else { $parent_id = wp_get_post_parent_id($post->ID); }
	if((count(get_pages(array('child_of' => $parent_id))) > 0 ) && ($parent_id != 'null')):
?>

<div class="row"> 
   <section id="section-menu" class="col-sm-12 page-content">
     <h3>Explore this Section</h3>
     <div class="row">
     <?php
		  $mypages = get_pages(array('child_of' => $parent_id, 'parent' => $parent_id,'sort_column' => 'menu_order', 'sort_order' => 'asc', 'post_status' => 'publish'));
		  foreach( $mypages as $page ):	
	  ?>
		  <div class="col-md-6">
			  <div class="menu-block">
				  <div class="img-wrapper">
					  <a href="<?php echo get_page_link($page->ID); ?>">
					  <h4><?php echo $page->post_title; ?></h4>
					  <?php
					  	  $thumb='';
					  	  $rows = get_field('page_banner', $page->ID);
						  $first_row = $rows[0];
						  $first_row_image = $first_row['banner_image'];
						  if($first_row_image):
						    $image = wp_get_attachment_image_src( $first_row_image['ID'], 'menu-thumb' );
							$thumb = $image[0];
						  else:
						    $thumb = Roots\Sage\Extras\get_attachment_image($page->ID, 'menu-thumb');
						  endif;
						  if($thumb == '') $thumb = get_bloginfo('url') . '/media/menu-bg-default.png';
					  ?>
					  <img class="img-responsive" src="<?php echo $thumb; ?>" alt="<?php echo $page->post_title; ?>" title="<?php echo $page->post_title; ?>">
					  <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x fa-inverse"></i></span>
					  <span class="gradient"></span>
					  </a>
					  
				  </div>
				  <p><?php the_field('page_summary', $page->ID); ?></p>
			  </div>
		  </div>
	  <?php $count++; endforeach; ?>
      </div>
  </section>
</div>

<?php endif; ?>