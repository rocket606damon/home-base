<?php
  $ID = Roots\Sage\Extras\get_post_top_ancestor_id();
  $slug = $post->post_name;
  if(!get_field('page_header')):
?>
<div class="row">
  <div class="page-menu">
      <div class="row">

      	<div class="col-xs-12">
        	<div class="breadcrumbs">
              <div class="row">
                <div class="col-xs-8">
                    <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p>','</p>'); } ?>
                </div>
                <div class="col-xs-4 social">
                  <?php do_action( 'addthis_widget', get_permalink(), get_the_title(), array(
                      'type' => 'custom',
                      'size' => '16',
                      'services' => 'facebook,twitter,linkedin,email',
                      'more' => false
                      ));
                  ?>
                </div>
              </div>
            </div>
        </div>

        <?php if(is_page('training-institute') || is_page('take-a-course')): ?>
        <div class="container cf">
        <section class="col-sm-12 page-content course-nav">
          <h3>Courses For:</h3>
          <div class="col-sm-3 col-md-3">
            <div class="btn-courses">
              <a id="Professionals" class="communitySelect" href="#">
                <div class="btn-professionals"></div>
                <p>Healthcare Professionals</p>
              </a>
            </div>
          </div>
          <div class="communitySelect col-sm-3 col-md-3">
            <div class="btn-courses">
              <a id="Responders" class="communitySelect" href="#">
                <div class="btn-responders"></div>
                <p>First Responders</p>
              </a>
            </div>
          </div>
          <div class="communitySelect col-sm-3 col-md-3">
            <div class="btn-courses">
              <a id="Families" class="communitySelect" href="#">
                <div class="btn-families"></div>
                <p>Military Families</p>
              </a>
            </div>
          </div>
          <div class="communitySelect col-sm-3 col-md-3">
            <div class="btn-courses">
              <a id="Community" class="communitySelect" href="#">
                <div class="btn-community"></div>
                <p>Community Members</p>
              </a>
            </div>
          </div>
        </section>
        </div>
        <?php elseif(is_page('courses-secondary')): ?>
          <div class="container cf">
            <section class="col-sm-12 page-content training-menu">
              <h5>As a community service, the Training Institute offers CME/CE/CEU certified<br>on-line and in-person training at no charge to clinicians, health care professionals<br>and community members throughout New England and nationally.</h5>
            </section>
            <section class="col-sm-12 page-content course-nav">
            <h3>Courses For:</h3>
            <div class="col-sm-3 col-md-3">
              <div class="btn-courses">
                <a id="Professionals" class="communitySelect" href="#">
                  <div class="btn-professionals"></div>
                  <p>Healthcare Professionals</p>
                </a>
              </div>
            </div>
            <div class="communitySelect col-sm-3 col-md-3">
              <div class="btn-courses">
                <a id="Responders" class="communitySelect" href="#">
                  <div class="btn-responders"></div>
                  <p>First Responders</p>
                </a>
              </div>
            </div>
            <div class="communitySelect col-sm-3 col-md-3">
              <div class="btn-courses">
                <a id="Families" class="communitySelect" href="#">
                  <div class="btn-families"></div>
                  <p>Military Families</p>
                </a>
              </div>
            </div>
            <div class="communitySelect col-sm-3 col-md-3">
              <div class="btn-courses">
                <a id="Community" class="communitySelect" href="#">
                  <div class="btn-community"></div>
                  <p>Community Members</p>
                </a>
              </div>
            </div>
          </section>
          </div>
        <?php elseif(is_page('courses-tertiary')): ?>
        <?php else: ?>

        <div class="col-sm-5 col-xs-12 max-height1">

        <?php if($ID == '880' || is_page(1124)): ?>

          <div class="page-menu-accordian">
            <h3><i class="fa fa-bolt"></i> Emergency Care</h3>
            <p>If you or a family member is in crisis or facing an emergency, do not use the Connect With Care email. Go to the nearest emergency room or call the Veterans Crisis Line at <a href="tel:800-273-8255">1-800-273-TALK(8255)</a>.</p>
          </div>

        <?php else: ?>

        <div class="page-menu-accordian ">
          <h3><i class="fa fa-heart"></i> Get Care</h3>
          <p class="get-care">Call us <a href="tel:617-724-5202">617-724-5202</a></p>
          <a class="btn btn-primary" href="<?php bloginfo('url'); ?>/contribute/connect-with-care/#connect-with-care"><i class="fa fa-envelope-o"></i> Or click here to email us</a>
        </div>

        <?php endif; ?>


		</div>

        <div class="col-sm-7 pull-right col-xs-12">
          <div class="page-menu-right cf">
			  <?php if(is_page('warrior-health-fitness') || is_page('new-england') || is_page('southwest-florida')): ?>

              <div class="newletter-left">
                <h6>Register for Warrior Health + Fitness</h6>
                <div class="newsletter-form">
                   <a class="btn btn-default" href="https://hbp.partners.org/fitness/register.php?loc=NE"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> New England</a>
                   <a class="btn btn-success" href="https://hbp.partners.org/fitness/register.php?loc=FL"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Florida</a>
                </div>
              </div>

              <?php elseif(is_page('mind-body-medicine') || is_page('resilient-warrior')): ?>

              <div class="newletter-left">
              	<br /><br />
                <h6>Resilient Warrior Registration</h6>
                <div class="newsletter-form">
                <br /><br />
                  <a href="https://hbp.partners.org/resilientWarrior/register.php"><input type="button" value="SIGN UP" class="news-submit" title="REGISTER"></a>
                  <!--form action="https://hbp.partners.org/sitePro.php" method="post" role="form" data-toggle="validator" id="resilient-warrior">
                    <div class="form-group">
                      <input type="text" class="first-textbox" placeholder="NAME:" tabindex="1" id="resilient-warrior-name" name="resilient-warrior-name" required>
                      <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                      <input type="email" placeholder="EMAIL:" tabindex="2" name="resilient-warrior-email" required>
                      <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                      <input type="submit" value="REGISTER" class="news-submit" title="FOR MORE INFORMATION" tabindex="3">
                      <input type="hidden" name="fmType" value="resilient-warrior">
                    </div>
                  </form-->
                </div>
              </div>

              <?php elseif(is_page('resilient-family')): ?>

              <div class="newletter-left">
                <br /><br />
                <h6>Resilient Family Registration</h6>
                <div class="newsletter-form">
                 <br /><br />
                  <a href="https://hbp.partners.org/resilientWarrior/register.php"><input type="button" value="SIGN UP" class="news-submit" title="REGISTER"></a>
                  <!--form action="https://hbp.partners.org/sitePro.php" method="post" role="form" data-toggle="validator" id="resilient-family">
                    <div class="form-group">
                      <input type="text" class="first-textbox" placeholder="NAME:" tabindex="1" id="resilient-family-name" name="resilient-family-name" required>
                      <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                      <input type="email" placeholder="EMAIL:" tabindex="2" name="resilient-family-email" required>
                      <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                      <input type="submit" value="REGISTER" class="news-submit" title="FOR MORE INFORMATION" tabindex="3">
                      <input type="hidden" name="fmType" value="resilient-family">
                    </div>
                  </form-->
                </div>
              </div>

              <?php elseif(is_page('adventure-series') || is_page('adventure-series-for-veterans-and-families')): ?>

              <div class="newletter-left">
                <h2>Register for<br><?php the_title(); ?></h2>
                <a href="https://hbp.partners.org/eventReg/list.php" class="btn btn-success" target="_blank">REGISTER</a>
              </div>

              <?php elseif(is_page('training-institute') || is_page('take-a-course') || is_page('resources') || is_page('staying-strong-support-for-military-children')): ?>

              <div class="newletter-left">
                <h6>Education + Training Opportunities</h6>
                <div class="newsletter-form">
                	<br /><br /><br />
                  <a href="https://hbp.partners.org/education/register.php"><input type="button" value="SIGN UP" class="news-submit" title="SIGN UP"></a>
                  <!--form action="https://hbp.partners.org/sitePro.php" method="post" role="form" data-toggle="validator" id="eduction-training">
                    <div class="form-group">
                      <input type="text" class="first-textbox" placeholder="NAME:" tabindex="1" id="eduction-training-name" name="eduction-training-name" required>
                      <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                      <input type="email" placeholder="EMAIL:" tabindex="2" name="eduction-training-email" required>
                      <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                      <input type="submit" value="SIGN UP" class="news-submit" title="SIGN UP" tabindex="3">
                      <input type="hidden" name="fmType" value="eduction-training">
                    </div>
                  </form-->
                </div>
              </div>

              <?php elseif($ID == '947' && !is_page('connect-with-care')): ?>

              		<p><?php the_field('donate_text', 'option'); ?></p>
              		<a target="_blank" title="Donate Now" class="btn btn-block btn-success" href="<?php the_field('donate_url', 'option'); ?>"><i class="fa fa-heart-o"></i> Donate Now</a>

              <?php elseif($ID == '880'): ?>

               		<h3><i class="fa fa-heart"></i> Get Care</h3>
                    <p class="get-care">Call us <a href="tel:617-724-5202">617-724-5202</a></p>
                    <a class="btn btn-success" href="<?php bloginfo('url'); ?>/contribute/connect-with-care/#connect-with-care"><i class="fa fa-envelope-o"></i> Or click here to email us</a>

			  <?php else: ?>

              <div class="newletter-left">
                <h6>Subscribe to Our Newsletter</h6>
                <div class="newsletter-form">
                  <a href="https://hbp.partners.org/NewsLetter/register.php"><input type="button" value="SIGN UP" class="news-submit" title="REGISTER"></a>
                  <!--form action="https://hbp.partners.org/sitePro.php" method="post" role="form" data-toggle="validator" id="newsletter-form">
                    <div class="form-group">
                      <input type="text" class="first-textbox" placeholder="NAME:" tabindex="1" id="newsletter-name" name="newsletter-name" required>
                      <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                      <input type="email" placeholder="EMAIL:" tabindex="2" name="newsletter-email" required>
                      <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                      <input type="submit" value="SUBSCRIBE" class="news-submit" title="SIGN UP" tabindex="3">
                      <input type="hidden" name="fmType" value="newsletter">
                    </div>
                  </form-->
                </div>
              </div>

              <?php endif; ?>

          </div>
        </div>
          <?php endif; ?>

      </div>
  </div>
</div>
<?php else: ?>
<div class="row">
  <div class="page-menu"></div>
</div>
<?php endif; ?>