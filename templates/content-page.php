<?php
  if(have_rows('content_sections')):
  while (have_rows('content_sections')) : the_row();
?>
  <?php if( get_row_layout() == 'content' ): ?>

  <div class="row">
    <section id="<?php echo Roots\Sage\Extras\custom_slug(get_sub_field('title')); ?>"<?php if(Roots\Sage\Extras\is_tree(1026)) echo ' class="resources"'; ?>>
        <div class="col-sm-12 page-content">
          <?php if(get_sub_field('title')): ?><h3><?php the_sub_field('title'); ?></h3><?php endif; ?>
          <?php if(get_sub_field('background_color')):?>
          <div class="panel" style="background-color: <?php the_sub_field('background_color'); ?>; border: none; padding: 15px 15px 5px 15px; border-radius: 2px;">
          <?php the_sub_field('content_section'); ?>
          </div>
          <?php else: ?>
          <?php the_sub_field('content_section'); ?>
          <?php endif; ?>
        </div>
    </section>
  </div>

  <?php elseif( get_row_layout() == 'accordion'): ?>
  <div class="row">  
    <div class="accordion">
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <?php $count = 0; ?>
      <?php if(have_rows('accordion_segment')):
        while (have_rows('accordion_segment')) : the_row(); ?>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title"><a data-toggle="collapse"<?php if(is_page('training-institute') && $count != 3)echo ' class="collapsed"';?>" data-parent="#accordion" href="#collapse<?php echo $count; ?>"><?php the_sub_field('title') ?></a></h3>
            </div>
            <div id="collapse<?php echo $count; ?>" class="panel-collapse collapse<?php if(is_page('training-institute') && $count == 3){echo ' in';}else{echo ' collapsed';} ?>">
              <div class="panel-body">
                <div class="accordion-inner">
                  <div class="row">
                    <section id="<?php echo Roots\Sage\Extras\custom_slug(get_sub_field('title')); ?>" class="col-sm-12 page-content">
                      <?php if(is_page('training-institute') && $count == 3): ?>
                        <p><?php if(get_field('funding_partners_blurb')) the_field('funding_partners_blurb'); ?></p>
                        <div class="row">
                        <?php 
                          if(have_rows('funding_partners')): $fundingPartners = 1; 
                          while(have_rows('funding_partners')) : the_row(); 
                          $logo = get_sub_field('logo');
                          $title = $logo['title'];
                          $logoSize = 'logo-thumb';
                          $logoThumb = $logo['sizes'][ $logoSize ];
                        ?>
                        <div class="col-sm-12 col-md-3 <?php if ($fundingPartners == 1 || $fundingPartners % 4 == 0) echo 'col-md-offset-1'; ?>">
                          <a href="<?php the_sub_field('url'); ?>" target="_blank">
                            <img class="img-responsive" src="<?php echo $logoThumb; ?>" title="<?php echo $title; ?>" />
                          </a>
                        </div>
                        <?php $fundingPartners++;endwhile;endif; ?>
                        </div><!--End of row-->
                      <?php else: ?>
                        <?php the_sub_field('content'); ?>  
                      <?php endif; ?>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php $count++;endwhile;endif; ?>   
      </div> 
    </div>
  </div> 

  <?php elseif( get_row_layout() == 'bios' ): ?>

  <div class="row">
    <section id="<?php echo Roots\Sage\Extras\custom_slug(get_sub_field('title')); ?>" class="col-sm-12 page-content bios">
    	<?php if(get_sub_field('title')): ?><h3><?php the_sub_field('title'); ?></h3><?php endif; ?>
        <div class="row">
		  <?php
		    $rows = get_sub_field('bios');
			$total_rows = count($rows);
            if( have_rows('bios') ): $count = 1;
            while ( have_rows('bios') ) : the_row();
            $image = get_sub_field('photo');
            $thumb = $image['sizes']['bio-thumb'];
            $slug = Roots\Sage\Extras\custom_slug(get_sub_field('name'));
          ?>
          <div class="col-sm-4">
              <div class="bio">
                  <div class="thumb"><img class="img-responsive img-circle" src="<?php echo $thumb; ?>" alt="<?php the_sub_field('name'); ?>" title="<?php the_sub_field('name'); ?>"></div>
                  <p class="name"><?php the_sub_field('name'); ?></p>
                  <p class="title"><?php the_sub_field('title'); ?></p>
                  <div class="divider"></div>
                  <i class="fa fa-plus-circle"></i>
                  <a href="#<?php echo $slug;?>" data-toggle="modal" data-target="#<?php echo $slug; ?>"></a>
              </div>
          </div>
          <div class="modal" id="<?php echo $slug;?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $slug;?>">
            <div class="vertical-alignment-helper">
              <div class="modal-dialog modal-lg vertical-align-center" role="document">
                <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-times fa-stack-1x fa-inverse"></i></span></span></button>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-4 profile-thumb">
                          <div class="thumb"><img class="img-responsive img-circle" src="<?php echo $thumb; ?>" alt="<?php the_sub_field('name'); ?>" title="<?php the_sub_field('name'); ?>"></div>
                          <p class="name"><?php the_sub_field('name'); ?></p>
                          <p class="title"><?php the_sub_field('title'); ?></p>
                      </div>
                      <div class="col-sm-8 profile-bio">
                          <?php the_sub_field('bio'); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php
		    if(($count % 3 == 0) && ($count < $total_rows)):
		  ?>
          </div>
          <div class="row">
          <?php endif; ?>
          <?php $count++; endwhile; endif; ?>
        </div>
    </section>
  </div>

  <?php elseif( get_row_layout() == 'video' ): ?>

  <div class="row">
    <section class="col-sm-12 page-content video">
      <?php if(get_sub_field('title')): ?><h3><?php the_sub_field('title'); ?></h3><?php endif; ?>
      <div class="embed-responsive embed-responsive-16by9">
      	<?php
			$iframe = get_sub_field('video');
			preg_match('/src="(.+?)"/', $iframe, $matches);
			$src = $matches[1];
			$params = array(
				'controls'    => 0,
				'hd'        => 1,
				'autohide'    => 1
			);
			$new_src = add_query_arg($params, $src);
			$iframe = str_replace($src, $new_src, $iframe);
			$attributes = 'class="embed-responsive-item"';
			$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
			echo $iframe;
		?>
      </div>
    </section>
  </div>

  <?php endif; ?>
<?php endwhile; endif; ?>