<?php
  if(have_rows('content_sections')):
  while (have_rows('content_sections')) : the_row();
?>
  <?php if( get_row_layout() == 'content' ): ?>
  
  <div class="row"> 
    <section id="<?php echo Roots\Sage\Extras\custom_slug(get_sub_field('title')); ?>">
        <div class="col-sm-12 page-content">
          <?php if(get_sub_field('title')): ?><h3><?php the_sub_field('title'); ?></h3><?php endif; ?>
          <?php the_sub_field('content_section'); ?>
        </div>
    </section>
  </div>
  
  <?php elseif( get_row_layout() == 'bios' ): ?>
  
  <div class="row"> 
    <section id="<?php echo Roots\Sage\Extras\custom_slug(get_sub_field('title')); ?>" class="col-sm-12 page-content bios">
    	<?php if(get_sub_field('title')): ?><h3><?php the_sub_field('title'); ?></h3><?php endif; ?>
        <div class="row">
		  <?php 
            if( have_rows('bios') ): $count = 1;
            while ( have_rows('bios') ) : the_row();
            $image = get_sub_field('photo');
            $thumb = $image['sizes']['bio-thumb'];
			$total_rows = count(get_field('bios')); 
            $slug = Roots\Sage\Extras\custom_slug(get_sub_field('name'));
          ?>
          <div class="col-sm-4">
              <div class="bio">
                  <div class="thumb"><img class="img-responsive img-circle" src="<?php echo $thumb; ?>" alt="<?php the_sub_field('name'); ?>" title="<?php the_sub_field('name'); ?>"></div>
                  <p class="name"><?php the_sub_field('name'); ?></p>
                  <p class="title"><?php the_sub_field('title'); ?></p>
                  <div class="divider"></div>
                  <i class="fa fa-plus-circle"></i>
                  <a href="#<?php echo $slug;?>" data-toggle="modal" data-target="#<?php echo $slug; ?>"></a>
              </div>
          </div>
          <div class="modal" id="<?php echo $slug;?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $slug;?>">
            <div class="vertical-alignment-helper">
              <div class="modal-dialog modal-lg vertical-align-center" role="document">
                <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-times fa-stack-1x fa-inverse"></i></span></span></button>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-4 profile-thumb">
                          <div class="thumb"><img class="img-responsive img-circle" src="<?php echo $thumb; ?>" alt="<?php the_sub_field('name'); ?>" title="<?php the_sub_field('name'); ?>"></div>
                          <p class="name"><?php the_sub_field('name'); ?></p>
                          <p class="title"><?php the_sub_field('title'); ?></p>
                      </div>
                      <div class="col-sm-8 profile-bio">
                          <?php the_sub_field('bio'); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php 
		    if(($count % 3 == 0) && ($count < $total_rows)):
		  ?>
          </div>
          <div class="row">
          <?php endif; ?>
          <?php $count++; endwhile; endif; ?>
        </div>
    </section>
  </div>
  
  <?php endif; ?>
<?php endwhile; endif; ?>

<div class="row"> 
  <section id="connect-with-care" class="col-sm-12 page-content">
  	<h3>Connect With Care</h3>
    <div class="row">
    
    
      <form action="https://hbp.partners.org/sitePro.php" method="post" role="form" data-toggle="validator" id="connect-with-care">
      <div class="gform_body">
          <ul class="gform_fields top_label form_sublabel_below description_below">
              <li class="gfield gfield_contains_required field_sublabel_below field_description_below">
                  <label class="gfield_label">Name*</label>
                  <div class="ginput_container no_prefix has_first_name no_middle_name has_last_name no_suffix gf_name_has_2 ginput_container_name form-group">
                      <span class="name_first">
                          <input type="text" tabindex="2" aria-label="First name" value="" id="name-first" name="name-first" required>
                          <label for="name-first">First</label><span class="help-block with-errors"></span>
                      </span>
                      <span class="name_last">
                          <input type="text" tabindex="4" aria-label="Last name" value="" id="name-last" name="name-last" required>
                          <label for="name-last">Last</label><span class="help-block with-errors"></span>
                      </span>
                  </div>
              </li>
              <li class="gfield gfield_contains_required field_sublabel_below field_description_below form-group">
                  <label for="email" class="gfield_label">Email*</label>
                  <div class="ginput_container ginput_container_email">
                      <input type="email" tabindex="6" class="medium" value="" id="email" name="email" required>
                      <span class="help-block with-errors"></span>
                  </div>
              </li>
              <li class="gfield gfield_contains_required field_sublabel_below field_description_below form-group">
                  <label for="preferred-phone" class="gfield_label">Preferred Phone*</label>
                  <div class="ginput_container ginput_container_phone">
                      <input type="tel" tabindex="7" class="medium" value="" id="preferred-phone" name="preferred-phone" required>
                      <span class="help-block with-errors"></span>
                  </div>
              </li>
              <li class="gfield field_sublabel_below field_description_below form-group">
                  <label class="gfield_label">This number is my*</label>
                  <div class="ginput_container ginput_container_radio">
                      <ul class="gfield_radio">
                          <li><input type="radio" tabindex="8" id="work" value="Work" name="phone-type" checked required><label id="work" for="work">Work</label></li>
                          <li><input type="radio" tabindex="9" id="home" value="Home" name="phone-type"><label id="home" for="home">Home</label></li>
                          <li><input type="radio" tabindex="10" id="mobile" value="Mobile" name="phone-type"><label id="mobile" for="mobile">Mobile</label></li>
                      </ul>
                  </div>
              </li>
              <li class="gfield field_sublabel_below field_description_below form-group">
                  <label class="gfield_label">I am a*</label>
                  <div class="ginput_container ginput_container_radio">
                      <ul class="gfield_radio">
                          <li><input type="radio" tabindex="11" id="service-member-veteran" value="vet" name="typeis" checked required><label id="service-member-veteran" for="service-member-veteran">Service Member or Veteran</label></li>
                          <li><input type="radio" tabindex="12" id="family-member" value="fam" name="typeis"><label id="family-member" for="family-member">Family member of a Service Member or Veteran</label></li>
                      </ul>
                  </div>
              </li>
              <li class="gfield field_sublabel_below field_description_below form-group">
                  <label for="message" class="gfield_label">How can we help?</label>
                  <div class="ginput_container ginput_container_textarea">
                      <textarea cols="50" rows="10" tabindex="13" class="textarea medium" id="message" name="message"></textarea>
                  </div>
              </li>
          </ul>
      </div>
      <div class="form_submit form-group">
          <input type="submit" tabindex="14" value="Submit Request" class="btn btn-primary"> 
          <input type="hidden" name="fmType" value="connect-with-care">
      </div>
      </form>
    
    </div>
  </section>
</div>