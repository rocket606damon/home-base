  <header class="banner" role="banner">
    <div id="header-main">
      <div class="container">

        <div class="inner-header-main cf">
          <div class="row">
          	<?php
      				if(get_field('logo', 'option')):
      				$logo = get_field('logo', 'option');
      			?>
            <div class="col-sm-5 logopart">
            	<a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>" class="logo"><img src="<?php echo $logo['url']; ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>"></a>
            </div>
            <?php endif; ?>
            <div class="col-sm-5 pull-right">
              <div class="right-header">
              	<?php if(have_rows('partners', 'option')): ?>
                <div class="partner-logo">
                <?php
                  $image = get_field('partners_heading', 'option');
                  if( !empty($image) ):
                  $url = $image['url'];
  	              $alt = $image['alt'];
                ?>
                <img class="img-responsive" src="<?php echo $url; ?>" alt="<?php echo $alt; ?>">
                <?php endif; ?>
                <?php
        				  while ( have_rows('partners', 'option') ) : the_row();
        				  $partner_logo = get_sub_field('partner_logo');
        				?>
                <a href="<?php the_sub_field('partner_link'); ?>" title="<?php the_sub_field('partner_name'); ?>" target="_blank"><img src="<?php echo $partner_logo['url']; ?>" alt="<?php the_sub_field('partner_name'); ?>" title="<?php the_sub_field('partner_name'); ?>"></a>
                <?php endwhile; ?>
                </div>
                <?php endif; ?>
                <div class="social-top">
                	<?php if(get_field('twitter', 'option')): ?><a href="<?php the_field('twitter', 'option'); ?>" title="twitter" target="_blank" class="fa fa-twitter social"></a><?php endif; ?>
                    <?php if(get_field('facebook', 'option')): ?><a href="<?php the_field('facebook', 'option'); ?>" title="facebook" target="_blank" class="fa fa-facebook social"></a><?php endif; ?>
                    <?php if(get_field('youtube', 'option')): ?><a href="<?php the_field('youtube', 'option'); ?>" title="youtube" target="_blank" class="fa fa-youtube social"></a><?php endif; ?>
                    <?php if(get_field('instagram', 'option')): ?><a href="<?php the_field('instagram', 'option'); ?>" title="instagram" target="_blank" class="fa fa-instagram social"></a><?php endif; ?>
                </div>
                <div class="right-nav">
                  <form class="search-field" role="search" method="get" action="<?= esc_url(home_url('/')); ?>">
                    <label class="sr-only"><?php _e('Search for:', 'sage'); ?></label>
                    <input type="search" value="<?= get_search_query(); ?>" name="s" placeholder="SEARCH">
                  </form>
                  <button type="submit" class="search-btnicon"><i class="fa fa-search"></i></button>
                  <a href="<?php bloginfo('url'); ?>/about-home-base/contact-us/" title="Contact Us"><i class="fa fa-phone"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="menu-navmain">
          <div class="row">
            <div class="col-sm-12">
              <div class="nav-main">
                <nav class="navbar navbar-default yamm">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" title="navigation"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
                  </div>
                  <div id="navbar" class="navbar-collapse collapse">
                    <?php bootstrap_megamenu_nav(); ?>
                  </div><!--End of /.nav-collapse-->
                </nav>
              </div>
            </div>
          </div>
        </div>
        <!-- /.menu-navmain -->

      </div>
    </div>
  </header>

  <?php if(have_rows('page_banner') && (get_field('banner_type') != 'image_block')): $count = 1; $vidCount = 0; ?>
  <div class="page-banner">
    <div class="container cf">
      <div id="featured-banner" class="carousel slide" data-ride="carousel">
        <?php if(count(get_field('page_banner')) > 1): $count = 0; ?>
          <ol class="carousel-indicators">
		  <?php while ( have_rows('page_banner') ) : the_row();  ?>
            <li<?php if($count == 0) echo ' class="active"'; ?> data-slide-to="<?php echo $count; ?>" data-target="#featured-banner"></li>
          <?php $count++; endwhile; ?>
          </ol>
        <?php $count = 1; endif; ?>
        <div class="carousel-inner" role="listbox">
          <?php
		    while ( have_rows('page_banner') ) : the_row();
			$image = get_sub_field('banner_image');
			$size = 'page-banner';
			$banner_image = $image['sizes'][ $size ];
			if(get_sub_field('external_link')):
				$link = get_sub_field('external_link');
				$target = "_blank";
			elseif(get_sub_field('page_link')):
				$link = get_sub_field('page_link');
				$target = "_self";
			endif;
		  ?>
          <div class="item<?php if($count == 1) echo ' active'; ?>">
          	<?php if(get_sub_field('video_modal')): $vidCount++; ?>
            <a class="video-modal" href="#video-modal-<?php echo $vidCount; ?>" data-toggle="modal" data-target="#video-modal-<?php echo $vidCount; ?>"><span></span><img src="<?php echo $banner_image; ?>" class="img-responsive" alt="<?php the_sub_field('message'); ?>" title="<?php the_sub_field('message'); ?>"></a>
          	<?php elseif(get_sub_field('banner_link')): ?>
            <a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><img src="<?php echo $banner_image; ?>" alt="<?php the_sub_field('message'); ?>" title="<?php the_sub_field('message'); ?>"></a>
            <?php else: ?>
            <img src="<?php echo $banner_image; ?>" alt="<?php the_sub_field('message'); ?>" title="<?php the_sub_field('message'); ?>">
            <?php endif; ?>
            <?php if(get_sub_field('message')): ?>
            <div class="slider-content<?php if(get_sub_field('video_modal')) echo ' video-modal'; ?>">
              <h2 <?php if(is_page('courses-secondary')) echo 'class="scaudience"'; ?>><?php the_sub_field('message'); ?> <?php if(get_sub_field('page_link') || get_sub_field('external_link') && !get_sub_field('banner_link') && !get_sub_field('video_modal')): ?><a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><i class="fa fa-chevron-right"></i></a><?php endif; ?></h2>
            </div>
            <?php endif; ?>
          </div>
          <?php $count++; endwhile; ?>
        </div>
      </div>
    </div>
  </div>
  <?php elseif(get_field('image_block') && (get_field('banner_type') == 'image_block')): ?>
  <div class="page-banner">
    <div class="container cf">
      <img src="<?php the_field('image_block'); ?>" class="img-responsive" alt="<?php the_title(); ?>">
    </div>
  </div>
  <?php endif; ?>

  <?php if(have_rows('page_banner') && (get_field('banner_type') == 'page_banner')): $count = 1; ?>
  <?php
  	while ( have_rows('page_banner') ) : the_row();
  	$videoLink = get_sub_field('video_embed_link');
	if(Roots\Sage\Extras\get_vimeo_id($videoLink) != ''):
		$embedURL = 'https://player.vimeo.com/video/' . Roots\Sage\Extras\get_vimeo_id($videoLink);
	else:
		$embedURL = 'https://www.youtube.com/embed/' . Roots\Sage\Extras\get_youtube_id($videoLink);
	endif;
  ?>
  <div class="modal bios" id="video-modal-<?php echo $count; ?>" tabindex="-1" role="dialog" aria-labelledby="video-modal-<?php echo $count; ?>">
    <div class="vertical-alignment-helper">
      <div class="modal-dialog modal-lg vertical-align-center" role="document">
        <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-times fa-stack-1x fa-inverse"></i></span></span></button>
          <div class="modal-body">
            <div id="video-wrapper" class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="<?php echo $embedURL; ?>"></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $count++; endwhile; endif; ?>
