<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>
      <h2 class="entry-title"><?php the_title(); ?></h2>
      <div class="row">
        <div class="col-xs-6">
        <?php get_template_part('templates/entry-meta'); ?>
        </div>
        <div class="col-xs-6 social">
        	<?php do_action( 'addthis_widget', get_permalink(), get_the_title(), array(
				'type' => 'custom',
				'size' => '32',
				'services' => 'facebook,twitter,linkedin,email',
				'more' => false
				));
			?>
        </div>
      </div>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
      <div class="clear"></div>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
