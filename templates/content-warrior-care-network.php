<?php
  if(have_rows('content_sections')):
  while (have_rows('content_sections')) : the_row();
?>
  <?php if( get_row_layout() == 'content' ): ?>

  <div class="row">
    <section id="<?php echo Roots\Sage\Extras\custom_slug(get_sub_field('title')); ?>">
        <div class="col-sm-12 page-content">
          <?php if(get_sub_field('title')): ?><h3><?php the_sub_field('title'); ?></h3><?php endif; ?>
          <?php the_sub_field('content_section'); ?>
        </div>
    </section>
  </div>

  <?php elseif( get_row_layout() == 'bios' ): ?>

  <div class="row">
    <section id="<?php echo Roots\Sage\Extras\custom_slug(get_sub_field('title')); ?>" class="col-sm-12 page-content bios">
    	<?php if(get_sub_field('title')): ?><h3><?php the_sub_field('title'); ?></h3><?php endif; ?>
        <div class="row">
		  <?php
            if( have_rows('bios') ): $count = 1;
            while ( have_rows('bios') ) : the_row();
            $image = get_sub_field('photo');
            $thumb = $image['sizes']['bio-thumb'];
			$total_rows = count(get_field('bios'));
            $slug = Roots\Sage\Extras\custom_slug(get_sub_field('name'));
          ?>
          <div class="col-sm-4">
              <div class="bio">
                  <div class="thumb"><img class="img-responsive img-circle" src="<?php echo $thumb; ?>" alt="<?php the_sub_field('name'); ?>" title="<?php the_sub_field('name'); ?>"></div>
                  <p class="name"><?php the_sub_field('name'); ?></p>
                  <p class="title"><?php the_sub_field('title'); ?></p>
                  <div class="divider"></div>
                  <i class="fa fa-plus-circle"></i>
                  <a href="#<?php echo $slug;?>" data-toggle="modal" data-target="#<?php echo $slug; ?>"></a>
              </div>
          </div>
          <div class="modal" id="<?php echo $slug;?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $slug;?>">
            <div class="vertical-alignment-helper">
              <div class="modal-dialog modal-lg vertical-align-center" role="document">
                <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-times fa-stack-1x fa-inverse"></i></span></span></button>
                  <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-4 profile-thumb">
                          <div class="thumb"><img class="img-responsive img-circle" src="<?php echo $thumb; ?>" alt="<?php the_sub_field('name'); ?>" title="<?php the_sub_field('name'); ?>"></div>
                          <p class="name"><?php the_sub_field('name'); ?></p>
                          <p class="title"><?php the_sub_field('title'); ?></p>
                      </div>
                      <div class="col-sm-8 profile-bio">
                          <?php the_sub_field('bio'); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php
		    if(($count % 3 == 0) && ($count < $total_rows)):
		  ?>
          </div>
          <div class="row">
          <?php endif; ?>
          <?php $count++; endwhile; endif; ?>
        </div>
    </section>
  </div>

  <?php endif; ?>
<?php endwhile; endif; ?>

<div class="row">
  <section id="locations" class="col-sm-12 page-content">
  	<h3>Warrior Care Network</h3>
    <div class="row">
	<?php
		$count = 1;
        $args = new WP_Query( array('posts_per_page' => -1, 'post_type' =>'locations', 'order' => 'ASC', 'orderby' => 'title', 'tax_query' => array(array('taxonomy' => 'location-type', 'field' => 'slug', 'terms' => 'warrior-care-network'))));
        $total_count = count($args);
		while($args->have_posts()):
        $args->the_post();
    ?>
    	<div class="location-block col-sm-4">
        	<?php
			  $thumb='';$url='';
			  if (has_post_thumbnail()):
			    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'location-thumb' );
			    $url = $thumb['0'];
			  endif;
			?>
            <a href="<?php the_field('url'); ?>" target="_blank"><img class="img-responsive" src="<?php echo $url; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><h4><?php the_title(); ?></h4></a>
        </div>
        <?php if(($count % 3 == 0) && ($count < $total_count)): ?>
        </div>
        <div class="row">
        <?php endif; ?>
    <?php $count++; endwhile; wp_reset_query(); ?>
    </div>
  </section>
</div>