<article id="content-main">

  <?php if(have_rows('featured_links')): ?>
  <div class="row">
    <div class="get-bg">
      <div class="col-xs-12">
        <div class="btn-group" role="group" aria-label="...">
          <?php
		  	while ( have_rows('featured_links') ) : the_row();
		  	if(get_sub_field('external_link')):
				$link = get_sub_field('external_link');
				$target = "_blank";
			else:
				$link = get_sub_field('page_link');
				$target = "_self";
		    endif; ?>
          <a href="<?php echo $link; ?>" class="btn btn-default btn-lg" title="<?php the_sub_field('button_text'); ?>" target="<?php echo $target; ?>"><?php the_sub_field('button_text'); ?></a>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <div class="row">
    <div class="home-blocks row-eq-height">
      <?php
          if(have_rows('home_blocks')):
          while ( have_rows('home_blocks') ) : the_row();
        ?>
      <div class="col-sm-4 col-xs-12">
        <div class="learn-info">
          <h2><?php the_sub_field('title'); ?></h2>
          <i class="fa <?php the_sub_field('icon'); ?> fa-2x"></i>
          <?php the_sub_field('content'); ?>
        </div>
        <a class="btn btn-default btn-lg" title="Learn More" href="<?php the_sub_field('page_link'); ?>" role="button"><?php the_sub_field('button_text'); ?></a>
      </div>
      <?php endwhile; endif; ?>
    </div>
  </div>

  <?php if( have_rows('news_blocks') ): ?>
  <div class="row">
    <div id="block-main">
        <div class="inner-blkmain">

    	  <?php
		  	while ( have_rows('news_blocks') ) : the_row();
			$post_object = get_sub_field('page_post');
			$post = $post_object;
			setup_postdata( $post );
			if( $post_object ):
				$thumb='';
				if(get_sub_field('image')):
					$img_override = get_sub_field('image');
					$size = 'thumbnail';
					$thumb = $image['sizes'][ $size ];
				else:
					$rows = get_field('page_banner', $post->ID);
					$first_row = $rows[0];
					$first_row_image = $first_row['banner_image'];
					if($first_row_image):
					  $image = wp_get_attachment_image_src( $first_row_image['ID'], 'thumbnail' );
					  $thumb = $image[0];
					else:
					  $thumb = Roots\Sage\Extras\get_attachment_image($post->ID, 'thumbnail');
					endif;
				endif;
          ?>

          <div class="col-xs-12 col-sm-4">
            <div class="block  cf">
              <div class="block-inner">
               <span><?php the_sub_field('category'); ?></span>
                <div class="img-border" style="background-image: url(<?php echo $thumb; ?>);"></div>
                <p><?php if(get_sub_field('title')) { the_sub_field('title'); } else { the_title(); } ?></p>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="fa fa-chevron-right article-link"></a></div>
            </div>
          </div>
          <?php wp_reset_postdata(); endif; ?>
          <?php endwhile; ?>

        </div>
    </div>
  </div>
  <?php endif; ?>

  <!--
  <div class="row">
    <div class="calendar">
        <a title="Calendar of Events" class="btn btn-default btn-lg btn-block" href="<?php bloginfo('url'); ?>/about-home-base/calendar-of-events/"><i class="fa fa-calendar"></i> Calendar of Events</a>
    </div>
  </div>
  -->

  <?php if( have_rows('featured_content') ): ?>
  <div class="row">
    <div class="main-abtpart cf">
    <?php while ( have_rows('featured_content') ) : the_row(); ?>

      <?php
	    if( get_row_layout() == 'content_left_1_column' ):
	    $image = get_sub_field('image');
	  ?>
      <div class="about-page">
        <div class="col-sm-5 col-xs-12">
          <div class="about-homebase cf">
            <a href="#" title="twitter" class="fa fa-twitter social"></a> <a href="#" title="facebook" class="fa fa-facebook social"></a> <a href="#" title="youtube" class="fa fa-youtube social"></a>
            <h2><?php the_sub_field('title'); ?></h2>
            <?php the_sub_field('content'); ?>
            <a class="btn btn-default btn-lg" title="Learn More" href="<?php the_sub_field('page_link'); ?>" role="button">Learn More</a> </div>
        </div>

        <div class="col-sm-7 pull-right">
          <div class="about-img abt-img1">
            <div class="img-shape"></div>
            <img src="<?php echo $image['url']; ?>" alt="<?php the_sub_field('title'); ?>" title="<?php the_sub_field('title'); ?>">
          </div>
        </div>
      </div>

      <?php
	    elseif( get_row_layout() == 'content_right_1_column' ):
	    $image = get_sub_field('image');
	  ?>
      <div class="about-page">
        <div class="col-sm-7">
          <div class="about-img abt-img2"> <img src="<?php echo $image['url']; ?>" alt="<?php the_sub_field('title'); ?>" title="<?php the_sub_field('title'); ?>">
            <div class="img-shape"></div>
          </div>
        </div>
        <div class="col-sm-5 col-xs-12">
          <div class="about-homebase cf">
            <?php if(get_field('twitter', 'option')): ?><a href="<?php the_field('twitter', 'option'); ?>" title="twitter" class="fa fa-twitter social"></a><?php endif; ?> <?php if(get_field('facebook', 'option')): ?><a href="<?php the_field('facebook', 'option'); ?>" title="facebook" class="fa fa-facebook social"></a><?php endif; ?> <?php if(get_field('youtube', 'option')): ?><a href="<?php the_field('youtube', 'option'); ?>" title="youtube" class="fa fa-youtube social"></a><?php endif; ?> <?php if(get_field('instagram', 'option')): ?><a href="<?php the_field('instagram', 'option'); ?>" title="instagram" class="fa fa-instagram social"></a><?php endif; ?>
            <h2><?php the_sub_field('title'); ?></h2>
            <?php the_sub_field('content'); ?>
            <a class="btn btn-default btn-lg" title="Learn More" href="<?php the_sub_field('page_link'); ?>" role="button">Learn More</a>
          </div>
        </div>
      </div>

      <?php
        elseif( get_row_layout() == 'content_left_2_column' ):
	    $image = get_sub_field('image');
	  ?>
      <div class="about-page">
        <div class="col-sm-7">
          <div class="about-homebase two cf">
            <h2><?php the_sub_field('title'); ?></h2>
            <div class="abtsnd-content">
              <div class="col-sm-5">
                <?php the_sub_field('column_1'); ?>
                <a class="btn btn-default btn-lg" title="learn more" href="<?php the_sub_field('column_1_link'); ?>" role="button">Learn More</a> </div>
              <div class="col-sm-7">
               <?php the_sub_field('column_2'); ?>
                <a class="btn btn-default btn-lg" title="Learn More" href="<?php the_sub_field('column_1_link'); ?>" role="button">Learn More</a> </div>
            </div>
          </div>
        </div>

        <div class="col-sm-5 col-xs-12">
          <div class="about-img abt-img3">
            <div class="img-shape"></div>
            <img src="<?php echo $image['url']; ?>" alt="<?php the_sub_field('title'); ?>" title="<?php the_sub_field('title'); ?>">
          </div>
        </div>
      </div>
      <?php endif; ?>

    <?php endwhile; ?>
    </div>
  </div>
  <?php endif; ?>

  <div class="row">
  	<div class="newsletter cf">
       <div class="row">
    	<div class="col-sm-5 ">
        	<img src="<?php bloginfo('url'); ?>/media/news-icon.png" alt="Newsletter" title="Newsletter">
            <h6>Subscribe to Our Newsletter:</h6>
        </div>
        <div class="col-sm-7">
        	<form action="https://hbp.partners.org/sitePro.php" method="post" role="form" data-toggle="validator" id="newsletter-form" class="form-inline">
              <div class="form-group">
                <input type="text" class="first-textbox" placeholder="NAME:" tabindex="1" id="newsletter-name" name="newsletter-name" required>
                <div class="help-block with-errors"></div>
              </div>
              <div class="form-group">
                <input type="email" placeholder="EMAIL:" tabindex="2" name="newsletter-email" required>
                <div class="help-block with-errors"></div>
              </div>
              <div class="form-group">
                <input type="submit" value="SUBSCRIBE" class="news-submit" title="SIGN UP" tabindex="3">
                <input type="hidden" name="fmType" value="newsletter">
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="news-main cf">
      <div class="row">
        <div class="col-sm-4">
          <div class="newletter-left">
            <img src="<?php bloginfo('url'); ?>/media/education-icon.png" alt="Education + Training" title="Education + Training">
            <p>Education + Training</p>
            <h6>Opportunities</h6>
            <p>
            <?php if(get_field('bottom_register_1', 'option')): ?><a href="<?php the_field('bottom_register_1', 'option'); ?>" class="btn btn-primary btn-block"><i aria-hidden="true" class="fa fa-pencil-square-o"></i> SIGN UP</a><?php endif; ?>
            </p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="newletter-left">
            <img src="<?php bloginfo('url'); ?>/media/health-icon.png" alt="Warrior Health & Fitness" title="Warrior Health & Fitness">
            <p>Register for</p>
            <h6>Warrior Health &amp; Fitness</h6>
            <p>
				<?php if(get_field('bottom_register_2_top', 'option')): ?><a href="<?php the_field('bottom_register_2_top', 'option'); ?>" class="btn btn-default btn-block"><i aria-hidden="true" class="fa fa-pencil-square-o"></i> New England</a><?php endif; ?>
				<?php if(get_field('bottom_register_2_bottom', 'option')): ?><a href="<?php the_field('bottom_register_2_bottom', 'option'); ?>" class="btn btn-primary btn-block"><i aria-hidden="true" class="fa fa-pencil-square-o"></i> Florida</a><?php endif; ?>
            </p>
          </div>
        </div>
        <div class="col-sm-4 last">
          <div class="newletter-left">
            <img src="<?php bloginfo('url'); ?>/media/register-icon.png" alt="Adventure Series" title="Adventure Series">
            <p>Register for</p>
            <h6>Adventure Series</h6>
            <p>
            <?php if(get_field('bottom_register_3', 'option')): ?><a href="<?php the_field('bottom_register_3', 'option'); ?>" class="btn btn-primary btn-block"><i aria-hidden="true" class="fa fa-pencil-square-o"></i> REGISTER</a><?php endif; ?>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>

</article>

<div class="modal bios" id="featured-video" tabindex="-1" role="dialog" aria-labelledby="featured-video">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog modal-lg vertical-align-center" role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-times fa-stack-1x fa-inverse"></i></span></span></button>
        <div class="modal-body">
          <div id="video-wrapper" class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="<?php the_field('featured_video_url'); ?>"></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>