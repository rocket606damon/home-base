<footer class="content-info" role="contentinfo">
  <div id="logo-footer">
    <div class="container">
      <div class="inner-logo">
        <div class="row">
          <div class="col-xs-12">

			<?php
				if(get_field('logo', 'option')):
				$logo = get_field('logo', 'option');
			?>
            <a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo $logo['url']; ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>"></a>
            <?php endif; ?>

			<?php if(have_rows('partners', 'option')): ?>
            <div class="inner-partners">
            <?php
              while ( have_rows('partners', 'option') ) : the_row();
              $partner_logo = get_sub_field('partner_logo');
            ?>
            <a href="<?php the_sub_field('partner_link'); ?>" title="<?php the_sub_field('partner_name'); ?>" target="_blank"><img src="<?php echo $partner_logo['url']; ?>" alt="<?php the_sub_field('partner_name'); ?>" title="<?php the_sub_field('partner_name'); ?>"></a>
            <?php endwhile; ?>
            </div>
            <?php endif; ?>

            <div class="footer-search">
            <form role="search" method="get" action="<?= esc_url(home_url('/')); ?>">
              <label class="sr-only"><?php _e('Search for:', 'sage'); ?></label>
              <input type="search" value="<?= get_search_query(); ?>" name="s" placeholder="SEARCH">
              <input type="submit" value="SEARCH" class="ftr-searchbtn" title="SEARCH">
            </form>
            </div>
            <ul class="ftr-listlink">
              <?php // wp_nav_menu( array('theme_location' => 'footer_navigation','items_wrap'=>'%3$s', 'container' => false )); ?>
              <li class="menu-item menu-clinical-care"><a href="http://homebase.org/clinical-care/">Clinical Care</a></li>
              <li class="menu-item menu-wellness-fitness"><a href="http://homebase.org/wellness-fitness/">Wellness &amp; Fitness</a></li>
              <li class="menu-item menu-education-training"><a href="http://homebase.org/education-training/">Education &amp; Training</a></li>
              <li class="menu-item menu-innovation"><a href="http://homebase.org/innovation/">Innovation</a></li>
              <li class="menu-item menu-contribute"><a href="http://homebase.org/contribute/">Contribute</a></li>
              <li class="menu-item menu-about-home-base"><a href="http://homebase.org/about-home-base/">About Home Base</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="partner-part">
    <div class="container">
      <div class="inner-partner cf">
        <div class="row">
          <?php
		    if(have_rows('footer_blocks', 'option')): $count = 1;
		    while ( have_rows('footer_blocks', 'option') ) : the_row();
		  ?>
          <div class="col-sm-4">
            <div class="block-<?php echo $count; ?>">
              <h6><i class="fa <?php the_sub_field('fb_icon'); ?> fa-2x"></i> <?php the_sub_field('fb_title'); ?></h6>
              <?php the_sub_field('fb_block_content'); ?>
            </div>
          </div>
          <?php $count++; endwhile; endif; ?>
          <div class="col-sm-4">
            <div class="block-3">
              <h6><i class="fa fa-twitter fa-2x"></i> Latest Tweet</h6>
			  <?php
				require_once(TEMPLATEPATH . '/lib/twitteroauth/autoload.php');
				use Abraham\TwitterOAuth\TwitterOAuth;
                $twitter_customer_key           = 'AhBtWC3rbhiE96q0hvlbvEAiD';
                $twitter_customer_secret        = 'HWIfIFSuX0fKBlB6Kaa2rBh8q71wlM4sOr6vA41KjMRkRoOhNK';
                $twitter_access_token           = '14528884-FxGqOiGHwxJcgx86hr5bNjECOqBd5JbdDSI7kVkOq';
                $twitter_access_token_secret    = 'w6ZmA3VNUn5q8qO3ZhaxWv2K9DwcvqU10SgFg0rHxuWeX';
                $connection = new TwitterOAuth($twitter_customer_key, $twitter_customer_secret, $twitter_access_token, $twitter_access_token_secret);
                $my_tweets = $connection->get('statuses/user_timeline', array('screen_name' => 'homebaseprogram', 'count' => 1));
                if(isset($my_tweets->errors))
                {
                    echo 'Error :'. $my_tweets->errors[0]->code. ' - '. $my_tweets->errors[0]->message;
                }else{
					echo '<p>';
                    echo makeClickableLinks($my_tweets[0]->text);
					$date = new DateTime($my_tweets[0]->created_at);
					echo '<span>' . $date->format('d F, Y') . '</span>';
					echo '</p>';
                }

                function makeClickableLinks($s) {
                  return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a target="blank" rel="nofollow" href="$1" target="_blank">$1</a>', $s);
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="main-ftr">
    <div class="container">
      <div class="innerftr-main">
        <div class="col-sm-5 col-xs-12">
          <p>Copyright &copy; <?php echo date("Y") ?> <a href="<?php bloginfo('url'); ?>" title="Home Base Program">Home Base Program</a></p>
        </div>
        <?php
		  if(get_field('footer_logo', 'option')):
		  $footer_logo = get_field('footer_logo', 'option');
		?>
        <div class="col-sm-2 col-xs-12">
          <a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo $footer_logo['url']; ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>"></a>
        </div>
        <?php endif; ?>
        <div class="col-sm-5 col-xs-12 pull-right">
          <ul>
            <li><a href="http://www.massgeneral.org/careers/apply/" title="Careers" target="_blank">Careers</a></li>
            <li><a href="<?php bloginfo('url'); ?>/privacy-and-security-statement/" title="privacy">Privacy</a></li>
            <li><a href="<?php bloginfo('url'); ?>/sitemap/" title="sitemap">Sitemap</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>