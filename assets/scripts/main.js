/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
    		$(".search-btnicon").click(function(e){
    			e.stopPropagation();
             	$(".search-field").toggleClass("search-open");
            });
    		$(document).click(function(){
    			$(".search-field").removeClass("search-open");
    		});
    		$(".search-field").click(function(e){
    			e.stopPropagation();
    		});
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
    		$('#video-modal-1').on('hidden.bs.modal', function () {
    			$("#video-modal-1 iframe").attr("src", $("#video-modal-1 iframe").attr("src"));
    		});
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // Training Institute Scripts
    'training_institute': {
      init: function() {  
    //   $('.btn-filter').on('click', function () {
  		//   var $target = $(this).data('target');
  		//   if ($target !== 'all') {
  		// 	$('.table tr').not( ".table thead tr" ).css('display', 'none');
  		// 	$('.table tr[data-status="' + $target + '"]').fadeIn('slow');
  		//   } else {
  		// 	$('.table tr').css('display', 'none').fadeIn('slow');
  		//   }
  		// });
        $(document).ready(function(){
          $('.communitySelect').on('click', function(){
            // hide all courses....
            $('.CourseList').hide();
            var type= $(this).prop('id');

            // show the courses containing the speecific
            // community/audience
            console.log("type="+type);
            window.location.href = 'http://homebase.org/education-training/training-institute/courses-secondary/?type='+type;
            return false;
          });
        });
      }
    },
    // Courses Secondary Scripts
    'courses_secondary': {
      init: function() {
        $(document).ready(function(){

          function $_GET(param) {
            var vars = {};
            window.location.href.replace( location.hash, '' ).replace(
              /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
              function( m, key, value ) { // callback
                vars[key] = value !== undefined ? value : '';
              }
            );

            if ( param ) {
              return vars[param] ? vars[param] : null;
            }
            return vars;
          }

          var audienceText = {
            Professionals: 'The Training Institute<br />for Healthcare Professionals',
            Responders: 'The Training Institute<br />for First Responders',
            Community: 'The Training Institute<br />for Community Members',
            Families: 'The Training Institute<br />for Military Families'
          };

          var bannerPath = {
            Professionals: '/media/Banner-Health-Professional.png',
            Responders: '/media/Banner-First-Responder.png',
            Community: '/media/Banner-Community-Member.png',
            Families: '/media/Banner-Military-Family.png'
          };

          // finding width of window
          var limit = 990;
          var slidesShowing = $(this).width() > limit ? 3 : 1;

          // instantiate slider
          var slider = $('.bxslider').bxSlider({
            slideWidth:300,    // need to set if horizontal
            responsive:true,  // require for resize
            mode:'horizontal', // and we are hirizontal
            auto:true, // autoadvance (optional)
            infiniteLoop: true,
            autoStart:true, // start auto on load (optional)
            minSlides: 1,   // minimum slides visible
            maxSlides: slidesShowing,   // maximum slides visible.
            moveSlides: slidesShowing,
            pause: 10000,
            speed: 1000
           });

          // on resize function
          $(window).resize(function() {
            // check: did we pass threshold for image count change?
            if ($(this).width() > limit){
              // if over limit, has slidesShowing already been set?
              if (slidesShowing  !== 3){
                console.log('change on resize:'+$(this).width() + ', showing=' + slidesShowing);
                // need to reset count.
                slidesShowing  = limit;
                // reload slider with new count.
                slider.reloadSlider({
                  maxSlides: slidesShowing,
                  minSlides:slidesShowing
                });
              }

            // if under limit, has slidesShowing already been set?
            } else if (slidesShowing !== 1){
              // need to reset count.
              slidesShowing  = 1;

              // reload slider with new count.
              slider.reloadSlider({
                maxSlides:slidesShowing,
                minSlides:slidesShowing
              });
            }
            return false;
          });

          $('.img-responsive').on('click', function(){
            var Audience = $('.btn-active').prop('id');
            var Category = $(this).closest('.course-section').prop('id');
            var url = $(this).parent('a').attr('href') + "&Audience=" + Audience + "&Category=" + Category;
            console.log("img-responsive: url=" + url);
            window.location = url;
            return false;
          });

          $('.communitySelect').on('click', function(){
            // hide all courses....
            $('.CourseList').hide();
            var type= $(this).prop('id');

            // show the courses containing the speecific
            // community/audience
            console.log("type="+type);
            /*jshint -W069 */
            window.history.pushState("null", "null", "/education-training/training-institute/courses-secondary/?type="+type);
            $('#featured-banner img').attr('src', bannerPath[type]);
            $('.scaudience').empty().html(audienceText[type]);
            $('.breadcrumb_last').html(audienceText[type]);
            /*jshint +W069 */
            $('.btn-courses a.btn-active').removeClass('btn-active');
            $('.btn-courses #'+type).addClass('btn-active');
            $('.'+type).show();
            return false;
          });

          $('.CourseList').hide();
          /* jshint ignore:start */
          var $_GET = $_GET();
          var type = $_GET['type'];
          /* jshint ignore:end */
          if (type){
            $('.'+type).show();
            $('.CourseList').hide();
            $('.'+type).show();
            window.history.pushState("null", "null", "/education-training/training-institute/courses-secondary/?type="+type+"#Course-Toggle-Top");
            location.href = "#Course-Toggle-Top";
            /*jshint -W069 */
            $('#featured-banner img').attr('src', bannerPath[type]);
            $('.scaudience').empty().html(audienceText[type]);
            $('.breadcrumb_last').html(audienceText[type]);
            /*jshint +W069 */
            $('.btn-courses a.btn-active').removeClass('btn-active');
            $('.btn-courses #'+type).addClass('btn-active');
            return false;
          }else{
            /*jshint -W069 */
            $('#featured-banner img').attr('src', bannerPath['Professionals']);
            $('.scaudience').empty().html(audienceText['Professionals']);
            $('.breadcrumb_last').html(audienceText['Professionals']);
            /*jshint +W069 */
            $('.btn-courses #Professionals').addClass('btn-active');
            $('.Professionals').show();
          }
        });
      }
    },
    // Courses Tertiary Scripts
    'courses_tertiary': {
      init: function(){
        $(document).ready(function(){

          function $_GET(param) {
            var vars = {};
            window.location.href.replace( location.hash, '' ).replace(
              /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
              function( m, key, value ) { // callback
                vars[key] = value !== undefined ? value : '';
              }
            );

            if ( param ) {
              return vars[param] ? vars[param] : null;
            }
            return vars;
          }

          var audienceList = {
            Professionals: 'Healthcare Professionals',
            Responders: 'First Responders',
            Community: 'Community Members',
            Families: 'Military Families'
          };

          var bannerPath = {
            Professionals: '/media/Banner-Health-Professional.png',
            Responders: '/media/Banner-First-Responder.png',
            Community: '/media/Banner-Community-Member.png',
            Families: '/media/Banner-Military-Family.png'
          };

          /* jshint ignore:start */
          var $_GET = $_GET();
          var audience = $_GET['Audience'];
          $('#second_to_last a').attr('href', 'http://homebase.org/education-training/training-institute/courses-secondary/?type='+audience);
          $('#return-btn a').attr('href', 'http://homebase.org/education-training/training-institute/courses-secondary/?type='+audience);
          $('#featured-banner img').attr('src', bannerPath[audience]);
          $('a.second_to_last').html(audienceList[audience]);
          /* jshint ignore:end */
        });
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
