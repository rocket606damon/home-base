<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/menu', 'page-head'); ?>
  <?php get_template_part('templates/content', 'home-base-operated'); ?>
  <?php get_template_part('templates/menu', 'explore-this-section'); ?>
<?php endwhile; ?>
