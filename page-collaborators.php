<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/menu', 'page-head'); ?>
  <?php get_template_part('templates/content', 'collaborators'); ?>
  <?php get_template_part('templates/menu', 'explore-this-section'); ?>
<?php endwhile; ?>
