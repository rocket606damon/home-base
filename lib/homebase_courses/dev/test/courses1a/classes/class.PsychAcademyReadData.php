<?php

class PsychAcademyReadData{

	public static function read($config){
		// PALog::log("PsychAcademyReadData::__construct: called");
		return PsychAcademyReadData::getByCategory($config);
		// PALog::log("PsychAcademyReadData::__construct: complete");
	}

	function getToken($config){
		// PALog::log("PsychAcademyReadData::getToken: called");

		$token = '';
		if ($config['username'] && $config['password']){
			// PALog::log("PsychAcademyReadData::getToken:calling formatJson");
			$jsonData = array(USERNAME => $config['username'] , PASSWORD => $config['password'] );
			// PALog::log("PsychAcademyReadData::getToken:calling request");
			$returnValues = PsychAcademyReadData::request($jsonData, $config['baseUrl'].$config['authenticateUrl']);
			$token = isset($returnValues['token']) ? $returnValues['token'] : '';
			// PALog::log("PsychAcademyReadData::getToken: token:".$token);
		}
		// PALog::log("PsychAcademyReadData::getToken: complete:".$token);
		return $token;
	}

	function getByCategory($config){
		// PALog::log("PsychAcademyReadData::getByCategory: called");

		$response = '';
		$token = PsychAcademyReadData::getToken($config);
		// PALog::log("PsychAcademyReadData::token: $token");
		$returnValues = array();
		if  ($token){
			//$jsonData = json_encode(array(TOKEN => $token, CATEGORYNAME => $config['category']));
			$jsonData = array(TOKEN => $token, CATEGORYNAME => $config['category']);
			// PALog::log("PsychAcademyReadData::getByCategory: called:$jsonData");
			$response = PsychAcademyReadData::request($jsonData, $config['baseUrl'].$config['getByCategoryUrl']);
		}else{
			// PALog::log("PsychAcademyReadData::getByCategory: invalid token:".$token);
		}
		PALog::log("PsychAcademyReadData::getByCategory: complete");
		return $response;
	}

	function request($data, $url){
		// PALog::log("PsychAcademyReadData:request:called");
		// PALog::log("PsychAcademyReadData:request:called:".print_r($data,1).":".$url);

		$headers = array('Accept' => 'application/json');
		Unirest\Request::verifyPeer(false);
		$body = Unirest\Request\Body::json($data);
		// PALog::log("PsychAcademyReadData:body:$body ");

		// PALog::log("PsychAcademyReadData::request: MEMORY-BEFORE:".memory_get_peak_usage ());
		$response = Unirest\Request::post($url, $headers, $body);
		// PALog::log("PsychAcademyReadData::request: MEMORY-AFTER:".memory_get_peak_usage ());

		$cookedResponse = substr($response->body, 3);
		$returnValues = json_decode($cookedResponse, 1);
		if (!$returnValues['success']){
			$lastError = isset($returnValues['error']) ? $returnValues['error'] : "Unknown error";
		}
		return $returnValues;
	}
}
