<?php

$sections = array(
	"Healthcare Professionals" => array(
		 "Military Culture and Intro Courses" => array(
			"3070" => "internal",
			"3043" => "internal",
			"3152" => "internal",
			"3050" => "internal",
			"3022" => "internal",
			"3049" => "internal",
			"3048" => "internal",
			"3021" => "internal",
			"external" => "http://homebasetraining.org/Users/HomeBaseProductDetails.aspx?ActivityID=2669",
		),
		"PTSD Courses" => array(
			"3035" => "internal",
			"3029" => "internal",
			"3034" => "internal",
			"3051" => "internal",
			"3045" => "internal",
			"3025" => "internal",
			"3031" => "internal",
			"3032" => "internal",
			"3042" => "internal",
			"3024" => "internal",
			"missing" => "missing",
			"external" => "http://homebasetraining.org/Users/HomeBaseProductDetails.aspx?ActivityID=2669",
		),
		"TBI Courses" => array(
			"3035" => "internal",
			"3051" => "internal",
			"3030" => "internal",
			"3180" => "internal",
			"external" => "http://homebasetraining.org/Users/HomeBaseProductDetails.aspx?ActivityID=2669",
		),
		"Substance Use Courses" => array(
			"3045" => "internal",
			"3048" => "internal",
			"missing" => "missing",
			"3070" => "internal",
			"external" => "http://homebasetraining.org/Users/HomeBaseProductDetails.aspx?ActivityID=2669",
		),
		"Additional Trauma and Treatment Courses" => array(
			"3039" => "internal",
			"3045" => "internal",
			"3070" => "internal",
			"3040" => "internal",
			"3033" => "internal",
			"3072" => "internal",
			"3041" => "internal",
			"3036" => "internal",
			"3046" => "internal",
			"3037" => "internal",
			"3038" => "internal",
			"external" => "http://homebasetraining.org/Users/HomeBaseProductDetails.aspx?ActivityID=2669",
		),
		"Military Family Courses" => array(
			"missing" => "missing",
			"3026" => "internal",
			"3047" => "internal",
			"3028" => "internal",
			"3025" => "internal",
			"3044" => "internal",
			"3071" => "internal",
			"3027" => "internal",
			"missing" => "missing",
		),
	),
	"Community Members" => array(
		 "Military Culture and Intro Courses" => array(
			"3043" => "internal",
			"3152" => "internal",
			"3050" => "internal",
			"3022" => "internal",
			"3049" => "internal",
			"3048" => "internal",
			"3021" => "internal",
		),
		"PTSD Courses" => array(
			"3029" => "internal",
			"3025" => "internal",
			"3024" => "internal",
		),
		"TBI Courses" => array(
			"3035" => "internal",
			"3030" => "internal",
		),
		"Substance Use Courses" => array(
			"3038" => "internal",
		),
		"Additional Trauma and Treatment Courses" => array(
			"3039" => "internal",
			"3040" => "internal",
			"3033" => "internal",
			"3041" => "internal",
			"3036" => "internal",
			"3037" => "internal",
		),
		"Military Family Courses" => array(
			"3026" => "internal",
			"3028" => "internal",
			"3044" => "internal",
			"3071" => "internal",
			"missing" => "missing",
		),
	),
	"First Responder" => array(
		 "" => array(
			"external" => "http://homebasetraining.org/Users/HomeBaseProductDetails.aspx?ActivityID=2771",
			"external" => "http://homebasetraining.org/Users/HomeBaseProductDetails.aspx?ActivityID=2771",
			"external" => "http://homebasetraining.org/Users/HomeBaseProductDetails.aspx?ActivityID=2771",
			"external" => "http://homebasetraining.org/Users/HomeBaseProductDetails.aspx?ActivityID=2771",
			"external" => "http://homebasetraining.org/Users/HomeBaseProductDetails.aspx?ActivityID=2771",
		),
	),
	"Military Families" => array(
		 "Military Culture and Intro Courses" => array(
			"3043" => "internal",
			"3152" => "internal",
			"3050" => "internal",
			"3022" => "internal",
			"3049" => "internal",
			"3048" => "internal",
			"3021" => "internal",
		),
		"PTSD Courses" => array(
			"3035" => "internal",
			"3029" => "internal",
			"3025" => "internal",
			"3024" => "internal",
		),
		"TBI Courses" => array(
			"3035" => "internal",
		),
		"Substance Use Courses" => array(
			"3038" => "internal",
		),
		"Additional Trauma and Treatment Courses" => array(
			"3039" => "internal",
			"3040" => "internal",
			"3033" => "internal",
			"3041" => "internal",
			"3036" => "internal",
			"3037" => "internal",
		),
		"Military Family Courses" => array(
			"missing" => "missing",
			"3026" => "internal",
			"3028" => "internal",
			"3025" => "internal",
			"3044" => "internal",
			"3071" => "internal",
			"3027" => "internal",
			"missing" => "missing",
		),
	),
);
$config = array();
function getItem($config, $itemId){
	return array('id' => $itemId);
}

function renderExternalItem($config, $url){
	$html = "\t\t\t<h4>$url</h4>\n";
	return $html;
}
function renderItem($config, $item){
	$html = "\t\t\t<h4>Item " . $item['id']. "</h4>\n";
	return $html;
}

function renderCategory($config, $title, $itemsHtml=''){
	$html = "\t\t<h3>$title</h3>\n";
	$html .= $itemsHtml;
	return $html;
}

function renderSection ($config, $title, $categoriesHtml=''){
	$html = "\t<h2>$title</h2>\n";
	$html .= $categoriesHtml;
	return $html;
}

function renderSections ($config, $sectionsHtml=''){
	$html = "$sectionsHtml\n";
	return $html;
}

$sectionsHtml = '';
foreach($sections as $sectionTitle => $sectionData){
	$sectionHtml = '';
	foreach($sectionData as $categoryTitle => $categoryData){
		$categoryHtml = '';
		foreach($categoryData as $itemId => $type){
			if ($itemId == "missing"){
			}else if ($itemId == "external"){
				$categoryHtml .= renderExternalItem($config, $type);
			}else{
				$item = getItem($config, $itemId);
				$categoryHtml .= renderItem($config, $item);
			}
		}
		$sectionHtml .= renderCategory($config, $categoryTitle, $categoryHtml);
	}
	$sectionsHtml .= renderSection ($config, $sectionTitle, $sectionHtml);
}
$html = renderSections ($sectionTitle, $sectionsHtml);
echo $html;
