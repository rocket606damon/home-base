<!doctype html>
<html class="no-js" lang="en-US" prefix="og: http://ogp.me/ns#">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="media/index.ico">
  <!--[if lt IE 9]>
  <script src="dist/scripts/html5shiv.min.js"></script>
  <script src="dist/scripts/respond.min.js"></script>
  <![endif]-->
  <title>Courses | Home Base Program</title>
  <link rel="stylesheet" href="dist/styles/main.min.css">
  <link rel="stylesheet" href="dist/styles/page.css">
	<style>
	.course-detail .inner-text{
		padding-top:10px !important;
		padding-right:30px;
		padding-top:30px;
		padding-bottom:10px;
	}
	.course-detail .inner-text p u{
		font-size:11px;
		font-weight:bold;
		font-family:"cocogoose light", sans-serif;
	}
	.Courselist a{
		text-decoration: none;
	}
	.course-detail{
	    background-color: rgba(250,250,250,0.8);
    }
</style>
</head>
  <body class="page courses-secondary">
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</div>
    <![endif]-->
      <header class="banner" role="banner">
    <div id="header-main">
      <div class="container">
        <div class="inner-header-main cf">
          <div class="row">
          	<div class="col-sm-5 logopart">
            	<a href="http://www.homebase.org" title="Home Base" class="logo"><img src="media/home-base-logo-stacked.png" alt="Home Base Program" title="Home Base Program"></a>
            </div>
            <div class="col-sm-5 pull-right">
              <div class="right-header">
                <div class="partner-logo">
                    <img class="img-responsive" src="media/partners-header.png" alt="partners-header" />
                    <a href="https://www.redsoxfoundation.org" title="Red Sox Foundation" target="_blank"><img src="media/header-red-sox-foundation.jpg" alt="Red Sox Foundation" title="Red Sox Foundation"></a>
                    <a href="http://www.massgeneral.org/" title="Massachusetts General Hospital" target="_blank"><img src="media/header-mass-general.jpg" alt="Massachusetts General Hospital" title="Massachusetts General Hospital"></a>
                </div>
                <div class="social-top">
                    <a href="http://www.facebook.com/homebaseprogram" title="twitter" target="_blank" class="fa fa-twitter social"></a>
                    <a href="http://www.facebook.com/homebaseprogram" title="facebook" target="_blank" class="fa fa-facebook social"></a>
                    <a href="http://www.youtube.com/homebaseprogram" title="youtube" target="_blank" class="fa fa-youtube social"></a>
                    <a href="#" title="instagram" target="_blank" class="fa fa-instagram social"></a>
                </div>
                <div class="right-nav">
                  <form class="search-field" role="search" method="get" action="http://www.homebase.org/">
                    <label class="sr-only">Search for:</label>
                    <input type="search" value="" name="s" placeholder="SEARCH">
                  </form>
                  <button type="submit" class="search-btnicon"><i class="fa fa-search"></i></button>
                  <a href="http://www.homebase.org/about-home-base/contact-us/" title="call us"><i class="fa fa-phone"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="menu-navmain">
          <div class="row">
            <div class="col-sm-12">
              <div class="nav-main">
                <nav class="navbar navbar-default yamm">
                  <div class="navbar-header">
                    <button title="navigation" aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
                  </div>
                  <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav">
                      <li class="dropdown"><a aria-expanded="false" aria-haspopup="true" role="button" data-hover="dropdown" class="dropdown-toggle" title="Clinical Care" href="http://www.homebase.org/clinical-care/">Clinical Care<span class="plus"><i class="fa fa-plus"></i></span> <span class="minus"><i class="fa fa-minus"></i></span></a>
                        <ul class="dropdown-menu">
                          <li class="subnav">
                            <div class="row">
                              <div class="col-sm-6">
                                <p><a href="http://www.homebase.org/clinical-care/clinical-care-support/">Family Care + Support at Home Base</a></p>
                                <ul>
                            	  <li><a href="http://www.homebase.org/clinical-care/clinical-care-support/outpatient-veteran-family-clinic-regional/">Outpatient Veteran &amp; Family Clinic (Regional)</a></li>
                            	  <li><a href="http://www.homebase.org/clinical-care/clinical-care-support/intensive-clinical-program-national-regional/">Intensive Clinical Program (National/Regional)</a></li>
                                </ul>
                                <p><a href="http://www.homebase.org/clinical-care/our-model/">Our Model</a></p>
                              </div>
                              <div class="col-sm-6">
                                <p><a href="http://www.homebase.org/clinical-care/invisible-wounds-war/">Invisible Wounds of War</a></p>
								                <ul>
 								                <li><a href="http://www.homebase.org/clinical-care/invisible-wounds-war/pts-d/">Post Traumatic Stress (PTS)</a></li>
                            	  <li><a href="http://www.homebase.org/clinical-care/invisible-wounds-war/tbi/">Traumatic Brain Injury (TBI)</a></li>
                                <li><a href="http://www.homebase.org/clinical-care/invisible-wounds-war/other-invisible-wounds/">Other Invisible Wounds</a></li>
								                <li><a href="http://www.homebase.org/clinical-care/invisible-wounds-war/take-our-online-self-assessment/">Take our online self-assessment</a></li>
                                </ul>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                      <li class="dropdown"><a aria-expanded="false" aria-haspopup="true" role="button" data-hover="dropdown" class="dropdown-toggle" title="Wellness &amp; Fitness" href="http://www.homebase.org/wellness-fitness/">Wellness &amp; Fitness<span class="plus"><i class="fa fa-plus"></i></span> <span class="minus"><i class="fa fa-minus"></i></span></a>
						<ul class="dropdown-menu">
                          <li class="subnav">
                            <div class="row">
                              <div class="col-sm-12">
                                <p><a href="http://www.homebase.org/wellness-fitness/warrior-health-fitness/">Warrior Health + Fitness</a></p>
                                <ul>
                            	  <li><a href="http://www.homebase.org/wellness-fitness/warrior-health-fitness/new-england/">New England</a></li>
                            	  <li><a href="http://www.homebase.org/wellness-fitness/warrior-health-fitness/southwest-florida/">Southwest Florida</a></li>
                                </ul>
                                <p><a href="http://www.homebase.org/wellness-fitness/mind-body-medicine/">Mind Body Medicine</a></p>
                                <ul>
                            	  <li><a href="http://www.homebase.org/wellness-fitness/mind-body-medicine/resilient-warrior/">Resilient Warrior</a></li>
                            	  <li><a href="http://www.homebase.org/wellness-fitness/mind-body-medicine/resilient-family/">Resilient Family</a></li>
                                </ul>
                                <p><a href="http://www.homebase.org/wellness-fitness/adventure-series-for-veterans-and-families/">Adventure Series for Veterans and Families</a></p>
                            </div>
                          </div></li>
                        </ul>
                      </li>
                      <li class="dropdown"><a aria-expanded="false" aria-haspopup="true" role="button" data-hover="dropdown" class="dropdown-toggle" title="Education &amp; Training" href="http://www.homebase.org/education-training/">Education &amp; Training<span class="plus"><i class="fa fa-plus"></i></span> <span class="minus"><i class="fa fa-minus"></i></span></a>
						<ul class="dropdown-menu">
                          <li class="subnav">
                            <div class="row">
                              <div class="col-sm-6">
                                <p><a href="http://www.homebase.org/education-training/training-institute/">Training Institute</a></p>
                                <p><a href="http://www.homebase.org/education-training/staying-strong-support-for-military-children/">Staying Strong: Support for Military Children</a></p>
                                <p><a href="http://www.homebasetraining.org/Users/HomeBaseProductDetails.aspx?ActivityID=2771">*New* First Responder Training</a></p>
                                <p><a href="http://www.homebasetraining.org/Users/HomeBaseProductDetails.aspx?ActivityID=2669">*New* Allied Health Professional Training</a></p>
                              </div>
                              <div class="col-sm-6">
                                <p><a href="http://www.homebase.org/education-training/resources/">Resources</a></p>
                                <ul>
                                  <li><a href="http://www.homebase.org/education-training/resources/veterans-2/">Veterans</a></li>
                            	  <li><a href="http://www.homebase.org/education-training/resources/families/">Families</a></li>
                            	  <li><a href="http://www.homebase.org/education-training/resources/health-professionals/">Health Professionals</a></li>
                                  <li><a href="http://www.homebase.org/education-training/resources/first-responders/">First Responders</a></li>
                            	  <li><a href="http://www.homebase.org/education-training/resources/educators-school-nurses/">Educators &amp; School Nurses</a></li>
                                </ul>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                      <li class="dropdown"><a aria-expanded="false" aria-haspopup="true" role="button" data-hover="dropdown" class="dropdown-toggle" title="Innovation" href="http://www.homebase.org/innovation/">Innovation<span class="plus"><i class="fa fa-plus"></i></span> <span class="minus"><i class="fa fa-minus"></i></span></a>
						<ul class="dropdown-menu">
              			  <li class="subnav">
                            <div class="row">
                              <div class="col-sm-6">
                                <p><a href="http://www.homebase.org/innovation/new-treatment-opportunities/">New Treatment Opportunities</a></p>
                                <ul>
  								  <li><a href="http://www.homebase.org/innovation/new-treatment-opportunities/overview/">Overview</a></li>
                            	  <li><a href="http://www.homebase.org/innovation/new-treatment-opportunities/facts-participating-clinical-trials/">Facts About Participating in Clinical Trials</a></li>
                            	  <li><a href="http://www.homebase.org/innovation/new-treatment-opportunities/featured-clinical-trials/">Featured Clinical Trials</a></li>
                                </ul>

                               </div>
                              <div class="col-sm-6">
                                <p><a href="http://www.homebase.org/innovation/technology/">Technology</a></p>
                                <p><a href="http://www.homebase.org/innovation/scientific-council/">Scientific Council</a></p>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                      <li class="dropdown"><a aria-expanded="false" aria-haspopup="true" role="button" data-hover="dropdown" class="dropdown-toggle" title="Contribute" href="http://www.homebase.org/contribute/">Contribute<span class="plus"><i class="fa fa-plus"></i></span> <span class="minus"><i class="fa fa-minus"></i></span></a>
						<ul class="dropdown-menu">
                          <li class="subnav">
                            <div class="row">
                              <div class="col-sm-6">
                                <p><a href="http://www.homebase.org/contribute/capital-campaign/">Capital Campaign</a></p>
                                <p><a href="http://www.homebase.org/contribute/get-involved/">Get Involved</a></p>
                                <ul>
                            	  <li><a target="_blank" href="https://giving.massgeneral.org/home-base/donate/">Make a Gift</a></li>
								  <li><a href="http://www.homebase.org/contribute/get-involved/giving-levels/">Giving Levels</a></li>
                                  <li><a href="http://www.homebase.org/contribute/get-involved/host-your-own-event/">Host Your Own Event</a></li>
                                </ul>
                                <p><a href="http://www.homebase.org/contribute/volunteer/">Volunteer</a></p>
                              </div>
                              <div class="col-sm-6">
                                <p><a href="http://www.homebase.org/contribute/signature-fundraising-events/">Signature Fundraising Events</a></p>
                                <ul>
                            	    <li><a href="http://www.homebase.org/contribute/signature-fundraising-events/veterans-day-telethon-special/">Veterans Day Special</a></li>
                                  <li><a href="http://www.homebase.org/contribute/signature-fundraising-events/mission-gratitude/">Mission Gratitude</a></li>
                                  <li><a href="http://www.homebase.org/contribute/signature-fundraising-events/run-to-home-base/">Run to Home Base</a></li>
                                </ul>
                                <p><a href="http://www.homebase.org/contribute/connect-with-care/">Connect With Care</a></p>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                      <li class="dropdown"><a aria-expanded="false" aria-haspopup="true" role="button" data-hover="dropdown" class="dropdown-toggle" title="About Home Base" href="http://www.homebase.org/about-home-base/">About Home Base<span class="plus"><i class="fa fa-plus"></i></span> <span class="minus"><i class="fa fa-minus"></i></span></a>
                      	<ul class="dropdown-menu pull-right">
                          <li class="subnav">
                            <div class="row">
                              <div class="col-sm-6">
                                <p><a href="http://www.homebase.org/about-home-base/who-we-are/">Who We Are</a></p>
                                <ul>
                            	  <li><a href="http://www.homebase.org/about-home-base/who-we-are/our-mission/">Our Mission</a></li>
                            	  <li><a href="http://www.homebase.org/about-home-base/who-we-are/our-story/">Our Story</a></li>
                            	  <li><a href="http://www.homebase.org/about-home-base/who-we-are/our-team/">Our Team</a></li>
                                <li><a href="http://www.homebase.org/about-home-base/who-we-are/board-and-partners/">Board and Partners</a></li>
                                <li><a href="http://www.homebase.org/about-home-base/who-we-are/collaborators/">Collaborators</a></li>
                                </ul>
                                <p><a href="http://www.homebase.org/about-home-base/media-center/">Media Center</a></p>
                                <ul>
                            	  <li><a href="http://www.homebase.org/about-home-base/what-we-do/media-center/annual-reports/">Annual Reports</a></li>
                                <li><a href="http://www.homebase.org/about-home-base/what-we-do/media-center/newsroom/">Newsroom</a></li>
                                <li><a href="http://www.homebase.org/about-home-base/what-we-do/media-center/newsletters/">Newsletters</a></li>
                                </ul>
                              </div>
                              <div class="col-sm-6">
                                <p><a href="http://www.homebase.org/about-home-base/what-we-do/">What We Do</a></p>
                                <ul>
                                <li><a href="http://www.homebase.org/about-home-base/what-we-do/whats-different-about-home-base/">What's Different About Home Base</a></li>
                                </ul>
                               	<p><a href="http://www.homebase.org/about-home-base/locations/">Locations</a></p>
                                <p><a href="http://www.homebase.org/about-home-base/contact-us/">Contact Us</a></p>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div class="page-banner">
  <div class="container cf">
    <div id="featured-banner" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
          <div class="item active">
              <img src="media/training-hero-banner.png" alt="The Training Institute for Health Professionals" title="The Training Institute for Health Professionals">
              <div class="slider-content">
                  <h2>The Training Institute<br>For Health Professionals</h2>
              </div>
          </div>
      </div>
    </div>
  </div>
  </div>
  <div class="wrapper" role="document">
    <div class="content">
      <div class="container cf">
        <main class="main" role="main">

            <div class="row">
              <div class="page-menu">
                   <div class="row">

                    <div class="col-xs-12">
                      <div class="breadcrumbs"><p><span xmlns:v="http://rdf.data-vocabulary.org/#"><span typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="http://www.homebase.org">Home</a> � <span class="breadcrumb_last">Course List</span></span></span></p></div>
                    </div>

                    <div class="col-xs-12 col-sm-12 training-menu">
                      <h5>As a community service, the Training Institute offers CME/CE/CEU certified<br>on-line and in-person training at no charge to clinicians, health care professionals<br>and community members throughout New England and nationally.</h5>
                    </div>

                  </div>
              </div>
            </div>

            <!--Course Toggle Top-->
            <div class="row" name="Course-Toggle-Top">
              <section class="col-sm-12 page-content course-nav">
                <h3>Courses For:</h3>
                <div class="col-sm-3 col-md-3">
                  <div class="btn-courses">
                    <a id="Professionals" class="communitySelect" href="#">
                      <div class="btn-professionals"></div>
                      <p>Healthcare Professionals</p>
                    </a>
                  </div>
                </div>
                <div class="communitySelect col-sm-3 col-md-3">
                  <div class="btn-courses">
                    <a id="Responders" class="communitySelect" href="#">
                      <div class="btn-responders"></div>
                      <p>First Responders</p>
                    </a>
                  </div>
                </div>
                <div class="communitySelect col-sm-3 col-md-3">
                  <div class="btn-courses">
                    <a id="Families" class="communitySelect" href="#">
                      <div class="btn-families"></div>
                      <p>Military Families</p>
                    </a>
                  </div>
                </div>
                <div class="communitySelect col-sm-3 col-md-3">
                  <div class="btn-courses">
                    <a id="Community" class="communitySelect" href="#">
                      <div class="btn-community"></div>
                      <p>Community Members</p>
                    </a>
                  </div>
                </div>
              </section>
            </div>

            <!--Featured Courses-->
            <div class="row">
              <section class="col-sm-12 page-content courses-featured">
                <h3>Featured Courses</h3>
              </section>
              <div class="flag-gradient">
                <div class="col-md-12">
                <div id="featured-slide" class="carousel multi-item-carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                    <!--Item 1-->
                    <div class="item active">
                      <div class="col-xs-12 col-sm-4 col-md-4">
                        <a href="level3.php?type=DETAIL&id=3070">
                          <img src="media/courses/HPC_3070_thumb.png" class="img-responsive">
                          <div class="slider-detail">
                            <div class="slider-inner-text">
                            <p><u>Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms</u></p>
                            <p>Credits: 1.00</p>
                            <p>Hours: 1.00</p>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                    <!--Item 2-->
                    <div class="item">
                      <div class="col-xs-12 col-sm-4 col-md-4">
                        <a href="level3.php?type=DETAIL&id=3043">
                          <img src="media/courses/HPC_3043_thumb.png" class="img-responsive">
                          <div class="slider-detail">
                            <div class="slider-inner-text">
                            <p><u>Is there an app for that?</u></p>
                            <p>Credits: </p>
                            <p>Hours: </p>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                    <!--Item 3-->
                    <div class="item">
                      <div class="col-xs-12 col-sm-4 col-md-4">
                        <a href="level3.php?type=DETAIL&id=3152">
                          <img src="media/courses/HPC_3152_thumb.png" class="img-responsive">
                          <div class="slider-detail">
                            <div class="slider-inner-text">
                            <p><u>Military Culture</u></p>
                            <p>Credits: 1.00</p>
                            <p>Hours: 1.00</p>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                    <!--Item 4-->
                    <div class="item">
                      <div class="col-xs-12 col-sm-4 col-md-4">
                        <a href="level3.php?type=DETAIL&id=3050">
                          <img src="media/courses/HPC_3050_thumb.png" class="img-responsive">
                          <div class="slider-detail">
                            <div class="slider-inner-text">
                            <p><u>Physical Health and Mental Health Following Deployment</u></p>
                            <p>Credits: </p>
                            <p>Hours: </p>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div><!--/carousel-inner-->
                  <a class="left carousel-control" href="#featured-slide" data-slide="prev"><i class="fa fa-chevron-left fa-4x"></i></a>
                  <a class="right carousel-control" href="#featured-slide" data-slide="next"><i class="fa fa-chevron-right fa-4x"></i></a>
                </div>
              </div>
              </div>
            </div>

            <!--Accordion-->
            <div class="accordion">
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                <!--Collapse 1-->
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse0">Military Culture and Intro Courses</a></h3>
                  </div>
                  <div id="collapse0" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <div class="accordion-inner">
                        <div class="row">
                          <section id="military-culture" class="col-sm-12 page-content course-section">
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3070">
                                  <img src="media/courses/HPC_3070_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3043">
                                  <img src="media/courses/HPC_3043_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Is there an app for that?</u></p>
                                      <p>Credits: </p>
                                      <p>Hours: </p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3152">
                                  <img src="media/courses/HPC_3152_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Military Culture</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3050">
                                  <img src="media/courses/HPC_3050_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Physical Health and Mental Health Following Deployment</u></p>
                                      <p>Credits: </p>
                                      <p>Hours: </p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3022">
                                  <img src="media/courses/HPC_3022_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Reintegration Issues From the Veterans Perspective: Overcoming the Stigma of Seeking Help</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3049">
                                  <img src="media/courses/HPC_3049_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Service Dogs, Acupuncture, Psychotherapy, and More: What's the Evidence and What's Evidence-based</u></p>
                                      <p>Credits: </p>
                                      <p>Hours: </p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3048">
                                  <img src="media/courses/HPC_3048_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Student Veterans Health: What College Health Professionals and Faculty Need to Know</u></p>
                                      <p>Credits: </p>
                                      <p>Hours: </p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3021">
                                  <img src="media/courses/HPC_3021_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>The Challenges of Coming Home After War: What Providers Need to Know</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList{{CLASSES_2669}}">
                                <a href="level3.php?type=DETAIL&id=2669">
                                  <img src="media/courses/HPC_2669_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS</u></p>
                                      <p>Credits: 3.75</p>
                                      <p>Hours: 3.75</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                            </section>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--End of Collapse 1-->
                  <!--Collapse 2-->
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1">PTSD Courses</a></h3>
                  </div>
                  <div id="collapse1" class="panel-collapse collapse collapsed">
                    <div class="panel-body">
                      <div class="accordion-inner">
                        <div class="row">
                          <section id="ptsd-courses" class="col-sm-12 page-content course-section">
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3035">
                                  <img src="media/courses/HPC_3035_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Challenges of Treating Co-Morbid PTSD and TBI</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3029">
                                  <img src="media/courses/HPC_3029_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Clinical Practice Guidelines and Resources for PTSD Treatment</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3034">
                                  <img src="media/courses/HPC_3034_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Cognitive Processing Therapy for PTSD</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3051">
                                  <img src="media/courses/HPC_3051_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Clinical Case Conference - Recognizing and Treating Mild TBI and PTSD</u></p>
                                      <p>Credits: </p>
                                      <p>Hours: </p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3045">
                                  <img src="media/courses/HPC_3045_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Clinical Case Conference - When Substance Abuse and PTSD Collide</u></p>
                                      <p>Credits: </p>
                                      <p>Hours: </p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3025">
                                  <img src="media/courses/HPC_3025_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Keeping Military Families Emotionally Strong: Couples Therapy for PTSD</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3031">
                                  <img src="media/courses/HPC_3031_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Prolonged Exposure and Virtual Reality Therapy for PTSD</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3032">
                                  <img src="media/courses/HPC_3032_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Psychopharmacology of PTSD</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3042">
                                  <img src="media/courses/HPC_3042_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>PTSD Diagnosis and DSM-V</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3024">
                                  <img src="media/courses/HPC_3024_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Recognizing PTSD and Co-Morbidities</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList{{CLASSES_2669}}">
                                <a href="level3.php?type=DETAIL&id=2669">
                                  <img src="media/courses/HPC_2669_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS</u></p>
                                      <p>Credits: 3.75</p>
                                      <p>Hours: 3.75</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList{{CLASSES_2669}}">
                                <a href="level3.php?type=DETAIL&id=2669">
                                  <img src="media/courses/HPC_2669_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS</u></p>
                                      <p>Credits: 3.75</p>
                                      <p>Hours: 3.75</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                            </section>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--End of Collapse 2-->
                  <!--Collapse 3-->
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse2">TBI Courses</a></h3>
                  </div>
                  <div id="collapse2" class="panel-collapse collapse collapsed">
                    <div class="panel-body">
                      <div class="accordion-inner">
                        <div class="row">
                          <section id="tbi-courses" class="col-sm-12 page-content course-section">
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3035">
                                  <img src="media/courses/HPC_3035_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Challenges of Treating Co-Morbid PTSD and TBI</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3051">
                                  <img src="media/courses/HPC_3051_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Clinical Case Conference - Recognizing and Treating Mild TBI and PTSD</u></p>
                                      <p>Credits: </p>
                                      <p>Hours: </p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community">
                                <a href="level3.php?type=DETAIL&id=3030">
                                  <img src="media/courses/HPC_3030_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Traumatic Brain Injury</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3180">
                                  <img src="media/courses/HPC_3180_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Mild Traumatic Brain Injury: Acute Management, Differential Diagnosis, Treatment, and Rehabilitation (April 2017)</u></p>
                                      <p>Credits: 9.75</p>
                                      <p>Hours: 9.75</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList{{CLASSES_2669}}">
                                <a href="level3.php?type=DETAIL&id=2669">
                                  <img src="media/courses/HPC_2669_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS</u></p>
                                      <p>Credits: 3.75</p>
                                      <p>Hours: 3.75</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                            </section>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--End of Collapse 3-->
                  <!--Collapse 4-->
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Substance Use Courses</a></h3>
                  </div>
                  <div id="collapse3" class="panel-collapse collapse collapsed">
                    <div class="panel-body">
                      <div class="accordion-inner">
                        <div class="row">
                          <section id="substance-use" class="col-sm-12 page-content course-section">
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3045">
                                  <img src="media/courses/HPC_3045_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Clinical Case Conference - When Substance Abuse and PTSD Collide</u></p>
                                      <p>Credits: </p>
                                      <p>Hours: </p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3038">
                                  <img src="media/courses/HPC_3038_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Substance Abuse</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=2669">
                                  <img src="media/courses/HPC_2669_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS</u></p>
                                      <p>Credits: 3.75</p>
                                      <p>Hours: 3.75</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3070">
                                  <img src="media/courses/HPC_3070_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=2669">
                                  <img src="media/courses/HPC_2669_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS</u></p>
                                      <p>Credits: 3.75</p>
                                      <p>Hours: 3.75</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                            </section>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--End of Collapse 4-->
                  <!--Collapse 5-->
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Additional Trauma and Treatment Courses</a></h3>
                  </div>
                  <div id="collapse4" class="panel-collapse collapse collapsed">
                    <div class="panel-body">
                      <div class="accordion-inner">
                        <div class="row">
                          <section id="additional-trauma" class="col-sm-12 page-content course-section">
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3039">
                                  <img src="media/courses/HPC_3039_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Aggression and Domestic Violence</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3045">
                                  <img src="media/courses/HPC_3045_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Clinical Case Conference - When Substance Abuse and PTSD Collide</u></p>
                                      <p>Credits: </p>
                                      <p>Hours: </p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3070">
                                  <img src="media/courses/HPC_3070_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3040">
                                  <img src="media/courses/HPC_3040_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Managing Grief and Loss in Returning Veterans and Families</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3033">
                                  <img src="media/courses/HPC_3033_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Military Sexual Trauma</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3072">
                                  <img src="media/courses/HPC_3072_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Opioid Use Disorders: Considerations in Medication Assisted Treatments of Military Veterans</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3041">
                                  <img src="media/courses/HPC_3041_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Pain Issues in Returning Veterans</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3036">
                                  <img src="media/courses/HPC_3036_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Recognizing Suicide Risk in Returning Veterans</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3046">
                                  <img src="media/courses/HPC_3046_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Reproductive Mental Health Care in Younger and Older Female Veterans</u></p>
                                      <p>Credits: </p>
                                      <p>Hours: </p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3037">
                                  <img src="media/courses/HPC_3037_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Sleep Issues in Returning Veterans</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3038">
                                  <img src="media/courses/HPC_3038_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Substance Abuse</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=2669">
                                  <img src="media/courses/HPC_2669_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS</u></p>
                                      <p>Credits: 3.75</p>
                                      <p>Hours: 3.75</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                            </section>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--End of Collapse 5-->
                  <!--Collapse 6-->
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Military Family Courses</a></h3>
                  </div>
                  <div id="collapse5" class="panel-collapse collapse collapsed">
                    <div class="panel-body">
                      <div class="accordion-inner">
                        <div class="row">
                          <section id="military-family" class="col-sm-12 page-content course-section">
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=2669">
                                  <img src="media/courses/HPC_2669_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS</u></p>
                                      <p>Credits: 3.75</p>
                                      <p>Hours: 3.75</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3026">
                                  <img src="media/courses/HPC_3026_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Challenges Facing Other Family Members When a Veteran has PTSD</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals">
                                <a href="level3.php?type=DETAIL&id=3047">
                                  <img src="media/courses/HPC_3047_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Clinical Case Conference - When a Parent has PTSD: Restoring Resilience in Parents and Their Children</u></p>
                                      <p>Credits: </p>
                                      <p>Hours: </p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3028">
                                  <img src="media/courses/HPC_3028_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Impact of Combat-Related Injury, Illness, and Death on Military Children and Families</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3025">
                                  <img src="media/courses/HPC_3025_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Keeping Military Families Emotionally Strong: Couples Therapy for PTSD</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3044">
                                  <img src="media/courses/HPC_3044_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Staying Strong: Helping Families and Schools Build Resilience in Military-Connected Children</u></p>
                                      <p>Credits: </p>
                                      <p>Hours: </p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3071">
                                  <img src="media/courses/HPC_3071_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Suicide Postvention: Survivor Care for Families of Service Members and Veterans</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=3028">
                                  <img src="media/courses/HPC_3028_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Impact of Combat-Related Injury, Illness, and Death on Military Children and Families</u></p>
                                      <p>Credits: 1.00</p>
                                      <p>Hours: 1.00</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                              <div class="col-sm-4 CourseList Professionals Community Families">
                                <a href="level3.php?type=DETAIL&id=2669">
                                  <img src="media/courses/HPC_2669_thumb.png" class="img-responsive" />
                                  <div class="course-detail">
                                    <div class="inner-text">
                                      <p><u>Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS</u></p>
                                      <p>Credits: 3.75</p>
                                      <p>Hours: 3.75</p>
                                    </div>
                                  </div>
                                </a>
                              </div>
                            </section>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--End of Collapse 6-->

              </div><!--//End of Panel Group-->
            </div><!--//End of Accordion-->


        </main><!-- /.main -->
      </div>
    </div><!-- /.content -->
  </div><!-- /.wrap -->
    <footer class="content-info" role="contentinfo">
      <div id="logo-footer">
        <div class="container">
          <div class="inner-logo">
            <div class="row">
              <div class="col-xs-12">
                <a href="http://www.homebase.org" title="Home Base"><img src="media/home-base-logo-stacked.png" alt="Home Base Program" title="Home Base Program"></a>
                <div class="inner-partners">
                <a href="https://www.redsoxfoundation.org" title="Red Sox Foundation" target="_blank"><img src="media/header-red-sox-foundation.jpg" alt="Red Sox Foundation" title="Red Sox Foundation"></a>
                <a href="http://www.massgeneral.org/" title="Massachusetts General Hospital" target="_blank"><img src="media/header-mass-general.jpg" alt="Massachusetts General Hospital" title="Massachusetts General Hospital"></a>
              </div>

                <div class="footer-search">
                <form role="search" method="get" action="http://www.homebase.org/">
                  <label class="sr-only">Search for:</label>
                  <input type="search" value="" name="s" placeholder="SEARCH">
                  <input type="submit" value="SEARCH" class="ftr-searchbtn" title="SEARCH">
                </form>
                </div>
                <ul class="ftr-listlink">
                  <li class="menu-item menu-clinical-care"><a href="http://homebase.org/clinical-care/">Clinical Care</a></li>
                  <li class="menu-item menu-wellness-fitness"><a href="http://www.homebase.org/wellness-fitness/">Wellness &#038; Fitness</a></li>
                  <li class="menu-item menu-education-training"><a href="http://www.homebase.org/education-training/">Education &#038; Training</a></li>
                  <li class="menu-item menu-innovation"><a href="http://www.homebase.org/innovation/">Innovation</a></li>
                  <li class="menu-item menu-contribute"><a href="http://www.homebase.org/contribute/">Contribute</a></li>
                  <li class="menu-item menu-about-home-base"><a href="http://www.homebase.org/about-home-base/">About Home Base</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="partner-part" style="clear: both;">
        <div class="container">
          <div class="inner-partner cf">
            <div class="row">
              <div class="col-sm-4">
                <d<iv class="block-1">
                  <h6><i class="fa fa-users fa-2x"></i> Our Partners</h6>
                  <p><a href="http://web.welcomebackveterans.org" target="_blank"><img class="alignleft img-responsive" src="media/Partners-homebaseorg.png" alt="partners-homebaseorg" width="256" height="174" /></a></p>
                </div>
              </div>
              <div class="col-sm-4">
                <d<iv class="block-2">
                  <h6><i class="fa fa-map-marker fa-2x"></i> Network Locations</h6>
                  <p><a href="http://www.homebase.org/about-home-base/locations/"><img src="media/map-img.png" alt="map-img" /></a></p>
                </div>
              </div>
              <div class="col-sm-4">
                <d<iv class="block-3">
                  <h6><i class="fa fa-twitter fa-2x"></i> Latest Tweet</h6>
                  <p></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="main-ftr">
        <div class="container">
          <div class="innerftr-main">
            <div class="col-sm-5 col-xs-12">
              <p>Copyright &copy; 2017 <a title="Home Base Program" href="http://www.homebase.org">Home Base Program</a></p>
            </div>
            <div class="col-sm-2 col-xs-12">
              <a title="Home Base Program" href="http://www.homebase.org"><img title="Home Base Program" alt="Home Base Program" src="media/homebase-footer-logo.png"></a>
            </div>
            <div class="col-sm-5 col-xs-12 pull-right">
              <ul>
                <li><a target="_blank" title="Careers" href="http://www.massgeneral.org/careers/apply/">Careers</a></li>
                <li><a href="http://www.homebase.org/privacy-and-security-statement/" title="privacy">Privacy</a></li>
                <li><a href="http://www.homebase.org/sitemap/" title="sitemap">Sitemap</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="dist/scripts/jquery.js"><\/script>')</script>
<script src="dist/scripts/main.js"></script>
<script src="dist/scripts/slider.js"></script>
<script>
  	$(document).ready(function(){
		$('.communitySelect').on('click', function(){
			// hide all courses....
			$('.CourseList').hide();
			var type= $(this).prop('id');

			// show the courses containing the speecific
			// community/audience
			console.log("type="+type);
			$('.'+type).show();

			return false;
		});

		function $_GET(param) {
			var vars = {};
			window.location.href.replace( location.hash, '' ).replace(
				/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
				function( m, key, value ) { // callback
					vars[key] = value !== undefined ? value : '';
				}
			);

			if ( param ) {
				return vars[param] ? vars[param] : null;
			}
			return vars;
		}

		var $_GET = $_GET();
		var type = $_GET['type'];
		$('.'+type).show();
		$('.CourseList').hide();
		$('.'+type).show();

		location.href = "#Course-Toggle-Top";

		return false;

  	});
</script>
<script>
  (function(s,o,i,l){s.ga=function(){s.ga.q.push(arguments);if(o['log'])o.log(i+l.call(arguments))}
  s.ga.q=[];s.ga.l=+new Date;}(window,console,'Google Analytics: ',[].slice))
  ga('create','UA-70403905-1','auto');ga('send','pageview')
</script>
</body>
</html>