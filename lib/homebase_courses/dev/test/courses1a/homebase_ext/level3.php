<?php

ini_set('memory_limit', '2048M');

define('CONFIG_FILE', 'config/config.php');

require_once(CONFIG_FILE);
require_once(CLASS_DIR."class.PsychAcademyReadData.php");
require_once(CLASS_DIR."class.RenderDetailHTML.php");
require_once(CLASS_DIR."class.RenderSummaryHTML.php");
require_once(CLASS_DIR."class.ParseJSON.php");
require_once(CLASS_DIR."class.RenderFromTemplate.php");
require_once(CLASS_DIR."class.PALog.php");
require_once(VENDOR_DIR."unirest/src/Unirest.php");

function getlist_homebase_shortcode($atts = [], $content = null, $tag = ''){
	$hb_attrs= shortcode_atts( array(
		'id' => '',
    ), $atts, 'homebase');

    // normalize attribute keys, lowercase
    $hb_atts = array_change_key_case((array)$hb_atts, CASE_LOWER);

    // get the course id
    $category_id = $hb_atts['id'];

	$jsonData = getJson($config);
	$html = '';

	PALog::log("renderHTML: DETAIL called");
	$html = ParseJSON::parse($config, $jsonData, SUMMARY, $id);

	// return output
    return $html;
}

function getfeatures_homebase_shortcode(){
    $html = '';

	$jsonData = getJson($config);
	$html = '';

	PALog::log("renderHTML: DETAIL called");
	$html = ParseJSON::parse($config, $jsonData, SUMMARY);    // return output
    return $html;
}

function getdetails_homebase_shortcode(_shortcode$atts = [], $content = null, $tag = ''){
	$hb_attrs= shortcode_atts( array(
		'id' => '',
    ), $atts, 'homebase');

    // normalize attribute keys, lowercase
    $hb_atts = array_change_key_case((array)$hb_atts, CASE_LOWER);

    // get the course id
    $course_id = $hp_atts['course_id'];

	$jsonData = getJson($config);
	$html = '';

	PALog::log("renderHTML: DETAIL called");
	$html = ParseJSON::parse($config, $jsonData, DETAIL, $id);

    // return output
    return $html;
}


function getJson($config){
	$cachePath = $config['cacheDir'].$config['cacheFile'];

	if (file_exists($cachePath) && (filemtime($cachePath) > (time() - 60 * $config['cacheMinutes'] ))) {
	   $response = json_decode(file_get_contents($cachePath),1);
	} else {
	   $response = PsychAcademyReadData::read($config);
	   file_put_contents($cachePath, json_encode($response,1), LOCK_EX);
	}
	return $response;
}

function homebase_shortcodes_init(){
	add_shortcode('getfeatures', 'getfeatures_homebase_shortcode');
	add_shortcode('getlist', 'getlist_homebase_shortcode');
	add_shortcode('getdetails', 'getdetails_homebase_shortcode');
}

add_action('init', 'mgh_homebasecourses_shortcodes_init');


