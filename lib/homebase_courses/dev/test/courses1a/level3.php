<?php

ini_set('memory_limit', '2048M');

define('CONFIG_FILE', 'config/config.php');

require_once(CONFIG_FILE);
require_once(CLASS_DIR."class.PsychAcademyReadData.php");
require_once(CLASS_DIR."class.RenderDetailHTML.php");
require_once(CLASS_DIR."class.RenderSummaryHTML.php");
require_once(CLASS_DIR."class.ParseJSON.php");
require_once(CLASS_DIR."class.RenderFromTemplate.php");
require_once(CLASS_DIR."class.PALog.php");
require_once(VENDOR_DIR."unirest/src/Unirest.php");

function getJson($config){
	$cachePath = $config['cacheDir'].$config['cacheFile'];

	if (file_exists($cachePath) && (filemtime($cachePath) > (time() - 60 * $config['cacheMinutes'] ))) {
	   $response = json_decode(file_get_contents($cachePath),1);
	} else {
	   $response = PsychAcademyReadData::read($config);
	   file_put_contents($cachePath, json_encode($response,1), LOCK_EX);
	}
	return $response;
}

function renderHTML($config, $type, $id=NULL){

	$jsonData = getJson($config);
	$html = '';

	switch($type){
		// Not tested.
		case SUMMARY:
			PALog::log("renderHTML: SUMMARY called");
			$html = ParseJSON::parse($config, $jsonData, SUMMARY);
			break;

		case DETAIL:
			PALog::log("renderHTML: DETAIL called");
			$html = ParseJSON::parse($config, $jsonData, DETAIL, $id);
			break;
	}
	return $html;
}

if (isset($_REQUEST['type'])){
	$type = $_REQUEST['type'];
	$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
	$html = renderHTML($config, $type, $id);
	echo $html;
}
