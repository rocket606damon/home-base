<?php

class ParseJSON{

	public static $data;
	public static $config;

	public static function getById($id){
		PALog::log("ParseJSON::getById: called:$id");

		$activityDetaillist = ParseJson::$data['activityDetaillist'];
		foreach($activityDetaillist as $item){
			if ($item['id'] == $id){
				PALog::log("ParseJson::getById: found item ($id)");
				return $item;
			}
		}
		PALog::log("ParseJson::getById: complete");
		return array();
	}


	public static function parseAudience($node){
		if (isset($node['targetAudiences'])){
			foreach($node['targetAudiences'] as $targetAudience){
				ParseJson::$data['targetaudience'][] = isset($targetAudience['targetAudience']) ?
						$targetAudience['targetAudience'] : '';
			}
		}
	}

	public static function parseObjectives($node){
		ParseJson::$data['objectives'] = array();
		ParseJson::$data['modules'] = array();
		if (!is_array($node['learningObjectives'][0]['learningObjective'])){
			ParseJson::$data['intro'] = $node['intro'];
			foreach($node['learningObjectives'] as $learningObjective){
				ParseJson::$data['objectives'][] = $learningObjective['learningObjective'];
			}
		}else{
			foreach($node['learningObjectives'] as $learningObjective){
				$modules = $learningObjective;
				$moduleList = array();
				foreach($modules as $module){
					$moduleIntro = isset($module['moduleIntro']) ? $module['moduleIntro'] : '';
					$moduleObjectives = array();
					foreach($module['moduleLearningObjectives'] as $moduleObjective){
						$moduleObjectives[] = isset($moduleObjective['moduleLearningObjective'])?
								$moduleObjective['moduleLearningObjective'] : '';
					}
					ParseJson::$data['modules'][] = array(
						'moduleIntro'=>$moduleIntro,
						'moduleObjectives'=>$moduleObjectives
					);
				}
			}
		}
	}

	public static function parse($config, $response, $id=NULL){
		PALog::log("ParseJson::parse: called");
		PALog::log("ParseJson::parse: activityDetaillist");
		ParseJson::$data = array();
		ParseJson::$config = $config;

		if (isset($response['activityDetaillist'])){

			$activityDetaillist = $response['activityDetaillist'];
			PALog::log("ParseJson::parse: found: activityDetaillist");
			PALog::log("ParseJson::parse: item count:".count($activityDetaillist));
			foreach($activityDetaillist as $content){
				if ($id == NULL || $content['id'] == $id){
					PALog::log("ParseJson::parse: next...");
					ParseJson::$data['id'] = isset($content['id'])? $content['id'] : '';
					ParseJson::$data['title'] = isset($content['title'])? $content['title'] : '';
					ParseJson::$data['format'] = isset($content['CustomActivityType'])? $content['CustomActivityType'] : '';
					ParseJson::$data['instructors'] = array();
					ParseJson::$data['credits'] = '';
					ParseJson::$data['items'] = array();
					ParseJson::$data['featuredItems'] = array();
					ParseJson::$data['time'] = '';
					ParseJson::$data['hours'] = '';
					ParseJson::$data['photo'] = '';
					ParseJson::$data['intro'] = '';
					ParseJson::$data['targetaudience'] = array();
					ParseJson::$data['objectives'] = array();
					ParseJson::$data['modules'] = array();
					ParseJson::$data['accreditations'] = array();
					ParseJson::$data['dump'] = '';

					if (isset($content['facultylists'])){
						foreach($content['facultylists'] as $facutlyMember){
							ParseJson::$data['instructors'][] = $facutlyMember['name'];
						}
					}

					if (isset($content['overviewcontent'])){
						foreach($content['overviewcontent'] as $overview){
							PALog::log("ParseJson::parse: overview-key: overview:".$overview['key']);
							switch($overview['key']){
								case 'LearningObjectivesInfoJSON':
									ParseJSON::parseObjectives($overview['value']);
									break;

								case 'TargetAudienceInfoJSON':
									ParseJSON::parseAudience($overview['value']);
									break;
							}
						}
					}

					if (isset($content['cmeinfocontent'])){
						foreach($content['cmeinfocontent'] as $cmeinfo){
							if ($cmeinfo['key'] == 'CMEInfoJson' || $cmeinfo['key'] == 'CMEInfoJSON'){
								if (isset($cmeinfo['value']['availableCreditTypes'])){
									foreach($cmeinfo['value']['availableCreditTypes'] as $cme){
										ParseJson::$data['accreditations'][] = $cme;
									}
								}
							}
						}
					}
					if ($id == NULL){
						$html .= RenderSummaryHTML::render(ParseJson::$config, ParseJson::$data);
					}else{
						$html = RenderDetailHTML::render(ParseJson::$config, ParseJson::$data);
					}
				}
			}
		}
		PALog::log("ParseJson::parse: complete:");
		return $html;
		//return ParseJson::$data['dump'];
	}
}
