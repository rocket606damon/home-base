<?php

define('VERSION', 'v0.2');
define('CLASS_DIR', 'classes/');
define('VENDOR_DIR', 'vendor/');
define('SUMMARY', 'SUMMARY');
define('DETAIL', 'DETAIL');

define("USERNAME", "username");
define("PASSWORD", "password");
define("TOKEN", "Token");
define("CATEGORYNAME", "CategoryName");

define('DETAILDOCTEMPLATE', 'detaildoctemplate');
define('DETAILACCREDITATIONTEMPLATE', 'detaildoctemplate');
define('DETAILOBJECTIVETEMPLATE', 'detailobjectivetemplate');
define('DETAILMODULETEMPLATE', 'detailmoduletemplate');

define('SUMMARYDOCTEMPLATE', 'detaildoctemplate');
define('SUMMARYITEMTEMPLATE', 'summaryItemTemplate');
define('SUMMARYFEATUREDITEMTEMPLATE', 'summaryFeaturedItemTemplate');
define('SUMMARYCATEGORYTEMTEMPLATE', 'summaryCategoryTemplate');

define('TEMPLATEDIR', 'templateDir');

$config = array(
	'qcBaseUrl' => 'https://homebasetraining-qc.astutetech.com/netscoreapi/RestAPIService.svc/',
	'baseUrl' => 'https://homebasetraining.org/netscoreapi/RestAPIService.svc/',
	'getByCategoryUrl' => 'GetAvailableActivitiesByCategory',
	'authenticateUrl' => 'Authenticate',
	'version' => 'v0.2',
	'versionDate' => '2017-04-05T17:03:00',
	'username' => 'api-lpcms',
	'password' => 'l@nChpA!',
	'category' => 'MGHHomeBase',

	'templateDir' => 'templates/',

	'summaryDocTemplate' => 'summary.doc.template.html',
	'summaryItemTemplate' => 'summary.item.template.html',
	'summaryFeaturedItemTemplate' => 'summary.featureditem.template.html',
	'summaryCategoryTemplate' => 'summary.category.template.html',

	'detailDocTemplate' => 'detail.doc.template.html',
	'detailAccreditationTemplate' => 'detail.accreditation.template.html',
	'detailObjectiveTemplate' => 'detail.objective.template.html',
	'detailModuleTemplate' => 'detail.Module.template.html',

	'cacheDir' => 'cache/',
	'cacheFile' => 'current_data.json',
	'cacheMinutes' => 60,
	'imageBaseUrl' => 'media/courses/',

	'debug' => true,
	'test' => true
);
