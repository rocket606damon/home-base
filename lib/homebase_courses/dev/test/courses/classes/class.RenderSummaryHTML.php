<?php

class RenderSummaryHtml{
	public static function render($config, $data){

		$templateDir = $config['templateDir'];
		PALOG::log('RenderSummaryHtml:__construct');

		$bodyTemplateName = $templateDir.$config['summaryDocTemplate'];
		$itemTemplateName = $templateDir.$config['summaryItemTemplate'];
		$featuredItemTemplateName = $templateDir.$config['summaryFeaturedItemTemplate'];
		$categoryTemplateName = $templateDir.$config['summaryCategoryTemplate'];

		$bodyTemplate = file_get_contents($bodyTemplateName);
		$itemTemplate = file_get_contents($itemTemplateName );
		$featuredItemTemplate = file_get_contents($featuredItemTemplateName );
		$categoryTemplate = file_get_contents($categoryTemplateName );

		$categoryAttrs = array(
			"number" => 'text',
			"title" => 'text',
			"id" => 'text',
			"photo" => 'text',
			"htmlItems" => 'text',
			"title" => 'text'
		);

		$featuredItemAttrs = array(
			"credits" => 'text',
			"hours" => 'text',
			"id" => 'text',
			"photo" => 'text',
			"title" => 'text'
		);

		$itemAttrs = array(
			"credits" => 'text',
			"hours" => 'text',
			"id" => 'text',
			"photo" => 'text',
			"title" => 'text',
		);

		$headAttrs = array(
			"featuredCourses",
			"mainCourses" => 'text',
			"category" => 'text',
			"htmlCategories" => 'text',
			"htmlFeaturedItems" => 'text'
		);

		$data['htmlItems'] = renderFromTemplate::renderList($data['items'], $itemAttrs, $itemTemplate);
		//$data['htmlFeaturedItems'] = renderFromTemplate::renderList($data['featuredItems'],
		//		$featuredItemAttrs, $itemFeaturedTemplate);

		PALog::log("ParseJson::parse: found: overview:".print_r($data, 1));

		$html = renderFromTemplate::render ($data, $headAttrs, $bodyTemplate);
		return $html;
	}
}
