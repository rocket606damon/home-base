<?php

class RenderDetailHtml{

	public static function render($config, $data){
		global $render;
		$bodyTemplateName = $config['templateDir'].$config['detailDocTemplate'];
		$accrTemplateName = $config['templateDir'].$config['detailAccreditationTemplate'];
		$moduleTemplateName = $config['templateDir'].$config['detailModuleTemplate'];
		$objectiveTemplateName = $config['templateDir'].$config['detailObjectiveTemplate'];

		PALog::log("RenderDetailHtml:__construct:bodyTemplateName:". $bodyTemplateName);
		$bodyTemplate = file_get_contents($bodyTemplateName);

		PALog::log("RenderDetailHtml:__construct:accrTemplateName:". $accrTemplateName);
		$accrTemplate = file_get_contents($accrTemplateName );

		PALog::log("RenderDetailHtml:__construct:moduleTemplateName:". $moduleTemplateName);
		$moduleTemplate = file_get_contents($moduleTemplateName );

		PALog::log("RenderDetailHtml:__construct:objectiveTemplateName:". $objectiveTemplateName);
		$objectiveTemplate = file_get_contents($objectiveTemplateName );

		$headAttrs = array(
			"id" => 'text',
			"intro" => 'text',
			"image" => 'text',
			"format" => 'text',
			"instructors" => 'array',
			"htmlObjectives" => 'text',
			"targetaudience" => 'text',
			"time" => 'text',
			"title" => 'text',
			"credits" => 'text',
			"htmlAccreditations" => 'text',
			"pagename" => 'text',
			"registerUrl" => 'text'
		);
		$accreditationAttrs = array(
			"accreditationNumber" => 'text',
			"accreditationStatment" => 'text',
			"creditType" => 'text',
			"credits" => 'text',
			"designationStatment" => 'text',
			"oddeven" => 'text'
		);
		$objectiveAttrs = array(
			"intro" => 'text',
			"htmlObjectives" => 'text'
		);

		$data['htmlObjectives'] = '';
		if (count($data['modules'])){
			foreach($data['modules'] as $module){
				$odata = array();
				$odata['intro'] = $module['moduleIntro'];
				$odata['htmlObjectives'] = '';
				foreach($module['moduleObjectives'] as $m){
					$odata['htmlObjectives'] .= "                <li>$m</li>\n";
				}
				//return print_r($module['moduleObjectives'], 1);
				$data['htmlObjectives'] .= renderFromTemplate::render($odata, $objectiveAttrs, $objectiveTemplate);
			}
		}else if (isset($data['objectives'])){
			$dump .= "objectives:".print_r($data, 1);
			$odata = array();
			$odata['intro'] = $data['intro'];
			$odata['htmlObjectives'] = '';
			foreach($data['objectives'] as $m){
				$odata['htmlObjectives'] .= "                <li>$m</li>\n";
			}
			$data['htmlObjectives'] = renderFromTemplate::render($odata, $objectiveAttrs, $objectiveTemplate);
		}
		//$data['htmlObjectives'] .= print_r($data, 1);
		$acccount = count($data['accreditations']);
		for ($i = 0; $i < $acccount; $i++){
			if (empty($data['credits'])){
				$data['credits'] = $data['accreditations'][$i]['credits'];
				$data['hours'] = $data['accreditations'][$i]['credits'];
			}
			$data['accreditations'][$i]['oddeven'] = ($i % 2 == 0) ? "even":"odd";
			$data['accreditations'][$i]['accreditationNumber'] = $i+1;
		}

		$data['targetaudience'] =  isset($data['targetaudience']) ?
				implode(", ", $data['targetaudience']) : '';
		$data['image'] = $config['imageBaseUrl']. 'HPC_' . $data['id'] . "_full.png";
		$data['pagename'] = "Course Information";
		$data['registerUrl'] = "#";
		$data['accreditations']  = isset($data['accreditations']) ? $data['accreditations'] : array();
		$data['htmlAccreditations'] = renderFromTemplate::renderList(
				$data['accreditations'], $accreditationAttrs, $accrTemplate);
		$html = renderFromTemplate::render ($data,$headAttrs,$bodyTemplate);
		return $html;
	}
}
