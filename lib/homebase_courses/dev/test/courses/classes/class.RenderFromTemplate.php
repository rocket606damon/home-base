<?php

class renderFromTemplate{
	public static function render($data, $fields, $html){
		foreach($fields as $title => $type){
			if ($type == 'text'){
				$dataValue = isset($data[$title]) ? $data[$title] : '';
				$html = str_replace('{{' . $title . '}}', $dataValue, $html );
			}else if ($type == 'array'){
				$dataValue = isset($data[$title]) ? $data[$title] : array();
				$listHtml = '';
				foreach($dataValue as $value){
					$format = "\t\t<li>{{value}}</li>\n";
					$listHtml .= str_replace('{{value}}', $value, $format);
				}
				$html = str_replace('{{' . $title . '}}', $listHtml, $html );
			}
		}
		return $html;
	}

	public static function renderList($data, $fields, $template){
		$html = '';
		for($i = 0; $i < count($data); $i++){
			$item = $data[$i];
			$fields['number'] = $i + 1;
			$html .= renderFromTemplate::render($item, $fields, $template);
		}
		return $html;
	}
}
