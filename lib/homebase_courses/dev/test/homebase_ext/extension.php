<?php

ini_set('memory_limit', '2048M');

$base_dir = dirname(__FILE__).'\\';
define('BASE_DIR', $base_dir);
define('CONFIG_FILE', BASE_DIR . 'config\config.php');

require_once(CONFIG_FILE);
require_once(CLASS_DIR."class.HomebaseCourseInfo.php");
require_once(CLASS_DIR."class.PsychAcademyReadData.php");
require_once(CLASS_DIR."class.RenderDetailHTML.php");
require_once(CLASS_DIR."class.RenderSummaryHTML.php");
require_once(CLASS_DIR."class.ParseJSON.php");
require_once(CLASS_DIR."class.RenderFromTemplate.php");
require_once(CLASS_DIR."class.PALog.php");

require_once(VENDOR_DIR."unirest/src/Unirest.php");

function homebaseGetList($categoryId=null){
	global $config;

	$jsonData = getJson();
	$html = '';

	PALog::log("renderHTML: DETAIL called for category [[$categoryId]]");
	$html = ParseJSON::parse($config, $jsonData, SUMMARY, $categoryId);
    return $html;
}

function homebaseGetDetail($id){
	global $config;

	$jsonData = getJson();
	$html = '';

	PALog::log("renderHTML: DETAIL called");
	$html = ParseJSON::parse($config, $jsonData, DETAIL, $id);

    // return output
    return $html;
}


function getJson($category=null){
	global $config;

	$cacheDir = BASE_DIR.'cache\\';
	$cachePath = $cacheDir.$config['cacheFile'];
	if (file_exists($cachePath) && (filemtime($cachePath) > (time() - 60 * $config['cacheMinutes'] ))) {
	   $response = json_decode(file_get_contents($cachePath),1);
	} else {
	   $response = PsychAcademyReadData::read($config);
	   file_put_contents($cachePath, json_encode($response,1), LOCK_EX);
	}
	return $response;
}

echo homebaseGetList();

