<?php

class RenderDetailHtml{
	public static function render($config1, $data){
		global $config;
		$templateDir = $config['templateDir'];

		$bodyTemplateName = $templateDir.$config['detailDocTemplate'];
		$accrTemplateName = $config['templateDir'].$config['detailAccreditationTemplate'];

		PALog::log("RenderDetailHtml:__construct:bodyTemplateName:". $bodyTemplateName);
		$bodyTemplate = file_get_contents($bodyTemplateName);

		PALog::log("RenderDetailHtml:__construct:accrTemplateName:". $accrTemplateName);
		$accrTemplate = file_get_contents($accrTemplateName );

		$headAttrs = array(
			"id" => 'text',
			"image" => 'text',
			"format" => 'text',
			"instructors" => 'array',
			"targetaudience" => 'text',
			"time" => 'text',
			"title" => 'text',
			"credits" => 'text',
			"htmlAccreditations" => 'text',
			"htmlObjectives" => 'text',
			"pagename" => 'text',
			"registerUrl" => 'text'
		);
		$objectiveAttrs = array(
			"intro" => 'text',
			"objectives" => 'list',
		);
		$accreditationAttrs = array(
			"accreditationNumber" => 'text',
			"accreditationStatment" => 'text',
			"creditType" => 'text',
			"credits" => 'text',
			"designationStatment" => 'text',
			"oddeven" => 'text'
		);

		$data['htmlObjectives'] = '';
		if(count($data['modules'])){
			foreach ($data['modules'] as $module){
				$data['htmlObjectives'] .=
					"<p>". $module['intro'] . "</p>\n".
					"<ul>\n" .
					"\t<li>".implode("</li>\n\t<li>", $module['objectives'])."</li>\n".
					"</ul>\n";
			}
		}else{
			$data['htmlObjectives'] =
				"<p>".$data['intro']."</p>\n".
				"<ul>\n" .
				"\t<li>".implode("</li>\n\t<li>", $data['objectives'])."</li>\n".
				"</ul>\n";
		}

		$acccount = count($data['accreditations']);
		for ($i = 0; $i < $acccount; $i++){
			if (empty($data['credits'])){
				$data['credits'] = $data['accreditations'][$i]['credits'];
				$data['hours'] = $data['accreditations'][$i]['credits'];
			}
			$data['accreditations'][$i]['oddeven'] = ($i % 2 == 0) ? "even":"odd";
			$data['accreditations'][$i]['accreditationNumber'] = $i+1;
		}
		$imageDir = 'media/courses/';
		$imageFilename = 'HPC_' . $data['id'] . '_full.png';
		$imagePath = $imageDir. $imageFilename;
		$data['image'] = plugins_url( $imagePath, __FILE__ );

		$data['pagename'] = "Course Information";
		$data['registerUrl'] = "#";
		$data['accreditations']  = isset($data['accreditations']) ? $data['accreditations'] : array();
		$data['htmlAccreditations'] = renderFromTemplate::renderList(
				$data['accreditations'], $accreditationAttrs, $accrTemplate);
		$html = renderFromTemplate::render ($data,$headAttrs,$bodyTemplate);
		return $html;
	}
}
