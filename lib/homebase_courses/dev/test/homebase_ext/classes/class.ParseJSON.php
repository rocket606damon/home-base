<?php

class ParseJson{

	public static function getById($data, $id){
		PALog::log("ParseJson::getById: called:$id");

		$activityDetaillist = $data['activityDetaillist'];
		foreach($activityDetaillist as $item){
			if ($item['id'] == $id){
				PALog::log("ParseJson::getById: found item ($id)");
				return $item;
			}
		}
		PALog::log("ParseJson::getById: complete");
		return array();
	}

	public static function parse($config, $response, $type, $id=NULL){
		PALog::log("ParseJson::parse: called");
		PALog::log("ParseJson::parse: activityDetaillist");
		PALog::log("ParseJson::parse: activityDetaillist");
		$sections = HomebaseCourseInfo::getAllSections();

		if (isset($response['activityDetaillist'])){

			$activityDetaillist = $response['activityDetaillist'];
			PALog::log("ParseJson::parse: found: activityDetaillist");
			PALog::log("ParseJson::parse: item count:".count($activityDetaillist));
			$number = 1;
			$sectionNumber = 1;
			$fullHTML = '';

			foreach($sections as $collapseNumber => $section){

				$sdata = array();
				$sdata['SectionTitle'] = $section['title'];
				$sdata['SectionId'] = $section['id'];
				$sdata['SectionNumber'] = $sectionNumber;
				$sdata['CollapseNumber'] = $collapseNumber;
				$sdata['SectionHTML'] = '';
				$html = '';

				foreach($activityDetaillist as $content){
					if ($id == NULL ||
							($type == DETAIL && $content['id'] == $id) ||
							($type == SUMMARY && HomebaseCourseInfo::courseInSection($id, $content['id']))
					){
						PALog::log("ParseJson::parse: next...");
						$data['id'] = isset($content['id'])? $content['id'] : '';
						$data['title'] = isset($content['title'])? $content['title'] : '';
						$data['format'] = isset($content['CustomActivityType'])? $content['CustomActivityType'] : '';
						$data['instructors'] = array();
						$data['credits'] = '';
						$data['items'] = array();
						$data['featuredItems'] = array();
						$data['time'] = '';
						$data['hours'] = '';
						$data['photo'] = '';
						$data['intro'] = '';
						$data['number'] = $number++;
						$data['targetaudience'] = '';
						$data['objectives'] = array();
						$data['accreditations'] = array();

						if (isset($content['facultylists'])){
							foreach($content['facultylists'] as $facutlyMember){
								$data['instructors'][] = $facutlyMember['name'];
							}
						}

						if (isset($content['overviewcontent'])){
							foreach($content['overviewcontent'] as $overview){

								PALog::log("ParseJson::parse: overview-key: overview:".$overview['key']);
								switch($overview['key']){

									case 'LearningObjectivesJSON':
										$data['intro'] = $overview['value']['intro'];
										$objectives = $overview['value']['learningObjectives'];
										if (!is_array($objectives[0]['learningObjective'])){
											// text: no modules.

											foreach($objectives as $o){
												$data['objectives'][] = $o['learningObjective'];
											}
										}else{
											foreach($objectives as $m){
												$module = $m['learningObjective'];
												$mdata = array();
												$mdata['intro'] = $module['moduleIntro'];
												foreach($module['moduleLearningObjectives'] as $m){
													$mdata['objectives'][] = $m['moduleLearningObjective'];
												}
												$data['modules'][] = $mdata;
											}
										}
										break;
									case 'TargetAudienceJSON':
										if (isset($overview['value']['targetAudiences'])){
											$targetAudience = array();
											foreach($overview['value']['targetAudiences'] as $targetAudience){
												$targetaudience[] = $targetAudience['targetAudience'];
											}
											$data['targetaudience'] = implode(", ", $targetaudience);
										}
										break;
								}
							}
						}

						if (isset($content['cmeinfocontent'])){
							foreach($content['cmeinfocontent'] as $cmeinfo){
								switch($cmeinfo['key']){
									case 'CMEInfoJSON':
										if (isset($cmeinfo['value']['availableCreditTypes'])){
											foreach($cmeinfo['value']['availableCreditTypes'] as $cme){
												$data['accreditations'][] = $cme;
											}
											$data['credits'] = isset($data['accreditations'][0]['credits']) ?
												$data['accreditations'][0]['credits'] : '';
											$data['time'] = $data['credits'];
											$data['hours'] = $data['credits'];
											break;
									}
								}
							}
						}
						switch ($type){
							case 'SUMMARY':
								$html .= RenderSummaryHTML::renderItem($config, $data);
								break;
							case 'DETAIL':
								$html = RenderDetailHTML::render($config, $data);
								break;
						}
					}
				}
				$sdata['SectioHTML'] .= $html;
				$fullHtml .= RenderSummaryHTML::renderSection($config, $sdata);
			}
		}
		if ($type == SUMMARY){
			$data['htmlItems'] = $fullHMtml;
			$html = RenderSummaryHTML::renderBody($config, $data);
			PALog::log("ParseJson::parse: complete:");
		}
		return $html;
	}

	public static function array_collapse($array, $attr){
		$return = array();
		foreach($array as $a){
			if (isset($a[$attr])){
				$return[] = $a[$attr];
			}
		}
		return $return;
	}
}
