<?php

$homebaseCourseInfo = array(

	'sections' => array(
		"Collapse1"  => array(
			"3070",
			"3043",
			"3152",
			"3050",
			"3022",
			"3049",
			"3048",
			"3021",
			"2669"
		),
		"Collapse2"  => array(
			"3035",
			"3029",
			"3034",
			"3051",
			"3045",
			"3025",
			"3031",
			"3032",
			"3042",
			"3024",
			"2669"
		),
		"Collapse3"  => array(
			"3035",
			"3051",
			"3030",
			"3180",
			"2669"
		),
		"Collapse4"  => array(
			"3045",
			"3038",
			"2669",
			"3070",
			"2669"
		),
		"Collapse5"  => array(
			"3039",
			"3045",
			"3070",
			"3040",
			"3033",
			"3072",
			"3041",
			"3036",
			"3046",
			"3037",
			"3038"
		),
		"Collapse6"  => array(
			"2669",
			"3026",
			"3047",
			"3028",
			"3025",
			"3044",
			"3071",
			"3028",
			"2669"
		)
	),

	'audiences' => array(
		"2669" => array(),
		"2669" => array("Professionals", "Community", "Families"),
		"3021" => array("Professionals", "Community", "Families"),
		"3022" => array("Professionals", "Community", "Families"),
		"3024" => array("Professionals", "Community", "Families"),
		"3025" => array("Professionals", "Community", "Families"),
		"3026" => array("Professionals", "Community", "Families"),
		"3028" => array("Professionals", "Community", "Families"),
		"3029" => array("Professionals", "Community", "Families"),
		"3030" => array("Professionals", "Community"),
		"3031" => array("Professionals"),
		"3032" => array("Professionals"),
		"3033" => array("Professionals", "Community", "Families"),
		"3034" => array("Professionals"),
		"3035" => array("Professionals", "Community", "Families"),
		"3036" => array("Professionals", "Community", "Families"),
		"3037" => array("Professionals", "Community", "Families"),
		"3038" => array("Professionals", "Community", "Families"),
		"3039" => array("Professionals", "Community", "Families"),
		"3040" => array("Professionals", "Community", "Families"),
		"3041" => array("Professionals", "Community", "Families"),
		"3042" => array("Professionals"),
		"3043" => array("Professionals", "Community", "Families"),
		"3044" => array("Professionals", "Community", "Families"),
		"3045" => array("Professionals"),
		"3046" => array("Professionals"),
		"3047" => array("Professionals"),
		"3048" => array("Professionals", "Community", "Families"),
		"3049" => array("Professionals", "Community", "Families"),
		"3050" => array("Professionals", "Community", "Families"),
		"3051" => array("Professionals"),
		"3070" => array("Professionals"),
		"3071" => array("Professionals", "Community", "Families"),
		"3072" => array("Professionals"),
		"3152" => array("Professionals", "Community", "Families"),
		"3180" => array("Professionals")
	),
	'sectionInfo' => array(
		"Collapse0"  => array('title'=>"Military Culture and Intro Courses",  'id'=>"military-culture"),
		"Collapse1"  => array('title'=>"PTSD Courses",  'id'=>"ptsd-courses"),
		"Collapse2"  => array('title'=>"TBI Courses",  'id'=>"tbi-courses"),
		"Collapse3"  => array('title'=>"Substance Use Courses",  'sectionid'=>"substance-use"),
		"Collapse4"  => array('title'=>"Additional Trauma and Treatment Courses",  'id'=>"additional-trauma"),
		"Collapse5"  => array('title'=>"Military Family Courses",  'id'=>"military-family")
	)
);

class HomebaseCourseInfo{

	private static $info;

	public static function setData($info){
		HomebaseCourseInfo::$info = $info;
	}

	private static function getFull($index){
		return isset(HomebaseCourseInfo::$info[$index]) ?
				HomebaseCourseInfo::$info[$index] : array();
	}

	private static function get($index, $id){
		return isset(HomebaseCourseInfo::$info[$index][$id]) ?
				HomebaseCourseInfo::$info[$index][$id] : array();
	}

	public static function getCoursesBySection($sectionId){
		return HomebaseCourseInfo::get('sections', $sectionId);
	}

	public static function courseInSection($sectionId, $courseId){
		return count(HomebaseCourseInfo::get('sections', $sectionId)) ? true : false;
	}

	public static function getCourseAudiences($courseId){
		return HomebaseCourseInfo::get('audiences', $courseId);
	}

	public static function getSectionInfo($sectionId){
		return HomebaseCourseInfo::get('sectionInfo', $sectionId);
	}

	public static function getAllSections(){
		return HomebaseCourseInfo::getFull('sectionInfo');
	}
}
HomebaseCourseInfo::setData($homebaseCourseInfo);

