<?php

class RenderSummaryHtml{

	public static function renderItem($config, $data){
		$templateDir = $config['templateDir'];
		$imageDir = 'media/courses/';

		PALOG::log('RenderSummaryHtml:__construct');

		$itemTemplateName = $templateDir.$config['summaryItemTemplate'];
		$itemTemplate = file_get_contents($itemTemplateName );
		$itemAttrs = array(
			"credits" => 'text',
			"hours" => 'text',
			"id" => 'text',
			"number" => 'text',
			"href" => 'text',
			"image" => 'text',
			"title" => 'text'
		);

		$thumbnailFilename = 'HPC_' . $data['id'] . '_thumb.png';
		$thumbnailPath = $imageDir. $thumbnailFilename;
		$data['image'] = plugins_url( $thumbnailPath, __FILE__ );

		// $data['href'] = get_page_link();
		$data['href'] = esc_url( add_query_arg( 'id', "DETAIL_".$data['id']));

		$html = renderFromTemplate::render($data, $itemAttrs, $itemTemplate);
		return $html;
	}

	public static function renderSection($config, $data){
		PALOG::log('RenderSummaryHtml:renderSection');
		$templateDir = $config['templateDir'];

		$sectionTemplateName = $templateDir.$config['summarySectionTemplate'];
		$sectionTemplate = file_get_contents($sectionTemplateName);
		$sectionAttrs = array(
			"SectionNumber" => 'text',
			"SectionTitle" => 'text',
			"CollapseNumber" => 'text',
			"SectionID" => 'text',
			"SectionHTML" => 'text'
		);
		$html = renderFromTemplate::render($data, $sectionAttrs, $sectionTemplate);
		return $html;
	}

	public static function renderBody($config, $data){
		PALOG::log('RenderSummaryHtml:renderBody');
		$templateDir = $config['templateDir'];

		$bodyTemplateName = $templateDir.$config['summaryDocTemplate'];
		$bodyTemplate = file_get_contents($bodyTemplateName);
		$bodyAttrs = array(
			"htmlItems" => 'text'
		);

		$html = renderFromTemplate::render($data, $bodyAttrs, $bodyTemplate);
		return $html;
	}
}
