<?php

define('CLASS_DIR', 'classes/');
define('VENDOR_DIR', 'vendor/');
define('SUMMARY', 'SUMMARY');
define('DETAIL', 'DETAIL');

define("USERNAME", "username");
define("PASSWORD", "password");
define("TOKEN", "Token");
define("CATEGORYNAME", "CategoryName");

define('DETAILDOCTEMPLATE', 'detaildoctemplate');
define('DETAILACCREDITATIONTEMPLATE', 'detaildoctemplate');

define('SUMMARYDOCTEMPLATE', 'detaildoctemplate');
define('SUMMARYSECTIONTEMPLATE', 'summarySectionTemplate');
define('SUMMARYITEMTEMPLATE', 'summaryItemTemplate');
define('SUMMARYFEATUREDITEMTEMPLATE', 'summaryFeaturedItemTemplate');
define('SUMMARYCATEGORYTEMTEMPLATE', 'summaryCategoryTemplate');

define('TEMPLATEDIR', 'templateDir');

$config = array(
	'qcBaseUrl' => 'https://homebasetraining-qc.astutetech.com/netscoreapi/RestAPIService.svc/',
	'baseUrl' => 'https://homebasetraining.org/netscoreapi/RestAPIService.svc/',
	'getByCategoryUrl' => 'GetAvailableActivitiesByCategory',
	'authenticateUrl' => 'Authenticate',
	'username' => 'api-lpcms',
	'password' => 'l@nChpA!',
	'category' => 'MGHHomeBase',

	'templateDir' => BASE_DIR."templates/",

	'summaryDocTemplate' => 'summary.doc.template.html',
	'summaryItemTemplate' => 'summary.item.template.html',
	'summarySectionTemplate' => 'summary.section.template.html',
	'summaryFeaturedItemTemplate' => 'summary.featureditem.template.html',
	'summaryCategoryTemplate' => 'summary.category.template.html',

	'detailDocTemplate' => 'detail.doc.template.html',
	'detailAccreditationTemplate' => 'detail.accreditation.template.html',

	'cacheDir' => BASE_DIR."cache/",
	'cacheFile' => 'current_data.json',
	'cacheMinutes' => 60,
	'imageBaseUrl' => BASE_DIR."courses/",

	'debug' => true,
	'test' => true
);
