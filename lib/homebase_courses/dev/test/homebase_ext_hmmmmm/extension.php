<?php
/*
* Plugin Name: MGH HomeBase Courses WordPress ShortCode
* Description: Acess MGH HomeBase Course Content
* Version: 1.0
* Author: Launchpad Media
* Author URI: http://launchpad.co
*/

function getlist_homebase_shortcode($atts = [], $content = null, $tag = ''){
	$hb_attrs= shortcode_atts( array(
		'id' => '',
    ), $atts, 'homebase');

    // normalize attribute keys, lowercase
    $hb_atts = array_change_key_case((array)$hb_atts, CASE_LOWER);

    // get the course id
    $category_id = $hb_atts['id'];

    $html = '';

	// load the html for Homebase course list page

    // return output
    return $html;
}

function getfeatures_homebase_shortcode(){
    $html = '';

	// load the html for Homebase course list page

    // return output
    return $html;
}

function getdetails_homebase_shortcode(_shortcode$atts = [], $content = null, $tag = ''){
	$hb_attrs= shortcode_atts( array(
		'id' => '',
    ), $atts, 'homebase');

    // normalize attribute keys, lowercase
    $hb_atts = array_change_key_case((array)$hb_atts, CASE_LOWER);

    // get the course id
    $course_id = $hp_atts['course_id'];

    // start output
    $html = '';

	// load the html for a Homebase course details page

    // return output
    return $html;
}

function homebase_shortcodes_init(){
	add_shortcode('getfeatures', 'getfeatures_homebase_shortcode');
	add_shortcode('getlist', 'getlist_homebase_shortcode');
	add_shortcode('getdetails', 'getdetails_homebase_shortcode');
}

add_action('init', 'mgh_homebasecourses_shortcodes_init');
?>