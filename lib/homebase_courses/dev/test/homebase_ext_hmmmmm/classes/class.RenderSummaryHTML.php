<?php

class RenderSummaryHtml{
	public static function renderItem($config, $data){

		$templateDir = $config['templateDir'];
		PALOG::log('RenderSummaryHtml:__construct');

		$itemTemplateName = $templateDir.$config['summaryItemTemplate'];
		$itemTemplate = file_get_contents($itemTemplateName );
		$itemAttrs = array(
			"credits" => 'text',
			"hours" => 'text',
			"id" => 'text',
			"number" => 'text',
			"photo" => 'text',
			"title" => 'text',
		);

		$html = renderFromTemplate::render($data, $itemAttrs, $itemTemplate);
		return $html;
	}
	public static function renderBody($config, $data){

		$templateDir = $config['templateDir'];
		PALOG::log('RenderSummaryHtml:__construct');

		$bodyTemplateName = $templateDir.$config['summaryDocTemplate'];
		$bodyTemplate = file_get_contents($bodyTemplateName);
		$bodyAttrs = array(
			"htmlItems" => 'text'
		);

		$html = renderFromTemplate::render($data, $bodyAttrs, $bodyTemplate);
		return $html;
	}
}
