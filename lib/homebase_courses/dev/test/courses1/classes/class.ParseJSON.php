<?php

class ParseJson{

	public static function getById($data, $id){
		PALog::log("ParseJson::getById: called:$id");

		$activityDetaillist = $data['activityDetaillist'];
		foreach($activityDetaillist as $item){
			if ($item['id'] == $id){
				PALog::log("ParseJson::getById: found item ($id)");
				return $item;
			}
		}
		PALog::log("ParseJson::getById: complete");
		return array();
	}

	public static function parse($config, $response, $id=NULL){
		PALog::log("ParseJson::parse: called");
		PALog::log("ParseJson::parse: activityDetaillist");

		$html = '';

		if (isset($response['activityDetaillist'])){

			$activityDetaillist = $response['activityDetaillist'];
			PALog::log("ParseJson::parse: found: activityDetaillist");
			PALog::log("ParseJson::parse: item count:".count($activityDetaillist));
			foreach($activityDetaillist as $content){
				if ($id == NULL || $content['id'] == $id){
					PALog::log("ParseJson::parse: next...");
					$data['id'] = isset($content['id'])? $content['id'] : '';
					$data['title'] = isset($content['title'])? $content['title'] : '';
					$data['format'] = isset($content['CustomActivityType'])? $content['CustomActivityType'] : '';
					$data['instructors'] = array();
					$data['credits'] = '';
					$data['items'] = array();
					$data['featuredItems'] = array();
					$data['time'] = '';
					$data['hours'] = '';
					$data['photo'] = '';
					$data['intro'] = '';
					$data['targetaudience'] = '';
					$data['objectives'] = array();
					$data['accreditations'] = array();

					if (isset($content['facultylists'])){
						foreach($content['facultylists'] as $facutlyMember){
							$data['instructors'][] = $facutlyMember['name'];
						}
					}

					if (isset($content['overviewcontent'])){
						foreach($content['overviewcontent'] as $overview){
							PALog::log("ParseJson::parse: overview-key: overview:".$overview['key']);
							switch($overview['key']){
								case 'LearningObjectivesInfoJSON':
									$data['intro'] = isset($overview['value']['intro']) ? $overview['value']['intro'] : '';
									if (isset($overview['value']['learningObjectives'])){
										foreach($overview['value']['learningObjectives'] as $learningObjective){
											$data['objectives'][] = $learningObjective['learningObjective'];
										}
									}
									break;
								case 'TargetAudienceInfoJSON':
									if (isset($overview['value']['targetAudiences'])){
										$targetAudience = array();
										foreach($overview['value']['targetAudiences'] as $targetAudience){
											$targetaudience[] = $targetAudience['targetAudience'];
										}
										$data['targetaudience'] = implode(", ", $targetaudience);
									}
									break;
							}
						}
					}

					if (isset($content['cmeinfocontent'])){
						foreach($content['cmeinfocontent'] as $cmeinfo){
							switch($cmeinfo['key']){
								case 'CMEInfoJson':
									if (isset($cmeinfo['value']['availableCreditTypes'])){
										foreach($cmeinfo['value']['availableCreditTypes'] as $cme){
											$data['accreditations'][] = $cme;
										}
										break;
								}
							}
						}
					}
					if ($id == NULL){
						$html .= RenderSUmmaryHTML::render($config, $data);
					}else{
						$html = RenderDetailHTML::render($config, $data);
					}
				}
			}
		}
		PALog::log("ParseJson::parse: complete:");
		return $html;
	}
}
