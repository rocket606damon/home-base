<?php

class RenderDetailHtml{
	public static function render($config, $data){
		$bodyTemplateName = $config['templateDir'].$config['detailDocTemplate'];
		$accrTemplateName = $config['templateDir'].$config['detailAccreditationTemplate'];

		PALog::log("RenderDetailHtml:__construct:bodyTemplateName:". $bodyTemplateName);
		$bodyTemplate = file_get_contents($bodyTemplateName);

		PALog::log("RenderDetailHtml:__construct:accrTemplateName:". $accrTemplateName);
		$accrTemplate = file_get_contents($accrTemplateName );

		$headAttrs = array(
			"id" => 'text',
			"intro" => 'text',
			"image" => 'text',
			"format" => 'text',
			"instructors" => 'array',
			"objectives" => 'array',
			"targetaudience" => 'text',
			"time" => 'text',
			"title" => 'text',
			"credits" => 'text',
			"htmlAccreditations" => 'text',
			"pagename" => 'text',
			"registerUrl" => 'text'
		);
		$accreditationAttrs = array(
			"accreditationNumber" => 'text',
			"accreditationStatment" => 'text',
			"creditType" => 'text',
			"credits" => 'text',
			"designationStatment" => 'text',
			"oddeven" => 'text'
		);

		$acccount = count($data['accreditations']);
		for ($i = 0; $i < $acccount; $i++){
			if (empty($data['credits'])){
				$data['credits'] = $data['accreditations'][$i]['credits'];
				$data['hours'] = $data['accreditations'][$i]['credits'];
			}
			$data['accreditations'][$i]['oddeven'] = ($i % 2 == 0) ? "even":"odd";
			$data['accreditations'][$i]['accreditationNumber'] = $i+1;
		}
		$data['image'] = $config['imageBaseUrl']. 'HPC_' . $data['id'] . "_full.png";
		$data['pagename'] = "Course Information";
		$data['registerUrl'] = "#";
		$data['accreditations']  = isset($data['accreditations']) ? $data['accreditations'] : array();
		$data['htmlAccreditations'] = renderFromTemplate::renderList(
				$data['accreditations'], $accreditationAttrs, $accrTemplate);
		$html = renderFromTemplate::render ($data,$headAttrs,$bodyTemplate);
		return $html;
	}
}
