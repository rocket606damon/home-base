<?php

$homebaseCourseInfo = array(

		'sections' => array(

			//Military Culture and Intro Courses
			"Collapse0"  => array(
				3070=> "Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms",
				3043=> "Is there an app for that?",
				3152=> "Military Culture",
				3050=> "Physical Health and Mental Health Following Deployment",
				3022=> "Reintegration Issues From the Veterans Perspective: Overcoming the Stigma of Seeking Help",
				3049=> "Service Dogs, Acupuncture, Psychotherapy, and More: What's the Evidence and What's Evidence-based",
				3048=> "Student Veterans Health: What College Health Professionals and Faculty Need to Know",
				3021=> "The Challenges of Coming Home After War: What Providers Need to Know",
				2669=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS",
				2771=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11  Veterans - FIRST RESPONDERS",
			),
			// PTSD Courses
			"Collapse1"  => array(
				3035=> "Challenges of Treating Co-Morbid PTSD and TBI",
				3029=> "Clinical Practice Guidelines and Resources for PTSD Treatment",
				3034=> "Cognitive Processing Therapy for PTSD",
				3051=> "Clinical Case Conference - Recognizing and Treating Mild TBI and PTSD",
				3045=> "Clinical Case Conference - When Substance Abuse and PTSD Collide",
				3025=> "Keeping Military Families Emotionally Strong: Couples Therapy for PTSD",
				3031=> "Prolonged Exposure and Virtual Reality Therapy for PTSD",
				3032=> "Psychopharmacology of PTSD",
				3042=> "PTSD Diagnosis and DSM-V",
				3024=> "Recognizing PTSD and Co-Morbidities",
				2669=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS",
				2771=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11  Veterans - FIRST RESPONDERS",
			),
			// TBI Courses
			"Collapse2"  => array(
				3035=> "Challenges of Treating Co-Morbid PTSD and TBI",
				3051=> "Clinical Case Conference - Recognizing and Treating Mild TBI and PTSD",
				3030=> "Traumatic Brain Injury",
				3180=> "Mild Traumatic Brain Injury: Acute Management, Differential Diagnosis, Treatment, and Rehabilitation (April 2017)",
				2669=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS",
				2771=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11  Veterans - FIRST RESPONDERS",
			),
			// Substance Use Courses
			"Collapse3"  => array(
				3045=> "Clinical Case Conference - When Substance Abuse and PTSD Collide",
				3038=> "Substance Abuse",
				3070=> "Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms",
				2669=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS",
				2771=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11  Veterans - FIRST RESPONDERS",
			),
			// "Additional Trauma and Treatment Courses"
			"Collapse4"  => array(
				2669=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS",
				2771=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11  Veterans - FIRST RESPONDERS",
				// 3023=> "When One Family Member Serves the Entire Family Serves",
				3027=> "Supporting Resilience in Military Connected Children: The PACT Model",
				//3028=> "Impact of Combat-Related Injury, Illness, and Death on Military Children and Families",
				3033=> "Military Sexual Trauma",
				3036=> "Recognizing Suicide Risk in Returning Veterans",
				3037=> "Sleep Issues in Returning Veterans",
	            3038=> "Substance Abuse",
				3039=> "Aggression and Domestic Violence",
				3040=> "Managing Grief and Loss in Returning Veterans and Families",
				3041=> "Pain Issues in Returning Veterans",
				3045=> "Clinical Case Conference - When Substance Abuse and PTSD Collide",
				3046=> "Reproductive Mental Health Care in Younger and Older Female Veterans",
				3070=> "Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms",
				3072=> "Opioid Use Disorders: Considerations in Medication Assisted Treatments of Military Veterans",
			),
			// "Military Family Courses"
			"Collapse5"  => array(
				2771=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11  Veterans - FIRST RESPONDERS",
				3023=> "When One Family Member Serves the Entire Family Serves",
				3025=> "Keeping Military Families Emotionally Strong: Couples Therapy for PTSD",
				3026=> "Challenges Facing Other Family Members When a Veteran has PTSD",
				3027=> "Supporting Resilience in Military Connected Children: The PACT Model",
				3028=> "Impact of Combat-Related Injury, Illness, and Death on Military Children and Families",
				3044=> "Staying Strong: Helping Families and Schools Build Resilience in Military-Connected Children",
				3047=> "Clinical Case Conference - When a Parent has PTSD: Restoring Resilience in Parents and Their Children",
				3071=> "Suicide Postvention: Survivor Care for Families of Service Members and Veterans",
			)
	),

	'allCourses' => array(
		2669 => array('title' => "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9\/11 Veterans - HEALTH CARE PROFESSIONALS",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		2771 => array('title' => "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9\/11  Veterans - FIRST RESPONDERS",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3021 =>  array('title' => "The Challenges of Coming Home After War: What Providers Need to Know",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3022 => array('title' => "Reintegration Issues From the Veterans Perspective: Overcoming the Stigma of Seeking Help",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3023 => array('title' => "When One Family Member Serves the Entire Family Serves",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3024 => array('title' => "Recognizing PTSD and Co-Morbidities",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3025 => array('title' => "Keeping Military Families Emotionally Strong: Couples Therapy for PTSD",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3026 => array('title' => "Challenges Facing Other Family Members When a Veteran has PTSD",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3027 => array('title' => "Supporting Resilience in Military Connected Children: The PACT Model",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3028 => array('title' => "Impact of Combat-Related Injury, Illness, and Death on Military Children and Families",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3029 => array('title' => "Clinical Practice Guidelines and Resources for PTSD Treatment",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3030 => array('title' => "Traumatic Brain Injury",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3031 => array('title' => "Prolonged Exposure and Virtual Reality Therapy for PTSD",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3032 => array('title' => "Psychopharmacology of PTSD",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3033 => array('title' => "Military Sexual Trauma",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3034 => array('title' => "Cognitive Processing Therapy for PTSD",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3035 => array('title' => "Challenges of Treating Co-Morbid PTSD and TBI",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3036 => array('title' => "Recognizing Suicide Risk in Returning Veterans",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3037 => array('title' => "Sleep Issues in Returning Veterans",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3038 => array('title' => "Substance Abuse",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3039 => array('title' => "Aggression and Domestic Violence",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3040 => array('title' => "Managing Grief and Loss in Returning Veterans and Families",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3041 => array('title' => "Pain Issues in Returning Veterans",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3042 => array('title' => "PTSD Diagnosis and DSM-V",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3043 => array('title' => "Is there an app for that?",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3044 => array('title' => "Staying Strong: Helping Families and Schools Build Resilience in Military-Connected Children",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3045 => array('title' => "Clinical Case Conference - When Substance Abuse and PTSD Collide",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3046 => array('title' => "Reproductive Mental Health Care in Younger and Older Female Veterans",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3047 => array('title' => "Clinical Case Conference - When a Parent has PTSD: Restoring Resilience in Parents and Their Children",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3048 => array('title' => "Student Veterans Health: What College Health Professionals and Faculty Need to Know",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3049 => array('title' => "Service Dogs, Acupuncture, Psychotherapy, and More: What's the Evidence and What's Evidence-based",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3050 => array('title' => "Physical Health and Mental Health Following Deployment",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3051 => array('title' => "Clinical Case Conference - Recognizing and Treating Mild TBI and PTSD",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3070 => array('title' => "Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3071 => array('title' => "Suicide Postvention: Survivor Care for Families of Service Members and Veterans",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3072 => array('title' => "Opioid Use Disorders: Considerations in Medication Assisted Treatments of Military Veterans",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3152 => array('title' => "Military Culture",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3180 => array('title' => "Mild Traumatic Brain Injury: Acute Management, Differential Diagnosis, Treatment, and Rehabilitation (April 2017)",
			'blueboxText' => "Optional Detail Page Text", 'unavailable' => false, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
	),

	'audiences' => array(
		2669 => array("Professionals", "Responders"),
		2771 => array("Community", "Responders", "Families"),
		3021 => array("Professionals", "Community", "Responders", "Families"),
		3022 => array("Professionals", "Community", "Responders", "Families"),
		3023 => array("Community", "Families"),
		3024 => array("Professionals", "Community", "Responders", "Families"),
		3025 => array("Professionals", "Community", "Responders", "Families"),
		3026 => array("Professionals", "Community", "Responders", "Families"),
		3027 => array("Families"),
		3028 => array("Professionals", "Community", "Responders", "Families"),
		3029 => array("Professionals", "Community", "Responders", "Families"),
		3030 => array("Professionals", "Community", "Responders"),
		3031 => array("Professionals"),
		3032 => array("Professionals"),
		3033 => array("Professionals", "Community", "Responders", "Families"),
		3034 => array("Professionals"),
		3035 => array("Professionals", "Community", "Responders", "Families"),
		3036 => array("Professionals", "Community", "Responders", "Families"),
		3037 => array("Professionals", "Community", "Responders", "Families"),
		3038 => array("Professionals", "Responders"),
		3039 => array("Professionals", "Community", "Responders", "Families"),
		3040 => array("Professionals", "Community", "Responders", "Families"),
		3041 => array("Professionals", "Community", "Responders", "Families"),
		3042 => array("Professionals"),
		3043 => array("Professionals", "Community", "Responders", "Families"),
		3044 => array("Professionals", "Community", "Responders", "Families"),
		3045 => array("Professionals"),
		3046 => array("Professionals"),
		3047 => array("Professionals"),
		3048 => array("Professionals", "Community", "Responders", "Families"),
		3049 => array("Professionals", "Community", "Responders", "Families"),
		3050 => array("Professionals", "Community", "Responders", "Families"),
		3051 => array("Professionals"),
		3070 => array("Professionals"),
		3071 => array("Community", "Families"),
		3072 => array("Professionals"),
		3152 => array("Professionals", "Community", "Responders", "Families"),
		3180 => array("Professionals"),
	),
	'featuredItemsInfo' => array(
		3180=> "Mild Traumatic Brain Injury: Acute Management, Differential Diagnosis, Treatment, and Rehabilitation (April 2017)",
		3152=> "Military Culture",
		3070=> "Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms",
		3071=> "Suicide Postvention: Survivor Care for Families of Service Members and Veterans",
		3072=> "Opioid Use Disorders: Considerations in Medication Assisted Treatments of Military Veterans",
	),
	'sectionInfo' => array(
		"Collapse0"  => array('title'=>"Military Culture and Intro Courses",  'id'=>"military-culture"),
		"Collapse1"  => array('title'=>"PTSD Courses",  'id'=>"ptsd-courses"),
		"Collapse2"  => array('title'=>"TBI Courses",  'id'=>"tbi-courses"),
		"Collapse3"  => array('title'=>"Substance Use Courses",  'id'=>"substance-use"),
		"Collapse4"  => array('title'=>"Additional Trauma and Treatment Courses",  'id'=>"additional-trauma"),
		"Collapse5"  => array('title'=>"Military Family Courses",  'id'=>"military-family")
	),
	$audienceInfo = array(
		'Professionals' => 	array('slug' => "healthcare-professionals", 'class' => "professionals", 'id' => "Professionals", 'title' => "Healthcare Professionals"),
		'Responders' => 	array('slug' => "first-responders", 		'class' => "responders", 'id' => "Responders", 'title' => "First Responders"),
		'Families' => 		array('slug' => "military-families", 		'class' => "families", 'id' => "Families", 'title' => "Military Families"),
		'Community' => 		array('slug' => "community-members", 		'class' => "community", 'id' => "Community", 'title' => "Community Members")
	)
);

$newHomebaseCourseInfo = array(
	'sections' => array(),
	'allCourses' => array(),
	'audiences' => array(),
	'featuredItemsInfo' => array(),
	'sectionInfo' => array(
		"Collapse0"  => array('title'=>"Military Culture and Intro Courses",  'id'=>"military-culture"),
		"Collapse1"  => array('title'=>"PTSD Courses",  'id'=>"ptsd-courses"),
		"Collapse2"  => array('title'=>"TBI Courses",  'id'=>"tbi-courses"),
		"Collapse3"  => array('title'=>"Substance Use Courses",  'id'=>"substance-use"),
		"Collapse4"  => array('title'=>"Additional Trauma and Treatment Courses",  'id'=>"additional-trauma"),
		"Collapse5"  => array('title'=>"Military Family Courses",  'id'=>"military-family")
	),
	'audienceInfo' => array(
		'Professionals' => 	array('slug' => "healthcare-professionals", 'class' => "professionals", 'id' => "Professionals", 'title' => "Healthcare Professionals"),
		'Responders' => 	array('slug' => "furst-responders", 		'class' => "responders", 'id' => "Responders", 'title' => "First Responders"),
		'Families' => 		array('slug' => "military-families", 		'class' => "families", 'id' => "Families", 'title' => "Military Families"),
		'Community' => 		array('slug' => "community-members", 		'class' => "community", 'id' => "Community", 'title' => "Community Members")
	)
);

class HomebaseCourseInfo{

	private static $info;

	public static function setData($info){
		HomebaseCourseInfo::$info = $info;
	}

	private static function getFull($index){
		return isset(HomebaseCourseInfo::$info[$index]) ?
				HomebaseCourseInfo::$info[$index] : array();
	}

	private static function get($index, $id){
		return isset(HomebaseCourseInfo::$info[$index][$id]) ?
				HomebaseCourseInfo::$info[$index][$id] : array();
	}

	public static function getCoursesBySection($sectionId){
		return HomebaseCourseInfo::get('sections', $sectionId);
	}

	public static function courseInSection($sectionId, $courseId){
		return count(HomebaseCourseInfo::get('sections', $sectionId)) ? true : false;
	}

	public static function getCourseAudiences($courseId){
		return HomebaseCourseInfo::get('audiences', $courseId);
	}

	public static function getSectionInfo($sectionId){
		return HomebaseCourseInfo::get('sectionInfo', $sectionId);
	}

	public static function getAllSections(){
		return HomebaseCourseInfo::getFull('sectionInfo');
	}
	public static function getFeaturedItems(){
		return HomebaseCourseInfo::getFull('featuredItemsInfo');
	}
	public static function getAudienceInfo(){
		return HomebaseCourseInfo::getFull('audienceInfo');
	}

	public static function loadCourseInfo(){

		global $newHomebaseCourseInfo;

		foreach ($newHomebaseCourseInfo['sectionInfo'] as $sectionId => $section){
			$args = array('category_name' => $section['id']);
			$posts = get_posts($args);
			//echo "<!== " . print_r($posts, 1) . "==>\n";
			foreach($posts as $post){
				$newHomebaseCourseInfo['sections'][$sectionId][$post->ID] = $post->post_title;
			}
		}

		foreach ($newHomebaseCourseInfo['audienceInfo'] as $audienceId => $audience){
			$args = array('category_name' => $audience['slug']);
			$posts = get_posts( $args );
			//echo "<!== " . print_r($posts, 1) . "==>\n";
			foreach ($posts as $post){
				$newHomebaseCourseInfo['audiences'][$post->ID][$audienceId] = true;
			}
		}
	}
}

HomebaseCourseInfo::loadCourseInfo();

//echo "<!== " . print_r($newHomebaseCourseInfo, 1) . "==>\n";

HomebaseCourseInfo::setData($homebaseCourseInfo);

