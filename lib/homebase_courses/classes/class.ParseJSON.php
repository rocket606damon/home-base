<?php

class ParseJSON{

	public static function getJson(){
		global $config;

		$cacheDir = BASE_DIR.'cache/';
		$cachePath = $cacheDir.$config['cacheFile'];
		$rawPath = $cacheDir.$config['rawFile'];
		if (file_exists($cachePath) &&
				filemtime($cachePath) > (time() - 60 * $config['cacheMinutes'])) {
			$json = file_get_contents($cachePath);
			$courses = json_decode($json , 1);
		} else {
			$response = PsychAcademyReadData::read($config);
			$courses = ParseJSON::preParse($response);
			file_put_contents($rawPath, json_encode($response, 1), LOCK_EX);
			file_put_contents($cachePath, json_encode($courses, 1), LOCK_EX);
		}
		return $courses;
	}

	public static function preParse($response){
		global $config;

		$courses = array();

		if (isset($response['activityDetaillist'])){
			$rawCourses = $response['activityDetaillist'];
			$number = 1;
			foreach($rawCourses as $rawCourse){
				$course = ParseJSON::itemParse($rawCourse, $number++);
				$id = trim($course['id']);
				$courses[$id] = $course;
			}
		}
		return $courses;
	}

	public static function summary(){
		global $config;

		$courses = ParseJSON::getJson();
		PALog::log("ParseJSON::parse: called");
		$number = 1;
		$sectionNumber = 1;
		$body = array('fullHTML' => '');
		$sections = HomebaseCourseInfo::getAllSections();

		foreach($sections as $number => $section){
			$sdata = array();
			$sdata['SectionTitle'] = $section['title'];
			$sdata['SectionId'] = $section['id'];
			$sdata['SectionNumber'] = $number;
			$sdata['CollapseNumber'] = $number;
			$sdata['SectionHTML'] = '';
			$html = '';

			$sectonIds = HomebaseCourseInfo::getCoursesBySection($number);

			foreach($sectonIds as $id => $title){
				$course = $courses[$id];
				$courseInfo = HomebaseCourseInfo::getCourseInfo($id);
				$course['unavailable'] = $courseInfo['unavailable'];
				$sdata['SectionHTML'] .= RenderSummaryHTML::renderItem($config, $course);
			}
			$body['htmlItems']  .= RenderSummaryHTML::renderSection($config, $sdata);
		}

		$html = RenderSummaryHTML::renderBody($config, $body);
		PALog::log("ParseJSON::parse: complete:");
		return $html;
	}

	public static function getArray($id=null){
		$courses = ParseJSON::getJson();
		if ($id !== null){
			return isset($courses[$id]) ? $courses[$id] : null;
		}
		return $courses;
	}

	public static function adminUI(){
		$courses = ParseJSON::getJson();
		PALog::log("ParseJSON::parse: complete:");
		return $courses;
	}

	public static function featured(){
		global $config;

		$courses = ParseJSON::getJson();

		PALog::log("ParseJSON::parse: called");
		PALog::log("ParseJSON::parse: activityDetaillist");
		$number = 1;
		$sectionNumber = 1;
		$body = array('htmlItems' => '');
		$features = HomebaseCourseInfo::getFeaturedItems();

		$first = true;
		foreach($features as $id => $title){
			$course = $courses[$id];
			$course['active'] = $first ? " active" : '';
			$body['htmlItems'] .= RenderFeaturedHTML::item($config, $course);
			$first = false;
		}

		$html = RenderFeaturedHTML::body($config, $body);
		PALog::log("ParseJSON::parse: complete:");
		return $html;
	}

	public static function csv(){
		global $config;

		$courses = ParseJSON::getJson();

		PALog::log("ParseJSON::csv: called");
		foreach($courses as $id){
			$course = $courses[$id];
			$body['htmlItems'] .= RenderDetailCSV::item($config, $course);
		}
		$html = RenderSummaryCSV::body($config, $body);
		PALog::log("ParseJSON::csv: complete:");
		return $html;
	}

	public static function detail($id){
		global $config;

		$courses = ParseJSON::getJson();
		$course = $courses[$id];
		return RenderDetailHTML::render($config, $course);
	}

	public static function title($id){
		global $config;

		$courses = ParseJSON::getJson();
		$title = $courses[$title];
		return $title;
	}

	public static function itemParse($rawCourse, $number){
		$course = array();

		$course['id'] = '';
		$course['url'] = '';
		$course['credits'] = '';
		$course['active'] = '';
		$course['time'] = '';
		$course['hours'] = '';
		$course['photo'] = '';
		$course['intro'] = '';
		$course['targetaudience'] = '';

		$course['instructors'] = array();
		$course['objectives'] = array();
		$course['accreditations'] = array();

		$course['number'] = trim($number);

		$course['id'] = isset($rawCourse['id'])? trim($rawCourse['id']) : '';
		$course['classes'] = implode(" ",  HomebaseCourseInfo::getCourseAudiences($course['id']));
		$course['title'] = isset($rawCourse['title'])? trim($rawCourse['title']) : '';
		$course['format'] = isset($rawCourse['CustomActivityType'])? trim($rawCourse['CustomActivityType']) : '';

		$course['format'] = ($course['format'] == "Virtual Grand Round Series") ?
				"Webinar" :
				$course['format'];
		$course['title'] = ($course['format'] == "CBT Online Course (Parent)") ?
				"Multi Week Course " . $course['title']  :
				$course['title'];

		if (isset($rawCourse['facultylists'])){
			foreach($rawCourse['facultylists'] as $facutlyMember){
				$course['instructors'][] = trim($facutlyMember['name']);
			}
		}

		if (isset($rawCourse['overviewcontent'])){
			foreach($rawCourse['overviewcontent'] as $overview){

				PALog::log("ParseJSON::parse: overview-key: overview:".$overview['key']);
				switch($overview['key']){

					case 'LearningObjectivesJSON':
						$course['intro'] = $overview['value']['intro'];
						$objectives = $overview['value']['learningObjectives'];
						if (!is_array($objectives[0]['learningObjective'])){
							// text: no modules.

							foreach($objectives as $o){
								$course['objectives'][] = trim($o['learningObjective']);
							}
						}else{
							foreach($objectives as $m){
								$module = $m['learningObjective'];
								$mdata = array();
								$mdata['intro'] = $module['moduleIntro'];
								foreach($module['moduleLearningObjectives'] as $m){
									$mdata['objectives'][] = trim($m['moduleLearningObjective']);
								}
								$course['modules'][] = $mdata;
							}
						}
						break;
					case 'TargetAudienceJSON':
						if (isset($overview['value']['targetAudiences'])){
							$targetAudience = array();
							foreach($overview['value']['targetAudiences'] as $targetAudience){
								$targetaudience[] = trim($targetAudience['targetAudience']);
							}
							$course['targetaudience'] = implode(", ", $targetaudience);
						}
						break;
				}
			}
		}

		if (isset($rawCourse['cmeinfocontent'])){
			foreach($rawCourse['cmeinfocontent'] as $cmeinfo){
				switch($cmeinfo['key']){
					case 'CMEInfoJSON':
						if (isset($cmeinfo['value']['availableCreditTypes'])){
							foreach($cmeinfo['value']['availableCreditTypes'] as $cme){
								$course['accreditations'][] = $cme;
							}
							$course['credits'] = isset($course['accreditations'][0]['credits']) ?
								$course['accreditations'][0]['credits'] : '';
							$course['time'] = $course['credits'];
							$course['hours'] = $course['credits'];
							break;
					}
				}
			}
		}
		return $course;
	}

	public static function array_collapse($array, $attr){

		$return = array();
		foreach($array as $a){
			if (isset($a[$attr])){
				$return[] = $a[$attr];
			}
		}
		return $return;
	}
}
