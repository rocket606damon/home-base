<?php

class RenderSummaryHTML{

	public static function renderItem($config, $data){
		$templateDir = $config['templateDir'];

		PALOG::log('RenderSummaryHtml:__construct');

		$itemTemplateName = $templateDir.$config['summaryItemTemplate'];
		$itemTemplate = file_get_contents($itemTemplateName );
		$itemAttrs = array(
			"credits" => 'text',
			"hours" => 'text',
			"id" => 'text',
			"url" => 'text',
			"number" => 'text',
			"href" => 'text',
			"photo" => 'text',
			"image" => 'text',
			"thumb" => 'text',
			"disabled-div" => 'text',
			"disable-register" => 'text',
			"title" => 'text',
			"classes" => 'text'
		);
		$data['image'] = homebaseGetImage($data['id'], "thumb");
		$data['photo'] = $data['image'];
		$data['thumb'] = $data['image'];
		$data['url'] = '/education-training/training-institute/courses-secondary/courses-tertiary?id=' +  $data['id'];

		$data['disabled-div'] = !empty($data['unavailable']) ? "disabled-div" : "";
		$data['disable-register'] = !empty($data['unavailable']) ? "course-disabled" : "";

		$html = renderFromTemplate::render($data, $itemAttrs, $itemTemplate);
		return $html;
	}

	public static function renderSection($config, $data){
		PALOG::log('RenderSummaryHtml:renderSection');
		$templateDir = $config['templateDir'];

		$sectionTemplateName = $templateDir.$config['summarySectionTemplate'];
		$sectionTemplate = file_get_contents($sectionTemplateName);
		$sectionAttrs = array(
			"SectionNumber" => 'text',
			"SectionTitle" => 'text',
			"CollapseNumber" => 'text',
			"SectionId" => 'text',
			"SectionHTML" => 'text'
		);
		$html = renderFromTemplate::render($data, $sectionAttrs, $sectionTemplate);
		return $html;
	}

	public static function renderBody($config, $data){
		PALOG::log('RenderSummaryHtml:renderBody');
		$templateDir = $config['templateDir'];

		$bodyTemplateName = $templateDir.$config['summaryDocTemplate'];
		$bodyTemplate = file_get_contents($bodyTemplateName);
		$bodyAttrs = array(
			"htmlItems" => 'text'
		);

		$html = renderFromTemplate::render($data, $bodyAttrs, $bodyTemplate);
		return $html;
	}
}
