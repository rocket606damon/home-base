<?php

class RenderAudienceButtonsHTML{

	public static function renderItem($config, $data){
		$templateDir = $config['templateDir'];
		$imageDir = '/lib/homebase_courses/media/courses/';

		PALOG::log('RenderSummaryHtml:__construct');

		$itemTemplateName = $templateDir.$config['audienceButtonTemplate'];
		$itemTemplate = file_get_contents($itemTemplateName );
		$itemAttrs = array(
			"number" => 'text',
			"class" => 'text',
			"id" => 'text',
			"title" => 'text'
		);

		$html = renderFromTemplate::render($data, $itemAttrs, $itemTemplate);
		return $html;
	}

	public static function renderBody($config, $data){
		PALOG::log('RenderSummaryHtml:renderBody');
		$templateDir = $config['templateDir'];

		$bodyTemplateName = $templateDir.$config['audienceDocTemplate'];
		$bodyTemplate = file_get_contents($bodyTemplateName);
		$bodyAttrs = array(
			"htmlItems" => 'text'
		);

		$html = renderFromTemplate::render($data, $bodyAttrs, $bodyTemplate);
		return $html;
	}
}
