<?php

$homebaseCourseInfo = array(

	'sections' => array(

		//Military Culture and Intro Courses
		"Collapse0"  => array(
			3043=> "Is there an app for that?",
			3152=> "Military Culture",
			3050=> "Physical Health and Mental Health Following Deployment",
			3022=> "Reintegration Issues From the Veterans Perspective: Overcoming the Stigma of Seeking Help",
			3049=> "Service Dogs, Acupuncture, Psychotherapy, and More: What's the Evidence and What's Evidence-based",
			3048=> "Student Veterans Health: What College Health Professionals and Faculty Need to Know",
			3021=> "The Challenges of Coming Home After War: What Providers Need to Know",
			3070=> "Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms",
			2669=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS",
			2771=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11  Veterans - FIRST RESPONDERS",
		),
		// PTSD Courses
		"Collapse1"  => array(
			3035=> "Challenges of Treating Co-Morbid PTSD and TBI",
			3029=> "Clinical Practice Guidelines and Resources for PTSD Treatment",
			3034=> "Cognitive Processing Therapy for PTSD",
			3051=> "Clinical Case Conference - Recognizing and Treating Mild TBI and PTSD",
			3045=> "Clinical Case Conference - When Substance Abuse and PTSD Collide",
			3025=> "Keeping Military Families Emotionally Strong: Couples Therapy for PTSD",
			3031=> "Prolonged Exposure and Virtual Reality Therapy for PTSD",
			3032=> "Psychopharmacology of PTSD",
			3042=> "PTSD Diagnosis and DSM-V",
			3024=> "Recognizing PTSD and Co-Morbidities",
			2669=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS",
			2771=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11  Veterans - FIRST RESPONDERS",
		),
		// TBI Courses
		"Collapse2"  => array(
			3035=> "Challenges of Treating Co-Morbid PTSD and TBI",
			3051=> "Clinical Case Conference - Recognizing and Treating Mild TBI and PTSD",
			3030=> "Traumatic Brain Injury",
			3180=> "Mild Traumatic Brain Injury: Acute Management, Differential Diagnosis, Treatment, and Rehabilitation (April 2017)",
			2669=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS",
			2771=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11  Veterans - FIRST RESPONDERS",
			3560=> "Mild Traumatic Brain Injury: Acute Management, Differential Diagnosis, Treatment, and Rehabilitation (September 2017)",
		),
		// Substance Use Courses
		"Collapse3"  => array(
			3045=> "Clinical Case Conference - When Substance Abuse and PTSD Collide",
			3038=> "Substance Abuse",
			3070=> "Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms",
			2669=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS",
			2771=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11  Veterans - FIRST RESPONDERS",
		),
		// "Additional Trauma and Treatment Courses"
		"Collapse4"  => array(
			// 3023=> "When One Family Member Serves the Entire Family Serves",
			3027=> "Supporting Resilience in Military Connected Children: The PACT Model",
			//3028=> "Impact of Combat-Related Injury, Illness, and Death on Military Children and Families",
			3033=> "Military Sexual Trauma",
			3036=> "Recognizing Suicide Risk in Returning Veterans",
			3037=> "Sleep Issues in Returning Veterans",
            3038=> "Substance Abuse",
			3039=> "Aggression and Domestic Violence",
			3040=> "Managing Grief and Loss in Returning Veterans and Families",
			3041=> "Pain Issues in Returning Veterans",
			3045=> "Clinical Case Conference - When Substance Abuse and PTSD Collide",
			3046=> "Reproductive Mental Health Care in Younger and Older Female Veterans",
			3070=> "Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms",
			3072=> "Opioid Use Disorders: Considerations in Medication Assisted Treatments of Military Veterans",
			2669=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11 Veterans - HEALTH CARE PROFESSIONALS",
			2771=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11  Veterans - FIRST RESPONDERS",
		),
		// "Military Family Courses"
		"Collapse5"  => array(
			3023=> "When One Family Member Serves the Entire Family Serves",
			3025=> "Keeping Military Families Emotionally Strong: Couples Therapy for PTSD",
			3026=> "Challenges Facing Other Family Members When a Veteran has PTSD",
			3027=> "Supporting Resilience in Military Connected Children: The PACT Model",
			3028=> "Impact of Combat-Related Injury, Illness, and Death on Military Children and Families",
			3044=> "Staying Strong: Helping Families and Schools Build Resilience in Military-Connected Children",
			3047=> "Clinical Case Conference - When a Parent has PTSD: Restoring Resilience in Parents and Their Children",
			3071=> "Suicide Postvention: Survivor Care for Families of Service Members and Veterans",
			2771=> "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9/11  Veterans - FIRST RESPONDERS",
		)
	),

	'allCourses' => array(
		2669 => array('title' => "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9\/11 Veterans - HEALTH CARE PROFESSIONALS",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		2771 => array('title' => "Serving Those Who Have Served: Practical Approaches to Addressing the Invisible Wounds of War in Service Members and Post-9\/11  Veterans - FIRST RESPONDERS",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3021 =>  array('title' => "The Challenges of Coming Home After War: What Providers Need to Know",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3022 => array('title' => "Reintegration Issues From the Veterans Perspective: Overcoming the Stigma of Seeking Help",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3023 => array('title' => "When One Family Member Serves the Entire Family Serves",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3024 => array('title' => "Recognizing PTSD and Co-Morbidities",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3025 => array('title' => "Keeping Military Families Emotionally Strong: Couples Therapy for PTSD",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3026 => array('title' => "Challenges Facing Other Family Members When a Veteran has PTSD",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3027 => array('title' => "Supporting Resilience in Military Connected Children: The PACT Model",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3028 => array('title' => "Impact of Combat-Related Injury, Illness, and Death on Military Children and Families",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3029 => array('title' => "Clinical Practice Guidelines and Resources for PTSD Treatment",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3030 => array('title' => "Traumatic Brain Injury",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3031 => array('title' => "Prolonged Exposure and Virtual Reality Therapy for PTSD",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3032 => array('title' => "Psychopharmacology of PTSD",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3033 => array('title' => "Military Sexual Trauma",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3034 => array('title' => "Cognitive Processing Therapy for PTSD",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3035 => array('title' => "Challenges of Treating Co-Morbid PTSD and TBI",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3036 => array('title' => "Recognizing Suicide Risk in Returning Veterans",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3037 => array('title' => "Sleep Issues in Returning Veterans",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3038 => array('title' => "Substance Abuse",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3039 => array('title' => "Aggression and Domestic Violence",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3040 => array('title' => "Managing Grief and Loss in Returning Veterans and Families",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3041 => array('title' => "Pain Issues in Returning Veterans",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3042 => array('title' => "PTSD Diagnosis and DSM-V",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3043 => array('title' => "Is there an app for that?",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3044 => array('title' => "Staying Strong: Helping Families and Schools Build Resilience in Military-Connected Children",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3045 => array('title' => "Clinical Case Conference - When Substance Abuse and PTSD Collide",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3046 => array('title' => "Reproductive Mental Health Care in Younger and Older Female Veterans",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3047 => array('title' => "Clinical Case Conference - When a Parent has PTSD: Restoring Resilience in Parents and Their Children",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3048 => array('title' => "Student Veterans Health: What College Health Professionals and Faculty Need to Know",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3049 => array('title' => "Service Dogs, Acupuncture, Psychotherapy, and More: What's the Evidence and What's Evidence-based",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3050 => array('title' => "Physical Health and Mental Health Following Deployment",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3051 => array('title' => "Clinical Case Conference - Recognizing and Treating Mild TBI and PTSD",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3070 => array('title' => "Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3071 => array('title' => "Suicide Postvention: Survivor Care for Families of Service Members and Veterans",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3072 => array('title' => "Opioid Use Disorders: Considerations in Medication Assisted Treatments of Military Veterans",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3152 => array('title' => "Military Culture",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
		3180 => array('title' => "Mild Traumatic Brain Injury: Acute Management, Differential Diagnosis, Treatment, and Rehabilitation (April 2017)",
			'blueboxText' => "If you are interested in taking this course when it becomes available again, please email Emma Morrison at emorrison4@partners.org.", 'unavailable' => true, 'video' => 'http://www.youtube.com/watch?v=XHCtZJw0WU0'),
		3560 => array('title' => "Mild Traumatic Brain Injury: Acute Management, Differential Diagnosis, Treatment, and Rehabilitation (September 2017)",
			'blueboxText' => "", 'unavailable' => false, 'video' => ''),
	),

	'audiences' => array(
		2669 => array("Professionals"),
		2771 => array("Responders"),
		3021 => array("Professionals", "Community", "Responders", "Families"),
		3022 => array("Professionals", "Community", "Responders", "Families"),
		3023 => array("Community", "Families", "Responders", "Professionals"),
		3024 => array("Professionals", "Community", "Responders", "Families"),
		3025 => array("Professionals", "Community", "Responders", "Families"),
		3026 => array("Professionals", "Community", "Responders", "Families"),
		3027 => array("Responders", "Professionals"),
		3028 => array("Professionals", "Community", "Responders", "Families"),
		3029 => array("Professionals", "Community", "Responders", "Families"),
		3030 => array("Professionals", "Community", "Responders"),
		3031 => array("Professionals"),
		3032 => array("Professionals"),
		3033 => array("Professionals", "Community", "Responders", "Families"),
		3034 => array("Professionals"),
		3035 => array("Professionals", "Community", "Responders", "Families"),
		3036 => array("Professionals", "Community", "Responders", "Families"),
		3037 => array("Professionals", "Community", "Responders", "Families"),
		3038 => array("Professionals", "Responders", "Families", "Community",),
		3039 => array("Professionals", "Community", "Responders", "Families"),
		3040 => array("Professionals", "Community", "Responders", "Families"),
		3041 => array("Professionals", "Community", "Responders", "Families"),
		3042 => array("Professionals"),
		3043 => array("Professionals", "Community", "Responders", "Families"),
		3044 => array("Professionals", "Community", "Responders", "Families"),
		3045 => array("Professionals"),
		3046 => array("Professionals"),
		3047 => array("Professionals"),
		3048 => array("Professionals", "Community", "Responders", "Families"),
		3049 => array("Professionals", "Community", "Responders", "Families"),
		3050 => array("Professionals", "Community", "Responders", "Families"),
		3051 => array("Professionals"),
		3070 => array("Professionals"),
		3071 => array("Community", "Families", "Responders", "Professionals"),
		3072 => array("Professionals"),
		3152 => array("Professionals", "Community", "Responders", "Families"),
		3180 => array("Professionals"),
		3560 => array("Professionals"),
	),
	'featuredItemsInfo' => array(
		3023=> "When One Family Member Serves the Entire Family Serves",
		3027=> "Supporting Resilience in Military Connected Children: The PACT Model",
		3035=> "Challenges of Treating Co-Morbid PTSD and TBI",
		3038=> "Substance Abuse",
		3040=> "Managing Grief and Loss in Returning Veterans and Families",
		3070=> "Introducing VetChange:  An Online Intervention for Veterans with Problem Drinking and PTSD Symptoms",
		3071=> "Suicide Postvention: Survivor Care for Families of Service Members and Veterans",
		3072=> "Opioid Use Disorders: Considerations in Medication Assisted Treatments of Military Veterans",
		3152=> "Military Culture",

	),
	'sectionInfo' => array(
		"Collapse0"  => array('title'=>"Military Culture and Intro Courses",  'id'=>"military-culture"),
		"Collapse1"  => array('title'=>"PTSD Courses",  'id'=>"ptsd-courses"),
		"Collapse2"  => array('title'=>"TBI Courses",  'id'=>"tbi-courses"),
		"Collapse3"  => array('title'=>"Substance Use Courses",  'id'=>"substance-use"),
		"Collapse4"  => array('title'=>"Additional Trauma and Treatment Courses",  'id'=>"additional-trauma"),
		"Collapse5"  => array('title'=>"Military Family Courses",  'id'=>"military-family")
	),
	'audienceInfo' => array(
		'Professionals' => 	array('slug' => "healthcare-professionals", 'class' => "professionals", 'id' => "Professionals", 'title' => "Healthcare Professionals"),
		'Responders' => 	array('slug' => "first-responders", 		'class' => "responders", 'id' => "Responders", 'title' => "First Responders"),
		'Families' => 		array('slug' => "military-families", 		'class' => "families", 'id' => "Families", 'title' => "Military Families"),
		'Community' => 		array('slug' => "community-members", 		'class' => "community", 'id' => "Community", 'title' => "Community Members")
	)
);

$newHomebaseCourseInfo = array(
	'sections' => array(),
	'allCourses' => array(),
	'audiences' => array(),
	'featuredItemsInfo' => array(),
	'sectionInfo' => array(),
	'audienceInfo' => array(
		'Professionals' => 	array('slug' => "healthcare-professionals", 'class' => "professionals", 'id' => "Professionals", 'title' => "Healthcare Professionals"),
		'Responders' => 	array('slug' => "furst-responders", 		'class' => "responders", 'id' => "Responders", 'title' => "First Responders"),
		'Families' => 		array('slug' => "military-families", 		'class' => "families", 'id' => "Families", 'title' => "Military Families"),
		'Community' => 		array('slug' => "community-members", 		'class' => "community", 'id' => "Community", 'title' => "Community Members")
	)
);

class HomebaseCourseInfo{

	private static $info;

	public static function setData($info){
		HomebaseCourseInfo::$info = $info;
	}

	private static function getFull($index){
		return isset(HomebaseCourseInfo::$info[$index]) ?
				HomebaseCourseInfo::$info[$index] : array();
	}

	private static function get($index, $id){
		return isset(HomebaseCourseInfo::$info[$index][$id]) ?
				HomebaseCourseInfo::$info[$index][$id] : array();
	}

	public static function getCoursesBySection($sectionId){
		return HomebaseCourseInfo::get('sections', $sectionId);
	}

	public static function courseInSection($sectionId, $courseId){
		return count(HomebaseCourseInfo::get('sections', $sectionId)) ? true : false;
	}

	public static function getCourseAudiences($courseId){
		return HomebaseCourseInfo::get('audiences', $courseId);
	}

	public static function getCourseInfo($courseId){
		return HomebaseCourseInfo::get('allCourses', $courseId);
	}

	public static function getSectionInfo($sectionId){
		return HomebaseCourseInfo::get('sectionInfo', $sectionId);
	}

	public static function getAllSections(){
		return HomebaseCourseInfo::getFull('sectionInfo');
	}
	public static function getFeaturedItems(){
		return HomebaseCourseInfo::getFull('featuredItemsInfo');
	}
	public static function getAudienceInfo(){
		return HomebaseCourseInfo::getFull('audienceInfo');
	}

	public static function get_children($id){
		$terms 	= get_terms( array(
			'taxonomy' => 'display-type',
			'hide_empty' => false,
		) );
		$children = array();
		foreach($terms  as $t){
			if($t->parent == $id){
				$children[] = $t;
			}
		}
		return $children;
	}

	public static function get_posts($id){
		$posts_array = get_posts(
			array(
				'posts_per_page' => -1,
				'post_type' => 'courses',
				'tax_query' => array(
					array(
						'taxonomy' => 'display-type',
						'field' => 'term_id',
						'terms' => $id,
					)
				)
			)
		);
		return $posts_array;
	}

	public static function get_display_type_by_slug($slug){
		$terms 	= get_terms( array(
			'taxonomy' => 'display-type',
			'hide_empty' => false,
		) );
		foreach($terms  as $t){
			if($t->slug == $slug){
				return $t;
			}
		}
		return false;
	}

	public static function loadCourseInfo($config, $courseInfo){

		// Sections (display categories)
		// 2-deep (cat/subcat)
		$cat = HomebaseCourseInfo::get_display_type_by_slug($config['displayCatSlug']);
		if ($cat){
			$subCats = HomebaseCourseInfo::get_children($cat->term_id);
			$collapseNumber = 0;
			foreach ($subCats as $id => $subCat){
				$sectionName = 'Collapse'.$collapseNumber++;
				$courseInfo['sectionInfo'][$sectionName]['id'] = $subCat->term_id;
				$courseInfo['sectionInfo'][$sectionName]['title'] = $subCat->name;;
				$posts = HomebaseCourseInfo::get_posts($subCat->term_id);
				$courseInfo['sections'][$sectionName] = array();
				foreach($posts as $post){

					$courseInfo['sections'][$sectionName][$post->ID] = $post->post_title;
					$postMeta = get_post_meta($post->ID);

					$changed = (isset($postMeta) && isset($postMeta['changed'][0])) ? $postMeta['changed'][0] : '';
					$hpcourse_id = (isset($postMeta) && isset($postMeta['hpcourse_id'][0])) ? $postMeta['hpcourse_id'][0] : '';
					$blue_box_text = (isset($postMeta) && isset($postMeta['blue_box_text'][0])) ? $postMeta['blue_box_text'][0] : '';
					$unavailable = (isset($postMeta) && isset($postMeta['unavailable'][0])) ? $postMeta['unavailable'][0] : '';
					$video_embed_link = (isset($postMeta) && isset($postMeta['video_embed_link'][0])) ? $postMeta['video_embed_link'][0] : '';

					$courseInfo['allCourses'][$post->ID] = array(
							'title'=>$post->post_title,
							'changed'=> $changed,
							'hpcourse_id'=> $hpcourse_id,
							'blue_box_text' => $blue_box_text,
							'unavailable'=> $unavailable,
							'video_embed_link'=> $video_embed_link
					);
				}
			}
		}

		// Audiences
		// 2-deep (cat/subcat)
		$cat = HomebaseCourseInfo::get_display_type_by_slug($config['audienceCatSlug']);
		if ($cat){
			$subCats = HomebaseCourseInfo::get_children($cat->term_id);
			foreach ($subCats as $id => $subCat){
				$courseInfo['audienceInfo'][$subCat->slug] = array(
                    'slug' => $subCat->slug,
                    'class' => str_replace('-', '', strToLower($subCat->slug)),
                    'id' => strToLower($subCat->slug),
                    'title' => $subCat->name
				);
				$posts = HomebaseCourseInfo::get_posts($subCat->term_id);
				foreach ($posts as $post){
					$courseInfo['audiences'][$post->ID][$subCat->name] = true;
				}
			}
		}

		// Featured Items
		// 1-deep (cat)
		$cat = HomebaseCourseInfo::get_display_type_by_slug($config['featuredCatSlug']);
		if ($cat){
			$posts = HomebaseCourseInfo::get_posts($subCat->term_id);
			$courseInfo['featuredItemsInfo'] = array();
			foreach ($posts as $post){
				$courseInfo['featuredItemsInfo'][$post->ID] = $post->post_name;
			}
		}
		return $courseInfo;
	}
}

function hb_init(){
	global $config;
	$newHomebaseCourseInfo;
	global $homebaseCourseInfo;

	//$newHomebaseCourseInfo = HomebaseCourseInfo::loadCourseInfo($config, $newHomebaseCourseInfo);
	//echo "<!--[[newHomebaseCourseInfo]] " . print_r($newHomebaseCourseInfo, 1) . "[[/newHomebaseCourseInfo]] -->\n";
	HomebaseCourseInfo::setData($homebaseCourseInfo);
	//echo "<!--[[homebaseCourseInfo]] " . print_r($homebaseCourseInfo, 1) . "[[/homebaseCourseInfo]] -->\n";
}

//add_action( 'wp_loaded', 'hb_init' );
hb_init();
