<?php

function fixTM($str){
	$tmStrings = array("TM", "tm");
	$tmChar = "&trade;"; // for output  and not for storage...
	if (is_string($str) && strlen($str) > 1){
		$lastTwo = substr($str, strlen($str) - 2);
		$lastTwo = str_replace ($tmStrings, $tmChar, $lastTwo);
		$firstNMinusTwo = substr($str, 0, strlen($str) - 2);
		$str = $firstNMinusTwo . $lastTwo;
	}
	return $str;
}


class RenderDetailHtml{

	public static function getUrl($configi, $id){
		global $config;

		$url = $config['registerUrlPrefix'] . $id;;
		return $url;
	}

	public static function render($config1, $data){
		global $config;
		global $homebaseCourseInfo;

		$templateDir = $config['templateDir'];

		$bodyTemplateName = $templateDir.$config['detailDocTemplate'];
		$accrTemplateName = $config['templateDir'].$config['detailAccreditationTemplate'];
		$videoTemplateName = $templateDir.$config['videoTemplate'];
		$blueboxTextTemplateName = $templateDir.$config['blueboxTextTemplate'];

		PALog::log("RenderDetailHtml:render:bodyTemplateName:". $bodyTemplateName);
		$bodyTemplate = file_get_contents($bodyTemplateName);

		PALog::log("RenderDetailHtml:render:accrTemplateName:". $accrTemplateName);
		$accrTemplate = file_get_contents($accrTemplateName );

		PALog::log("RenderVideo:render:bodyTemplateName:". $videoTemplateName);
		$videoTemplate = file_get_contents($videoTemplateName);

		PALog::log("RenderBlueBoxText:render:blueboxTextTemplateName:". $blueboxTextTemplateName);
		$blueboxTextTemplate = file_get_contents($blueboxTextTemplateName );

		$headAttrs = array(
			"id" => 'text',
			"image" => 'text',
			"photo" => 'text',
			"format" => 'text',
			"instructors" => 'array',
			"targetaudience" => 'text',
			"video" => 'text',
			"disable-register" => 'text',
			"htmlBlueboxtext" => 'text',
			"time" => 'text',
			"title" => 'text',
			"credits" => 'text',
			"htmlBlueboxtextDiv" => 'text',
			"htmlVideoDiv" => 'text',
			"htmlAccreditations" => 'text',
			"htmlObjectives" => 'text',
			"pagename" => 'text',
			"videoClasses" => 'text',
			"objectivesClasses" => 'text',
			"debug" => 'text',
			"url" => 'text',
			"wholeenchilada" => 'text'
		);
		$objectiveAttrs = array(
			"intro" => 'text',
			"objectives" => 'list',
		);
		$videoAttrs = array(
			"video-class" => 'text',
			"video-width" => 'text',
			"video-height" => 'text',
			"video-url" => 'text'
		);
		$blueboxTextAttrs = array(
			"blue-box-text-class" => 'text',
			"blue-box-text" => 'text'
		);
		$accreditationAttrs = array(
			"SectionNumber" => 'text',
			"CollapseNumber" => 'text',
			"accreditationStatment" => 'text',
			"accreditationNumber" => 'text',
			"SectionID" => 'text',
			"creditType" => 'text',
			"credits" => 'text',
			"designationStatment" => 'text',
		);

		$data['htmlObjectives'] = '';
		if(count($data['modules'])){
			foreach ($data['modules'] as $module){
				$data['htmlObjectives'] .=
					"<p>". $module['intro'] . "</p>\n".
					"<ul>\n" .
					"\t<li>".implode("</li>\n\t<li>", $module['objectives'])."</li>\n".
					"</ul>\n";
			}
		}else{
			$data['htmlObjectives'] =
				"<p>".$data['intro']."</p>\n".
				"<ul>\n" .
				"\t<li>".implode("</li>\n\t<li>", $data['objectives'])."</li>\n".
				"</ul>\n";
		}

		$acccount = count($data['accreditations']);
		for ($i = 0; $i < $acccount; $i++){
			if (empty($data['credits'])){
				$data['credits'] = $data['accreditations'][$i]['credits'];
				$data['hours'] = $data['accreditations'][$i]['credits'];
			}
			$data['accreditations'][$i]['accreditationStatment'] = fixTM($data['accreditations'][$i]['accreditationStatment']);
			$data['accreditations'][$i]['designationStatment'] = fixTm($data['accreditations'][$i]['designationStatment']);
			$data['accreditations'][$i]['creditType'] = fixTm($data['accreditations'][$i]['creditType']);

			$data['accreditations'][$i]['SectionNumber'] = $i;
			$data['accreditations'][$i]['SectionID'] = "Collapse"+$i;
			$data['accreditations'][$i]['accreditationNumber'] = $i;
			$data['accreditations'][$i]['CollapseNumber'] = $i+1;
		}
		$data['image'] = homebaseGetImage($data['id'], "full");
		$data['photo'] = $data['image'];

		$data['pagename'] = "Course Information";
		$data['accreditations']  = isset($data['accreditations']) ? $data['accreditations'] : array();
		$data['url'] = RenderDetailHtml::getUrl($config, $data['id']);

		$data['disable-register'] = (!empty($homebaseCourseInfo['allCourses'][$data['id']]['unavailable'])) ?
			"course-disabled" : "";

		if (empty($homebaseCourseInfo['allCourses'][$data['id']]['video'])){
			$data['video-class'] = 'panel panel-default';
			$data['objectivesClasses'] = "col-sm-12 col-md-12";
			$data['video-width'] = '';
			$data['video-height'] = '';
			$data['video-url'] = '';
			$data['htmlVideoDiv'] = '';
		}else{
			global $wp_embed;
			$data['objectivesClasses'] = "col-sm-6 col-md-6";
			$data['video-class'] = 'panel panel-default';
			$data['video-width'] = $config['video-width'];
			$data['video-height'] = $config['video-height'];
			$width = !empty($data['video-width']) ? " width=".$data['video-width'] : '';
			$height = !empty($data['video-height']) ? " height=".$data['video-height'] : '';
			$data['video-url'] = $wp_embed->run_shortcode( '[embed' . $width . $height . ']' . $homebaseCourseInfo['allCourses'][$data['id']]['video'] . '[/embed]' );
			$data['htmlVideoDiv'] = renderFromTemplate::render ($data, $videoAttrs, $videoTemplate);
		}

		if (empty($homebaseCourseInfo['allCourses'][$data['id']]['blueboxText'])){
			$data['htmlBlueboxtextDiv'] = '';
		}else{
			$data['blue-box-text'] = $homebaseCourseInfo['allCourses'][$data['id']]['blueboxText'];
			$data['blue-box-text-class'] = $config['blue-box-text-class'];
			$data['htmlBlueboxtextDiv'] = renderFromTemplate::render ($data, $blueboxTextAttrs, $blueboxTextTemplate);
		}

		$data['htmlAccreditations'] = renderFromTemplate::renderList(
				$data['accreditations'],
				$accreditationAttrs,
				$accrTemplate);
		$data['wholeenchilada'] = str_replace("\n", "", print_r($data,1));
		$html = renderFromTemplate::render ($data,$headAttrs,$bodyTemplate);

		$html = do_shortcode($html, true);

		return $html;
	}
}
