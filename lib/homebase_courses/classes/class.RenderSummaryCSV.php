<?php

class RenderSummaryCSV{

	public static function getUrl($config, $id){
		$url = "/education-training/training-institute/courses-tertiary/?id=" . $id;
		return $url;
	}

	public static function renderItem($config, $data){
		$templateDir = $config['templateDir'];
		$imageDir = '/lib/homebase_courses/media/courses/';

		PALOG::log('RenderSummaryHtml:__construct');

		$itemTemplateName = $templateDir.$config['csvItemTemplate'];
		$itemTemplate = file_get_contents($itemTemplateName );
		$itemAttrs = array(
			"credits" => 'text',
			"hours" => 'text',
			"id" => 'text',
			"number" => 'text',
			"href" => 'text',
			"image" => 'text',
			"title" => 'text',
			"classes" => 'text'
		);

		$thumbnailFilename = 'HPC_' . $data['id'] . '_thumb.png';
		$thumbnailPath = $imageDir. $thumbnailFilename;
		$data['image'] = get_template_directory_uri() . $thumbnailPath;

		// $data['href'] = get_page_link();
		$data['href'] = RenderSummaryHtml::getUrl($config, $data['id']);

		$html = renderFromTemplate::render($data, $itemAttrs, $itemTemplate);
		return $html;
	}

	public static function renderSection($config, $data){
		PALOG::log('RenderSummaryHtml:renderSection');
		$templateDir = $config['templateDir'];

		$sectionTemplateName = $templateDir.$config['summarySectionTemplate'];
		$sectionTemplate = file_get_contents($sectionTemplateName);
		$sectionAttrs = array(
			"SectionNumber" => 'text',
			"SectionTitle" => 'text',
			"CollapseNumber" => 'text',
			"SectionId" => 'text',
			"SectionHTML" => 'text'
		);
		$html = renderFromTemplate::render($data, $sectionAttrs, $sectionTemplate);
		return $html;
	}

	public static function renderBody($config, $data){
		PALOG::log('RenderSummaryHtml:renderBody');
		$templateDir = $config['templateDir'];

		$bodyTemplateName = $templateDir.$config['summaryDocTemplate'];
		$bodyTemplate = file_get_contents($bodyTemplateName);
		$bodyAttrs = array(
			"htmlItems" => 'text'
		);

		$html = renderFromTemplate::render($data, $bodyAttrs, $bodyTemplate);
		return $html;
	}
}
