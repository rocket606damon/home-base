<?php

class RenderFeaturedHTML{

	public static function getUrl($config, $id){
		$url = "/education-training/training-institute/courses-tertiary/?id=" . $id;
		return $url;
	}

	public static function item($config, $data){
		$templateDir = $config['templateDir'];
		PALOG::log('RenderFeaturedHTML:__construct');

		$itemTemplateName = $templateDir.$config['featuredItemTemplate'];
		$itemTemplate = file_get_contents($itemTemplateName );
		$itemAttrs = array(
			"id" => 'text',
			"number" => 'text',
			"href" => 'text',
			"active" => 'text',
			"photo" => 'text',
			"image" => 'text',
			"thumb" => 'text',
			"title" => 'text',
			"credits" => 'text',
			"hours" => 'text',
		);

		$data['photo'] = homebaseGetImage($data['id'], "thumb");
		$data['thumb'] = $data['photo'];
		$data['image'] = $data['photo'];

		$data['href'] = RenderFeaturedHTML::getUrl($config, $data['id']);
		$html = renderFromTemplate::render($data, $itemAttrs, $itemTemplate);
		return $html;
	}

	public static function body($config, $data){
		PALOG::log('RenderFeaturedHTML:renderBody');
		$templateDir = $config['templateDir'];

		$bodyTemplateName = $templateDir.$config['featuredDocTemplate'];
		$bodyTemplate = file_get_contents($bodyTemplateName);
		$bodyAttrs = array(
			"htmlItems" => 'text'
		);

		$html = renderFromTemplate::render($data, $bodyAttrs, $bodyTemplate);
		return $html;
	}
}
