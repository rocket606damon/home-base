<?php

$base_dir = dirname(__FILE__).'/';
define('BASE_DIR', $base_dir);
define('CONFIG_FILE', BASE_DIR . 'config/config.php');

require_once(CONFIG_FILE);
require_once(CLASS_DIR."class.HomebaseCourseInfo.php");
require_once(CLASS_DIR."class.PsychAcademyReadData.php");
require_once(CLASS_DIR."class.RenderDetailHTML.php");
require_once(CLASS_DIR."class.RenderSummaryHTML.php");
require_once(CLASS_DIR."class.RenderFeaturedHTML.php");
require_once(CLASS_DIR."class.ParseJSON.php");
require_once(CLASS_DIR."class.RenderFromTemplate.php");
require_once(CLASS_DIR."class.PALog.php");

require_once(VENDOR_DIR."unirest/src/Unirest.php");

function homebaseGetList(){
	PALog::log("renderHTML: homebaseGetList called");

	$html = ParseJSON::featured();
	$html .= ParseJSON::summary();
    return $html;
}

function homebaseGetDetail($id=null){
	PALog::log("renderHTML: homebaseGetDetail called");
	if ($id === null){
		$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
	}
	$detail = ParseJSON::detail($id);
    return $detail;
}

function homebaseGetFeatures(){
	PALog::log("renderHTML: homebaseGetFeatures called");

	$features = ParseJSON::featured();
    return $features;
}

function homebaseGetCourseTitle($id){
	PALog::log("renderHTML: homebaseGetCourseTitle called");

	$title = ParseJSON::title($id);
    return $title;
}

function homebaseGetAudiences(){
	PALog::log("renderHTML: homebaseGetAudiences called");

	$audiences = ParseJSON::audience();
    return $audiences;
}

function homebaseGetCSV(){
	PALog::log("renderHTML: homebaseGetCSV called");

	$csv = ParseJSON::csv();
    return $csv;
}

function homebaseGetAdminUI(){
	PALog::log("renderHTML: homebaseGetAdminUI called");

	$adminUI = ParseJSON::adminUI();
    return $adminUI;
}

function homebaseGetArray($id=null){
	PALog::log("renderHTML: homebaseGetArray called");

	$array = ParseJSON::getArray($id);
    return $array;
}

function homebaseGetImage($id, $type="full"){
	$exceptions = array(3560 => "3560a");
	$imageDir = '/lib/homebase_courses/media/courses/';

	$id = isset($exceptions[$id]) ? $exceptions[$id] : $id;
	$filename = "HPC_" . $id . "_" . $type . ".png";
	$path = $imageDir. $filename;
	$fullPath = get_template_directory_uri() . $path;
	return $fullPath;
}

