<?php

//ini_set('memory_limit', '2048M');

$base_dir = dirname(__FILE__).'/';
define('BASE_DIR', $base_dir);
define('CONFIG_FILE', BASE_DIR . 'config/config.php');

require_once(CONFIG_FILE);
require_once(CLASS_DIR."class.HomebaseCourseInfo.php");
require_once(CLASS_DIR."class.PsychAcademyReadData.php");
require_once(CLASS_DIR."class.RenderDetailHTML.php");
require_once(CLASS_DIR."class.RenderSummaryHTML.php");
require_once(CLASS_DIR."class.RenderFeaturedHTML.php");
require_once(CLASS_DIR."class.ParseJSON.php");
require_once(CLASS_DIR."class.RenderFromTemplate.php");
require_once(CLASS_DIR."class.PALog.php");

require_once(VENDOR_DIR."unirest/src/Unirest.php");

define('WP_USE_THEMES', false);
define('HB_WP_POST_TYPE', 'courses');
define('HB_WP_POST_STATUS', 'draft');
define('HB_WP_COURSEID_KEY', 'hpcourse_id');

class WordpressConnect{

	public static function init(){
		require ('../../../../wp-load.php');
		//require ('../../../../../wp-admin/includes/image.php');
	}

	public static function getCurrentCourses(){
		$courses = array();
		$args = array(
			'posts_per_page' => -1,
			'post_status' => 'draft',
			'post_type' => 'courses',
		);

		$posts = get_posts($args);
		foreach($posts as $p){
			$id = $p->ID;
			$courseId = get_post_meta($id, HB_WP_COURSEID_KEY, true);
			echo "course $id:" . $courseId."\n";
			$courses[$courseId] = $id;
		}
		print_r($courses);
		exit;
		return $courses;
	}

	public static function getCourseTerms($id){
		global $homebaseCourseInfo;

		$terms = array();

		$sections = array();
		foreach($homebaseCourseInfo['sections'] as $courseId => $courses){
			if (isset($courses[$id])){
				$sections[]= $homebaseCourseInfo['sectionInfo'][$courseId]['id'];
			}
		}
		if (count($sections)){
			$terms[] = "section-info";
			foreach($sections as $s){
				$terms[] = $s;
			}
		}

		$audienceSlugs = '';
		if (isset($homebaseCourseInfo['audiences'][$id])){
			if (count($homebaseCourseInfo['audiences'][$id])){
				$terms[] = "audience-info";
				foreach($homebaseCourseInfo['audiences'][$id] as $a){
					$terms[] = $homebaseCourseInfo['audienceInfo'][$a]['slug'];
				}
			}
		}

		if (isset($homebaseCourseInfo['featuredItemsInfo'][$id])){
			$terms[] = 'featured-course';
		}
		return $terms;
	}

	public static function addCourse($id, $title){
		$postData = array(
			'post_type' 	=> HB_WP_POST_TYPE,
			'post_title'    => $title,
			'post_content'  => '',
			'post_excerpt'  => '',
			'post_status'   => 'draft',
			'post_author'   => 1
		);

		$wpId = WordpressConnect::addPost($postData);
		if ($wpId){
			add_post_meta($wpId, HB_WP_COURSEID_KEY, $id);
			add_post_meta($wpId, 'blue_box_text', '');
			add_post_meta($wpId, 'changed', 1);

			$terms = WordpressConnect::getCourseTerms($id);
			if (count($terms)){
				wp_set_object_terms($wpId, $terms, "display-type");
			}
		}
		return $wpId;
	}

	public static function getCurrentCourseNext($id){

		// set up metadata search
		$args = array(
			'post_type' => HB_WP_POST_TYPE,
			'meta_query'  => array(
				array(
					'key' => HB_WP_COURSEID_KEY,
					'value' => $id
				)
			)
		);

		$postId = wp_insert_post($postData, true);

		if (is_wp_error($postId)) {
			$errors = $postId->get_error_messages();
		}else{
			print_r($postId);
		}
		return $postId;
	}

	public static function addPost($postData){
		$postId = wp_insert_post($postData);
		if (is_wp_error($postId)) {
			$errors = $post_id->get_error_messages();
		}
		return $post_id;
	}

	public static function findAllCourses($courses){
		$posts = WordpressConnect::getCurrentCourses();
		foreach($courses as $id => $data){
			echo "findAllCourses: course $id\n";
			$post = WordpressConnect::getCurrentCourse($id);
			//echo "course: $id; post: " . $post->ID . "\n";
			print_r($post);
		}
	}

	public static function updateCourses($courses){
		foreach($courses as $id => $data){
			$title = $data['title'];
			$post = WordpressConnect::addCourse($id, $title);
			echo "course: $id; post: " . $post->ID . "\n";
		}
	}

	public static function setCurrentImage($courseId){
		foreach($courses as $id => $data){
			$title = $data['title'];
			$post = WordpressConnect::addCourse($id, $title);
			echo "course: $id; post: " . $post->ID . "\n";
		}
	}

	public static function addImage($parentId, $courseId, $filename ){

		$filetype = wp_check_filetype( basename( $filename ), null );

		// Prepare an array of post data for the attachment.
		$attachment = array(
			'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ),
			'post_mime_type' => $filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			'post_content'   => '',
			'course_id'   	 => $courseId,
			'post_status'    => 'inherit'
		);
		print_r($attachment);

		// Insert the attachment.
		$attach_id = wp_insert_attachment($attachment, $filename, $parentId );
		echo "wp_insert_attachment: $attach_id\n";
		exit;

		// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		// Generate the metadata for the attachment, and update the database record.
		$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
		wp_update_attachment_metadata( $attach_id, $attach_data );

		set_post_thumbnail( $parentId, $attach_id );
	}
}

//WordpressConnect::init();

$filename = "/srv/users/serverpilot/apps/homebase/public/wp-content/themes/homebase/lib/homebase_courses/HPC_" .	 $courseId . "_full.png";
//WordpressConnect::addImage($parentId, $courseId, $filename);
// $courses = ParseJSON::getJson();
// WordpressConnect::findAllCourses($courses);
//WordpressConnect::updateCourses($courses);

function v21(){
	// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
	require ('../../../../../wp-load.php');

	$postId = 2833;
	$courseId = 3039;
	$parentId = $postId;

	$file = "/srv/users/serverpilot/apps/homebase/public/wp-content/themes/homebase/lib/dev/homebase_courses/HPC_" . $courseId . "_full.png";
	$filename = basename($file);
	$upload_file = wp_upload_bits($filename, null, file_get_contents($file));
	if (!$upload_file['error']) {
		$wp_filetype = wp_check_filetype($filename, null );
		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_parent' => $parentId,
			'post_title' =>  sanitize_file_name($filename),
			'post_content' => '',
			'post_status' => 'inherit'
		);
		print
		$attachmentId = wp_insert_attachment( $attachment, $upload_file['file'], $parentId );
		if (!is_wp_error($attachmentId)) {

			require_once(ABSPATH . "wp-admin" . '/includes/image.php');
			$attachmentData = wp_generate_attachment_metadata( $attachmentId, $upload_file['file'] );
			wp_update_attachment_metadata( $attachmentId,  $attachmentData );
			update_post_meta($postId, '_thumbnail_id', $attachmentId);
		}
	}
}

require ('../../../../../wp-load.php');

$postId = 2833;
$courseId = 3039;
$file = "/srv/users/serverpilot/apps/homebase/public/wp-content/themes/homebase/lib/homebase_courses/HPC_" . $courseId . "_full.png";

Generate_Featured_Image( $file, $postId);

function Generate_Featured_Image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2= set_post_thumbnail( $post_id, $attach_id );
}