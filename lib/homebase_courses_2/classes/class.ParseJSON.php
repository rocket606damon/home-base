<?php

class ParseJSON{

	public static function getJson(){
		$config = getHomebaseConfig();

		$courses=array();

		$cacheDir = BASE_DIR.'cache/';
		$cachePath = $cacheDir.$config['cacheFile'];
		if (file_exists($cachePath) &&
				filemtime($cachePath) > (time() - 1 * $config['cacheMinutes'])) {
			$json = file_get_contents($cachePath);
			$courses = json_decode($json , 1);
		} else {
			$response = PsychAcademyReadData::read();
			$courses = ParseJSON::preParse($response);
			file_put_contents($cachePath, json_encode($courses, 1), LOCK_EX);
		}
		return $courses;
	}

	public static function reloadJson(){
		$config = getHomebaseConfig();
		$cacheDir = BASE_DIR.'cache/';
		$cachePath = $cacheDir.$config['cacheFile'];

		$response = PsychAcademyReadData::read();
		$courses = ParseJSON::preParse($response);
		file_put_contents($cachePath, json_encode($courses, 1), LOCK_EX);
	}

	public static function preParse($response){
		$courses = array();

		if (isset($response['activityDetaillist'])){
			$rawCourses = $response['activityDetaillist'];
			$number = 1;
			foreach($rawCourses as $rawCourse){
				$course = ParseJSON::itemParse($rawCourse, $number++);
				$course['checksum'] = md5(json_encode($course));
				$id = trim($course['id']);
				$courses[$id] = $course;
			}
		}
		return $courses;
	}

	public static function summary(){

		$courses = ParseJSON::getJson();

		$number = 1;
		$sectionNumber = 1;
		$sections = HomebaseCourseInfo::getAllSections();

		$body = array('htmlItems' => '');
		foreach($sections as $number => $section){
			$sdata = array();
			$sdata['SectionTitle'] = $section['title'];
			$sdata['SectionId'] = $section['id'];
			$sdata['SectionNumber'] = $number;
			$sdata['CollapseNumber'] = $number;
			$sdata['SectionHTML'] = '';
			$html = '';

			$sectionIds = HomebaseCourseInfo::getCoursesBySection($number);

			$tempSections = array();
			foreach($sectionIds as $id){
				if (isset($courses[$id])){
					$courseInfo = HomebaseCourseInfo::getCourseInfo($id);
					$courseInfo['id'] = $id;
					if (!isset($courseInfo['order_within_category']) || $courseInfo['order_within_category'] == 0){
						$courseInfo['order_within_category'] =  999;
					}else{
					}
					$combinedCourseInfo = array_merge($courseInfo, $courses[$id]);
					$tempSections[] = $combinedCourseInfo;
				}
			}

			usort($tempSections, function($a, $b) {
				return (intval($a['order_within_category']) <  intval($b['order_within_category'])) ? -1 : 1;
			});

			foreach($tempSections as $course){
				$id = $course['id'];
				$sdata['SectionHTML'] .= RenderSummaryHTML::renderItem($course);
			}
			$body['htmlItems']  .= RenderSummaryHTML::renderSection($sdata);
		}
		$html = RenderSummaryHTML::renderBody($body);
		PALog::log("ParseJSON::parse: complete:");
		return $html;
	}

	public static function show_keys($sections){
		$out = array();
		foreach ($sections as $s){
			$out[] = $s['order_within_category'];
		}
		return $out;
	}

	public static function featured(){
		$courses = ParseJSON::getJson();

		PALog::log("ParseJSON::parse:featured called");
		$body = array('htmlItems' => '');

		$featuredCourses = HomebaseCourseInfo::getFeaturedCourses();
		foreach($featuredCourses as $id => $courseInfo){
			if (isset($courses[$id])){
				$courseInfo['id'] = $id;
				$combinedCourseInfo = array_merge($courseInfo, $courses[$id]);
				$body['htmlItems'] .= RenderFeaturedHTML::item($combinedCourseInfo);
			}
		}

		$html = RenderFeaturedHTML::body($body);
		return $html;
	}

	public static function detail($id){
		$courses = ParseJSON::getJson();
		$courseInfo = HomebaseCourseInfo::getCourseInfo($id);
		$courseInfo['id'] = $id;
		$combinedCourseInfo = array_merge($courseInfo, $courses[$id]);
		return RenderDetailHTML::render($combinedCourseInfo);
	}

	public static function itemParse($rawCourse, $number){
		$course = array();
		$course['id'] = '';
		$course['url'] = '';
		$course['credits'] = '';
		$course['time'] = '';
		$course['hours'] = '';
		$course['photo'] = '';
		$course['intro'] = '';
		$course['targetaudience'] = '';

		$course['instructors'] = array();
		$course['objectives'] = array();
		$course['accreditations'] = array();

		$course['number'] = trim($number);

		$course['id'] = isset($rawCourse['id'])? trim($rawCourse['id']) : '';

		$course['classes'] = implode(" ",  HomebaseCourseInfo::getCourseAudiences($course['id']));

		$course['title'] = isset($rawCourse['title'])? trim($rawCourse['title']) : '';
		$course['format'] = isset($rawCourse['CustomActivityType'])? trim($rawCourse['CustomActivityType']) : '';

		$course['format'] = ($course['format'] == "Virtual Grand Round Series") ?
				"Webinar" :
				$course['format'];
		$course['title'] = ($course['format'] == "CBT Online Course (Parent)") ?
				"Multi Week Course " . $course['title']  :
				$course['title'];

		if (isset($rawCourse['facultylists'])){
			foreach($rawCourse['facultylists'] as $facutlyMember){
				$course['instructors'][] = trim($facutlyMember['name']);
			}
		}

		if (isset($rawCourse['overviewcontent'])){
			foreach($rawCourse['overviewcontent'] as $overview){

				PALog::log("ParseJSON::parse: overview-key: overview:".$overview['key']);
				switch($overview['key']){

					case 'LearningObjectivesJSON':
						$course['intro'] = $overview['value']['intro'];
						$objectives = $overview['value']['learningObjectives'];
						if (!is_array($objectives[0]['learningObjective'])){
							// text: no modules.

							foreach($objectives as $o){
								$course['objectives'][] = trim($o['learningObjective']);
							}
						}else{
							foreach($objectives as $m){
								$module = $m['learningObjective'];
								$mdata = array();
								$mdata['intro'] = $module['moduleIntro'];
								foreach($module['moduleLearningObjectives'] as $m){
									$mdata['objectives'][] = trim($m['moduleLearningObjective']);
								}
								$course['modules'][] = $mdata;
							}
						}
						break;
					case 'TargetAudienceJSON':
						if (isset($overview['value']['targetAudiences'])){
							$targetAudience = array();
							foreach($overview['value']['targetAudiences'] as $targetAudience){
								$targetaudience[] = trim($targetAudience['targetAudience']);
							}
							$course['targetaudience'] = implode(", ", $targetaudience);
						}
						break;
				}
			}
		}

		if (isset($rawCourse['cmeinfocontent'])){
			foreach($rawCourse['cmeinfocontent'] as $cmeinfo){
				switch($cmeinfo['key']){
					case 'CMEInfoJSON':
						if (isset($cmeinfo['value']['availableCreditTypes'])){
							foreach($cmeinfo['value']['availableCreditTypes'] as $cme){
								$course['accreditations'][] = $cme;
							}
							$course['credits'] = isset($course['accreditations'][0]['credits']) ?
								$course['accreditations'][0]['credits'] : '';
							$course['time'] = $course['credits'];
							$course['hours'] = $course['credits'];
							break;
					}
				}
			}
		}
		return $course;
	}

	public static function title($id){
		$courses = ParseJSON::getJson();
		$title = $courses[$id]['title'];
		return $title;
	}

	public static function getThumb(&$course){
		$config = getHomebaseConfig();
		$type = 'thumb';
		$id = $course['id'];

		if ($config['useStaticImages'] || empty($course[$type])){
			$id = ($id == "3560"		) ? "3560a" : $id;
			$image = str_replace('{{COURSEID}}', $id, $config['thumbTemplate']);
			$course[$type] = $config['imageDir'] . $image;
		}
	}

	public static function getImage(&$course){
		$config = getHomebaseConfig();
		$type = 'image';
		$id = $course['id'];

		if ($config['useStaticImages'] || empty($course[$type])){
			$id = ($id == "3560") ? "3560a" : $id;
			$image = str_replace("{{COURSEID}}", $id, $config['imageTemplate']);
			$course[$type] = $config['imageDir'] . $image;
		}
	}

	public static function getArray($id=null){
		$courses = ParseJSON::getJson();
		if ($id !== null){
			return isset($courses[$id]) ? $courses[$id] : null;
		}
		return $courses;
	}

	public static function array_collapse($array, $attr){
		$return = array();
		foreach($array as $a){
			if (isset($a[$attr])){
				$return[] = $a[$attr];
			}
		}
		return $return;
	}

	public static function adminUI(){
		$courses = ParseJSON::getJson();
		PALog::log("ParseJSON::parse: complete:");
		return $courses;
	}
}
