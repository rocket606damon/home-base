<?php

class RenderDetailHtml{

	public static function getImage($id){
		$id = ($id == 3560) ? "3560a" : $id;
		$imageDir = '/lib/homebase_courses/media/courses/';

		$imageFilename = 'HPC_' . $id . '_full.png';
		$imagePath = $imageDir. $imageFilename;
		$wpPath = get_template_directory_uri() . $imagePath;
		return $wpPath;
	}

	public static function getUrl($id){
		$config = getHomebaseConfig();
		$url = $config['registerUrlPrefix'] . $id;;
		return $url;
	}

	public static function render($data){
		$config = getHomebaseConfig();

		$headAttrs = array(
			"id" => 'text',
			"image" => 'text',
			"format" => 'text',
			"instructors" => 'array',
			"targetaudience" => 'text',
			"blue_box_text" => 'text',
			"video_embed_link" => 'text',
			"disable-register" => 'text',
			"htmlBlueboxtext" => 'text',
			"time" => 'text',
			"title" => 'text',
			"credits" => 'text',
			"htmlBlueboxtextDiv" => 'text',
			"htmlVideoDiv" => 'text',
			"htmlAccreditations" => 'text',
			"htmlObjectives" => 'text',
			"pagename" => 'text',
			"videoClasses" => 'text',
			"objectivesClasses" => 'text',
			"debug" => 'text',
			"url" => 'text'
		);
		$objectiveAttrs = array(
			"intro" => 'text',
			"objectives" => 'list',
		);
		$videoAttrs = array(
			"video-class" => 'text',
			"video-width" => 'text',
			"video-height" => 'text',
			"video_embed_link" => 'text'
		);
		$blueboxTextAttrs = array(
			"blue_box_text_class" => 'text',
			"blue_box_text" => 'text'
		);
		$accreditationAttrs = array(
			"SectionNumber" => 'text',
			"CollapseNumber" => 'text',
			"accreditationStatment" => 'text',
			"accreditationNumber" => 'text',
			"SectionID" => 'text',
			"creditType" => 'text',
			"credits" => 'text',
			"designationStatment" => 'text',
		);

		$data['htmlObjectives'] = '';
		if(isset($data['modules']) && count($data['modules'])){
			foreach ($data['modules'] as $module){
				$data['htmlObjectives'] .=
					"<p>". $module['intro'] . "</p>\n".
					"<ul>\n" .
					"\t<li>".implode("</li>\n\t<li>", $module['objectives'])."</li>\n".
					"</ul>\n";
			}
		}else{
			$data['htmlObjectives'] =
				"<p>".$data['intro']."</p>\n".
				"<ul>\n" .
				"\t<li>".implode("</li>\n\t<li>", $data['objectives'])."</li>\n".
				"</ul>\n";
		}

		$acccount = count($data['accreditations']);
		for ($i = 0; $i < $acccount; $i++){
			if (empty($data['credits'])){
				$data['credits'] = $data['accreditations'][$i]['credits'];
				$data['hours'] = $data['accreditations'][$i]['credits'];
			}
			$data['accreditations'][$i]['SectionNumber'] = $i;
			$data['accreditations'][$i]['SectionID'] = "Collapse"+$i;
			$data['accreditations'][$i]['accreditationNumber'] = $i;
			$data['accreditations'][$i]['CollapseNumber'] = $i+1;
		}

		$data['pagename'] = "Course Information";
		$data['accreditations']  = isset($data['accreditations']) ? $data['accreditations'] : array();
		$data['image'] = RenderDetailHtml::getImage($data['id']);
		$data['url'] = RenderDetailHtml::getUrl($data['id']);
		$data['disable-register'] = (!empty($data['disabled'])) ?"course-disabled" : "";
		ParseJSON::getImage($data);


		if (empty($data['video_embed_link'])){
			$data['video-class'] = 'panel panel-default';
			$data['objectivesClasses'] = "col-sm-12 col-md-12";
			$data['video-width'] = '';
			$data['video-height'] = '';
			$data['video_embed_link'] = '';
			$data['htmlVideoDiv'] = '';
		}else{
			global $wp_embed;
			$data['objectivesClasses'] = "col-sm-6 col-md-6";
			$data['video-class'] = 'panel panel-default';
			$data['video-width'] = $config['video-width'];
			$data['video-height'] = $config['video-height'];
			$width = !empty($data['video-width']) ? " width=".$data['video-width'] : '';
			$height = !empty($data['video-height']) ? " height=".$data['video-height'] : '';
			$data['video_embed_link'] = $wp_embed->run_shortcode( '[embed' . $width . $height . ']' . $data['video_embed_link'] . '[/embed]' );
			$data['htmlVideoDiv'] = renderFromTemplate::render ($data, $videoAttrs, 'videoTemplate');
		}

		if (empty($data['blue_box_text'])){
			$data['htmlBlueboxtextDiv'] = '';
		}else{
			$data['blue_box_text_class'] = $config['blue-box-text-class'];
			$data['htmlBlueboxtextDiv'] = renderFromTemplate::render ($data, $blueboxTextAttrs, 'blueboxTextTemplate');
		}
		$accrTemplateName = $config['templateDir'].$config['detailAccreditationTemplate'];

		$data['htmlAccreditations'] = renderFromTemplate::renderList($data['accreditations'],
					$accreditationAttrs, 'detailAccreditationTemplate');
		$html = renderFromTemplate::render ($data, $headAttrs,'detailDocTemplate');

		$html = do_shortcode($html, true);

		return $html;
	}
}
