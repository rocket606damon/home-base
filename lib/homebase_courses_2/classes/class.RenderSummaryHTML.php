<?php

class RenderSummaryHTML{

	public static function getThumbImage($id){
		$id = ($id == 3560) ? "3560a" : $id;
		$imageDir = '/lib/homebase_courses/media/courses/';

		$thumbnailFilename = 'HPC_' . $id . '_thumb.png';
		$thumbnailPath = $imageDir. $thumbnailFilename;
		$wpPath = get_template_directory_uri() . $thumbnailPath;
		return $wpPath;
	}

	public static function renderItem($data){
		PALOG::log('RenderSummaryHtml:__construct');

		$itemAttrs = array(
			"credits" => 'text',
			"hours" => 'text',
			"id" => 'text',
			"url" => 'text',
			"number" => 'text',
			"href" => 'text',
			"thumb" => 'text',
			"disabled-div" => 'text',
			"disable-register" => 'text',
			"title" => 'text',
			"classes" => 'text'
		);

		ParseJSON::getThumb($data);
		$data['url'] = '/education-training/training-institute/courses-secondary/courses-tertiary?id=' +  $data['id'];
		$data['thumb'] = RenderSummaryHTML::getThumbImage($data['id']);
		$data['disabled-div'] = !empty($data['unavailable']) ? "disabled-div" : "";
		$data['disable-register'] = !empty($data['unavailable']) ? "course-disabled" : "";

		$html = renderFromTemplate::render($data, $itemAttrs, 'summaryItemTemplate');
		return $html;
	}

	public static function renderSection($data){
		//PALOG::log('RenderSummaryHtml:renderSection');

		$sectionAttrs = array(
			"SectionNumber" => 'text',
			"SectionTitle" => 'text',
			"CollapseNumber" => 'text',
			"SectionId" => 'text',
			"SectionHTML" => 'text'
		);
		$html = renderFromTemplate::render($data, $sectionAttrs, 'summarySectionTemplate');
		return $html;
	}

	public static function renderBody($data){
		//PALOG::log('RenderSummaryHtml:renderBody');

		$bodyAttrs = array(
			"htmlItems" => 'text'
		);
		$html = renderFromTemplate::render($data, $bodyAttrs, 'summaryDocTemplate');
		return $html;
	}
}
