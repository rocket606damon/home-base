<?php

class RenderFeaturedHTML{

	public static function getUrl($id){
		$url = "/education-training/training-institute/courses-tertiary/?id=" . $id;
		return $url;
	}

	public static function getThumbImage($id){
		$id = ($id == 3560) ? "3560a" : $id;
		$imageDir = '/lib/homebase_courses/media/courses/';

		$thumbnailFilename = 'HPC_' . $id . '_thumb.png';
		$thumbnailPath = $imageDir. $thumbnailFilename;
		$wpPath = get_template_directory_uri() . $thumbnailPath;
		return $wpPath;
	}

	public static function item($data){
		if (empty($data['id'])){
			return '';
		}else{
			$itemAttrs = array(
				"id" => 'text',
				"number" => 'text',
				"href" => 'text',
				"thumb" => 'text',
				"title" => 'text',
				"credits" => 'text',
				"hours" => 'text',
			);
			ParseJSON::getThumb($data);
			$data['href'] = RenderFeaturedHTML::getUrl($data['id']);
			$data['thumb'] = RenderFeaturedHTML::getThumbImage($data['id']);
			$html = renderFromTemplate::render($data, $itemAttrs, 'featuredItemTemplate');
			return $html;
		}
	}

	public static function body($data){

		$bodyAttrs = array(
			"htmlItems" => 'text'
		);
		$html = renderFromTemplate::render($data, $bodyAttrs, 'featuredDocTemplate');

		return $html;
	}
}
