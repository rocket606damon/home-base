<?php

define('CLASS_DIR', 'classes/');
define('VENDOR_DIR', 'vendor/');
define('SUMMARY', 'SUMMARY');
define('DETAIL', 'DETAIL');

define("USERNAME", "username");
define("PASSWORD", "password");
define("TOKEN", "Token");
define("CATEGORYNAME", "CategoryName");

define('DETAILDOCTEMPLATE', 'detailDocTemplate');
define('DETAILACCREDITATIONTEMPLATE', 'detailAccreditationTemplate'); //was detaildoctemplate

define('SUMMARYDOCTEMPLATE', 'summaryDocTemplate'); //was detaildoctemplate
define('SUMMARYSECTIONTEMPLATE', 'summarySectionTemplate');
define('SUMMARYITEMTEMPLATE', 'summaryItemTemplate');

define('SUMMARYCATEGORYTEMTEMPLATE', 'summaryCategoryTemplate');

define('FEATUREDITEMTEMPLATE', 'featuredItemTemplate');
define('FEATUREDDOCTEMPLATE', 'featuredDocTemplate');

define('CSVITEMTEMPLATE', 'csvItemTemplate');
define('CSVDOCTEMPLATE', 'csvDocTemplate');
define('CSVACCREDITATIONTEMPLATE', 'csvAccreditationTemplate');

define('AUDIENCEITEMTEMPLATE', 'audienceItemTemplate');
define('AUDIENCEBODYTEMPLATE', 'audienceBodyTemplate');

define('REGISTERURLPREFIX', 'https://homebasetraining.org/Users/HomeBaseUserLogin.aspx?ReturnURL=/users/producthomepage.aspx?ProductID=');
define('LEVEL3URLPREFIX', '/education-training/training-institute/courses-secondary/courses-tertiary?id=');

define('TEMPLATEDIR', 'templateDir');

define('JSON_COURSE_DATA_FILE' , BASE_DIR . 'cache/course_data.json');
define('JSON_COURSE_DATA_TEST_FILE' , BASE_DIR . 'cache/course_test_data.json');

define('HOMEBASE_COURSE_DATA_FILE' , BASE_DIR . 'cache/homebase_info.json');
define('HOMEBASE_COURSE_DATA_TEST_FILE' , BASE_DIR . 'cache/homebase_info.test.json');

define('LOGFILE' , BASE_DIR . 'log/hbcourses.log');

$config = array(
	'registerUrlPrefix' => 'https://homebasetraining.org/Users/HomeBaseUserLogin.aspx?ReturnURL=/users/producthomepage.aspx?ProductID=',
	'detailUrlPrefix' => '/education-training/training-institute/courses-secondary/courses-tertiary?id=',
	'qcBaseUrl' => 'https://homebasetraining-qc.astutetech.com/netscoreapi/RestAPIService.svc/',
	'baseUrl' => 'https://homebasetraining.org/netscoreapi/RestAPIService.svc/',
	'getByCategoryUrl' => 'GetAvailableActivitiesByCategory',
	'authenticateUrl' => 'Authenticate',
	'username' => 'api-lpcms',
	'password' => 'l@nChpA!',
	'category' => 'MGHHomeBase',

	'templateDir' => BASE_DIR."templates/",

	'summaryDocTemplate' => 'summary.doc.template.html',
	'summaryItemTemplate' => 'summary.item.template.html',
	'summarySectionTemplate' => 'summary.section.template.html',
	'summaryCategoryTemplate' => 'summary.category.template.html',

	'featuredItemTemplate' => 'featured.item.template.html',
	'featuredDocTemplate' => 'featured.doc.template.html',

	'csvItemTemplate' => 'csv.item.template.html',
	'csvDocTemplate' => 'csv.doc.template.html',
	'csvAccreditationTemplate' => 'csv.accreditation.template.html',

	'audienceItemTemplate' => 'audience.item.template.html',
	'audienceDocTemplate' => 'audience.doc.template.html',

	'detailDocTemplate' => 'detail.doc.template.html',
	'detailAccreditationTemplate' => 'detail.accreditation.template.html',
	'videoTemplate' => 'detail.video.template.html',
	'blueboxTextTemplate' => 'detail.blueboxtext.template.html',

	'cacheDir' => BASE_DIR."cache/",
	'cacheFile' => 'current_data.json',
	'cacheMinutes' => 60,
	'imageBaseUrl' => BASE_DIR."courses/",

	'debug' => true,
	'test' => true,

	'video-width' => 320,
	'video-height' => 240,
	'video-class' => '',

	'blue-box-text-class' => '',

	'audienceCatSlug' => 'audience-info',
	'displayCatSlug' => 'section-info',
	'featuredCatSlug' => 'featured-course'
);
