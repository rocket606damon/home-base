<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Config;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Config\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return '<div class="read-more"><a class="btn btn-primary" href="' . get_permalink() . '">' . __('Read More', 'sage') . '</a></div>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Change default media folder
 */
update_option('uploads_use_yearmonth_folders', 0);
update_option('upload_path', 'media');

/**
 * Enable font size & font family selects in the editor
 */
if ( ! function_exists( 'wpex_mce_buttons' ) ) {
	function wpex_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'fontselect' ); // Add Font Select
		array_unshift( $buttons, 'fontsizeselect' ); // Add Font Size Select
		return $buttons;
	}
}
add_filter( 'mce_buttons_2', __NAMESPACE__ . '\\wpex_mce_buttons' );

/**
 * Customize mce editor font sizes
 */
if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
	function wpex_mce_text_sizes( $initArray ){
		$initArray['fontsize_formats'] = "9px 10px 12px 13px 14px 16px 18px 20px 24px 28px 32px 36px 42px";
		return $initArray;
	}
}
add_filter( 'tiny_mce_before_init', __NAMESPACE__ . '\\wpex_mce_text_sizes' );

/**
 * Add Options Page
 */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'General Settings',
		'menu_title'	=> 'General Settings',
		'menu_slug' 	=> 'general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Header Settings',
		'menu_title'	=> 'Header Settings',
		'parent_slug'	=> 'general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer Settings',
		'menu_title'	=> 'Footer Settings',
		'parent_slug'	=> 'general-settings',
	));
	
}

 /**
 * Add responsive class to images
 */
function add_image_responsive_class($content) {
   global $post;
   $pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
   $replacement = '<img$1class="$2 img-responsive"$3>';
   $content = preg_replace($pattern, $replacement, $content);
   return $content;
}
add_filter('the_content', __NAMESPACE__ . '\\add_image_responsive_class');
add_filter('acf_the_content', __NAMESPACE__ . '\\add_image_responsive_class');

 /**
 * Wordpress SEO Meta
 */
add_filter( 'wpseo_metabox_prio', function() { return 'low';});

 /**
 * Get Top Ancestor ID
 */
if(!function_exists('get_post_top_ancestor_id')){
  function get_post_top_ancestor_id(){
	  global $post;
	  if($post->post_parent){
		  $ancestors = array_reverse(get_post_ancestors($post->ID));
		  return $ancestors[0];
	  }
	  return $post->ID;
  }
}

 /**
 *  Check if descendant
 */
function is_tree( $pid ) {
    global $post;              

    if ( is_page($pid) )
        return true;    

    $anc = get_post_ancestors( $post->ID );
    foreach ( $anc as $ancestor ) {
        if( is_page() && $ancestor == $pid ) {
            return true;
        }
    }
    return false;
}

/**
 * Get attachment image
 */
function get_attachment_image($ID, $size) {
	if (!$order) $order = 'DESC';
	$args = array(
	  'numberposts' => 1,
	  'order'=> $order,
	  'post_mime_type' => 'image',
	  'post_parent' => $ID,
	  'post_type' => 'attachment'
	);
	$url = wp_get_attachment_image_src(get_post_thumbnail_id($ID), 'thumbnail');
	if(!$url):
		$get_children_array = get_children($args,ARRAY_A);
		$rekeyed_array = array_values($get_children_array);
		$child_image = $rekeyed_array[0];  
		$url = wp_get_attachment_image_src($child_image['ID'], $size );
	endif;
	return $url[0];
}

/**
 * Custom Slug
 */
function custom_slug($slug) {
	$slug = strtolower($slug);
	$slug = preg_replace("/[^a-z0-9_\s-]/", "", $slug);
	$slug = preg_replace("/[\s-]+/", " ", $slug);
	$slug = preg_replace("/[\s_]/", "-", $slug);
	return $slug;
}

/**
 * Custom Slug
 */
function has_children() {
	global $wp_query; $page_id = $wp_query->queried_object->ID;
	if(get_pages('child_of='.$page_id)) return TRUE;
	else return FALSE;
}

/**
 * Redirect to first child
*/

function has_published_children() {
	global $wp_query; $page_id = $wp_query->queried_object->ID;
	$children = get_pages('child_of='.$page_id.'&sort_column=menu_order,post_title');
	foreach((array)$children as $child) {
		if($child->post_status == 'publish') return TRUE; break;
	}
}

function has_parent() {
	global $wp_query; $page_parent = $wp_query->queried_object->post_parent;
	if($page_parent) return TRUE;
	else return FALSE;
}

function get_first_published_child_id() {
	global $wp_query; $page_id = $wp_query->queried_object->ID;
	$children = get_pages('child_of='.$page_id.'&sort_column=menu_order,post_title');
	foreach((array)$children as $child) {
		if($child->post_status == 'publish') return $child->ID; break;
	}
}

add_action('template_redirect', __NAMESPACE__ . '\\redirect_to_first_child');
function redirect_to_first_child() {
	global $wp_query; $page_id = $wp_query->queried_object->ID;
	if(is_page() && has_children() && has_published_children && !has_parent()) {
		$children = get_pages('child_of='.$page_id.'&sort_column=menu_order,post_title&post_status=publish');
		$redirect = get_permalink(get_first_published_child_id());
		Header( "HTTP/1.1 301 Moved Permanently" ); 
		Header( "Location: $redirect" );
	}
}

/**
 * Register Locations Post Type
 */
function locations_post_type() {
	$labels = array(
		'name'                => _x( 'Locations', 'Post Type General Name' ),
		'singular_name'       => _x( 'Location', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Locations' ),
		'parent_item_colon'   => __( 'Parent Location' ),
		'all_items'           => __( 'All Locations' ),
		'view_item'           => __( 'View Location' ),
		'add_new_item'        => __( 'Add New Location' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Location' ),
		'update_item'         => __( 'Update Location' ),
		'search_items'        => __( 'Search Location' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
		
	$args = array(
		'label'               => __( 'Locations' ),
		'description'         => __( 'Locations' ),
		'labels'              => $labels,
		'supports' 			  => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'          => array( 'location-type' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 22,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'menu_icon' => 'dashicons-location',
		'rewrite' => array( 'slug' => 'locations' )
	);
	
	register_post_type( 'locations', $args );

}
add_action( 'init', __NAMESPACE__ . '\\locations_post_type', 0 );

/**
 * Register Locations Taxonomy
 */
function location_type_init() {
	register_taxonomy(
		'location-type',
		'locations',
		array(
			'label' => __( 'Location Type' ),
			'hierarchical' => true,
        	'show_admin_column' => true
		)
	);
}
add_action( 'init', __NAMESPACE__ . '\\location_type_init' );

/**
 * Register Locations Taxonomy
 */
add_filter( 'gform_cdata_open', __NAMESPACE__ . '\\wrap_gform_cdata_open' );
function wrap_gform_cdata_open( $content = '' ) {
  $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
  return $content;
}
add_filter( 'gform_cdata_close', __NAMESPACE__ . '\\wrap_gform_cdata_close' );
function wrap_gform_cdata_close( $content = '' ) {
  $content = ' }, false );';
  return $content;
}

/**
 * Update Featured Image with Banner Image
 */
add_action('save_post', __NAMESPACE__ . '\\set_featured_image_from_image');
function set_featured_image_from_image()
{
	$has_thumbnail = get_the_post_thumbnail($post->ID);
	if (!$has_thumbnail) {
		$page_banner = get_field('page_banner');
		$image_id = $page_banner[0]['banner_image']['id'];
		if ($image_id) {
			set_post_thumbnail($post->ID, $image_id);
		}
	}
}

/**
 * Sort an Array
 */
function array_sort($array, $on, $order=SORT_ASC){

    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}

 /**
 * Get YouTube ID from URL
 */
function get_youtube_id($url) {
	$pattern = 
        '%^# Match any YouTube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        |youtube(?:-nocookie)?\.com  # or youtube.com and youtube-nocookie
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char YouTube id.
        %x'
        ;
    $result = preg_match($pattern, $url, $matches);
    if (false !== $result) {
        return $matches[1];
    }
    return false;
}

 /**
 * Get Vimeo ID from URL
 */
function get_vimeo_id( $url ) {
	$regex = '~
		# Match Vimeo link and embed code
		(?:<iframe [^>]*src=")?         # If iframe match up to first quote of src
		(?:                             # Group vimeo url
				https?:\/\/             # Either http or https
				(?:[\w]+\.)*            # Optional subdomains
				vimeo\.com              # Match vimeo.com
				(?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
				\/                      # Slash before Id
				([0-9]+)                # $1: VIDEO_ID is numeric
				[^\s]*                  # Not a space
		)                               # End group
		"?                              # Match end quote if part of src
		(?:[^>]*></iframe>)?            # Match the end of the iframe
		(?:<p>.*</p>)?                  # Match any title information stuff
		~ix';
	
	preg_match( $regex, $url, $matches );
	
	return $matches[1];
}

 /**
 * Register Courses Post Type
 */
function courses_post_type() {
	$labels = array(
		'name'                => _x( 'Courses', 'Post Type General Name' ),
		'singular_name'       => _x( 'Course', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Courses' ),
		'parent_item_colon'   => __( 'Parent Course' ),
		'all_items'           => __( 'All Courses' ),
		'view_item'           => __( 'View Course' ),
		'add_new_item'        => __( 'Add New Course' ),
		'add_new'             => __( 'Add New' ),
		'edit_item'           => __( 'Edit Course' ),
		'update_item'         => __( 'Update Course' ),
		'search_items'        => __( 'Search Course' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' ),
	);
		
	$args = array(
		'label'               => __( 'Courses' ),
		'description'         => __( 'Courses' ),
		'labels'              => $labels,
		'supports' 			  => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'          => array( 'audience-type', 'category-type'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 18,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'menu_icon' => 'dashicons-welcome-learn-more',
		'rewrite' => array( 'slug' => 'courses' )
	);
	
	register_post_type( 'courses', $args );

}
add_action( 'init', __NAMESPACE__ . '\\courses_post_type', 0 );

/**
 * Register Display Taxonomy
 */
function display_type_init() {
	register_taxonomy(
		'display-type',
		'courses',
		array(
			'label' => __( 'Display Type' ),
			'hierarchical' => true,
      'show_admin_column' => true
		)
	);
}
add_action( 'init', __NAMESPACE__ . '\\display_type_init' );

/**
 * Adding New Columns
 */
function my_course_columns($columns)
{
	$columns = array(
		'cb'	 	=> '<input type="checkbox" />',
		'title' 	=> 'Title',
		'display-type' 	=> 'Display Type',
		'changed' => 'Changed',
		'disabled' => 'Disabled',
		'date'		=>	'Date',
	);
	return $columns;
}

function my_edited_columns($column, $post_id)
{
	switch($column) {
		case 'display-type':
			$terms = get_the_term_list( $post_id, 'display-type', '', ', ', '' );
			if ( is_string( $terms ) ) {
				echo $terms;
			} else {
				_e( 'Unable to get categories', 'your_text_domain' );
			}
			break;	
		case 'changed':
			if(get_field('changed')){echo 'Yes'; } else{echo 'No'; }
			break;
		case 'disabled':
			if(get_field('disabled')){echo 'True'; } else{echo 'False'; }
			break;	
	}
}

add_action('manage_courses_posts_custom_column', __NAMESPACE__ . '\\my_edited_columns');
add_filter('manage_edit-courses_columns', __NAMESPACE__ . '\\my_course_columns');