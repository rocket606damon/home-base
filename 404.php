<script>
  window.location.href = "http://homebase.org/";
</script>

<-- Code Disabled by Christopher Sonn on 09/02/2016 to redirect all "page not found" to the home base homepage
<?php get_template_part('templates/page', 'header'); ?>

<div class="alert alert-warning">
  <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
</div>
--?
